import React from "react";
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    ActivityIndicator,
    TouchableOpacity,
    AlertIOS,
    Alert,
    Image,
    Modal,
    Platform 
} from "react-native";
import { BarCodeScanner,Camera, ScreenOrientation } from "expo";
import {MaterialCommunityIcons} from "@expo/vector-icons";

import layout from "../assets/styles/layout_checkin";
import Colors from "../constants/Colors_checkin";

//import layout from "../assets/styles/layout";
var {height, width} = Dimensions.get('window');

   
export default class ModalBarCode extends React.Component {
    state = {
        modalVisible: false
    }

    isScanned = false;
    type = BarCodeScanner.Constants.Type.front;

    componentWillUnmount(){
        Dimensions.removeEventListener("change", () => {});
    }

    async componentWillMount() {
        
        //console.log(ScreenOrientation.Orientation);
        var screen = Dimensions.get('window');
        width = screen.width;
        height = screen.height;
        //console.log(BarCodeScanner.Constants);
        let _this = this;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            height = screen.height;
            _this.setState({ appIsReady: true });
        })

        
    }

    close = () => {
        if(Platform.OS === "ios"){
            ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_RIGHT);    
        }
        this.props.closeBarCode();
        this.setState({modalVisible: false});
    }

    show = (orien) => {
        //this.setState({styleAnimation : 'slide',modalVisible: true });
        this.isScanned = false;
        this.setState({modalVisible: true });

        setTimeout(() => {
            //ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_RIGHT);
            //_this.setState({ appIsReady: true });

            if(Platform.OS != "ios"){
                if(orien == 'lanscape'){
                    ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_LEFT);
                }                
            }
            
            if(Platform.OS === "ios"){
                if(orien == 'lanscape'){
                    ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_RIGHT);
                    //ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
                }     
            }

        },1000)
    }
    
    _handleBarCodeRead = ({ type, data }) => {
        if(!this.isScanned){
            //ScreenOrientation.allow(ScreenOrientation.Orientation.ALL);
            this.isScanned = true;
            //if(type === 'org.iso')
            //Alert.alert('Error',`Bar code with type ${type} and data ${data} has been scanned!`);
            
            //this.close();
            this.props.scanned(data);
        }
       
    }

    changeType = () => {
        this.type = this.type == BarCodeScanner.Constants.Type.front ?  BarCodeScanner.Constants.Type.back : BarCodeScanner.Constants.Type.front;
        this.setState({rerender:true});
    }

    

    /*
    cancel = () => {
        //console.log('ok');
    }*/

    render() {
        let color = 'rgba(0,0,0,0.7)';
        return(
            <Modal
                animationType={'none'}
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}

            >
                <View style={styles.container}>
                    
                {/*
                    <BarCodeScanner
                        onBarCodeRead={this._handleBarCodeRead}
                        style={StyleSheet.absoluteFill}
                />*/}
                    

{
    /*
}
                        <View>
                            <Text style={styles.texttop}>
                            Scan QR Code
                            </Text>
                        </View>
                        <View style={styles.rectangleContainer}>
                            <Camera
                                aspectRatio={1.33}
                                barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
                                //flashMode={flashMode}
                                onBarCodeRead={this._handleBarCodeRead}
                                ratio="4:3"
                                //ref={ref => this.camera = ref}
                                style={styles.camera}
                                type={BarCodeScanner.Constants.Type.back}>
                                <View style={styles.rectangleContainer}>
                                    <View style={styles.rectangle}/>
                                </View>            
                            </Camera>
                        </View>
                        <Text style={styles.textbottom}>
                            Cancel
                        </Text>
*/}

                    <BarCodeScanner         
                        //aspectRatio={1.33}
                        barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
                        //flashMode={flashMode}
                        onBarCodeRead={this._handleBarCodeRead}
                        //ratio="4:3"
                        //ref={ref => this.camera = ref}
                        style={{flex:1, width: width, height:height}}
                        type={this.type}
                        >
                        <View style={styles.container2}>
                            <View style={{backgroundColor:color, height: height / 2 - 125, width:width,alignItems:'center'}}>
                                <Text style={styles.texttop}>
                                    Scan QR Code
                                </Text>
                                <TouchableOpacity style={styles.switch} activeOpacity={1} onPress={this.changeType}>
                                    <MaterialCommunityIcons
                                        name={'camera-party-mode'}
                                        size={40}
                                        color={'#fff'} 
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{height:250,width:width,position:'relative',zIndex:1}}>
                                <View style={{flex:1, flexDirection:'row',justifyContent:'space-between'}}>
                                    <View style={{backgroundColor:color, height:250, width:width / 2 - 125, position:'relative'}}>
                                        <View style={{position:'absolute',right:-17, top:-3, width:20, height:20,
                                            borderColor: '#fff',borderWidth: 3,borderRightWidth: 0,borderBottomWidth: 0}}></View>
                                        <View style={{position:'absolute',right:-17, bottom:-3, width:20, height:20, 
                                            borderColor: '#fff',borderWidth: 3,borderRightWidth: 0,borderTopWidth: 0}}></View>     
                                    </View>
                                    <View style={{backgroundColor:'transparent', height:250, width:250}}></View>
                                    <View style={{backgroundColor:color, height:250, width:width / 2 - 125, position:'relative'}}>
                                        <View style={{position:'absolute',left:-17, top:-3, width:20, height:20, 
                                            borderColor: '#fff',borderWidth: 3,borderLeftWidth: 0,borderBottomWidth: 0}}></View>
                                        <View style={{position:'absolute',left:-17, bottom:-3, width:20, height:20, 
                                            borderColor: '#fff',borderWidth: 3,borderLeftWidth: 0,borderTopWidth: 0}}></View> 
                                    </View>
                                </View>
                            </View>
                            <View style={{backgroundColor:color, height: height / 2 - 125, width:width,alignItems:'center'}}>
                                <Text style={styles.textbottom} onPress={this.close}>
                                    Cancel
                                </Text>
                            </View>
                        </View>
                    </BarCodeScanner>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container2:{
        flex:1,
        flexDirection:'column',
        justifyContent:'space-between'
    },

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    camera: {
        flex: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        height: Dimensions.get('window').width,
        width: Dimensions.get('window').width,
    },  
    texttop: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        position:'absolute',
        top:70,
        color:'#fff',
        zIndex:1
    },
    switch:{
        position:'absolute',
        top:120,
        zIndex:1
    },
    textbottom: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        position:'absolute',
        bottom:40,
        color:'#fff',
        zIndex:1
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    rectangleContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },
rectangle: {
        height: 250,
        width: 250,
        borderWidth: 2,
        borderColor: '#fff',
        backgroundColor: 'transparent',
    }, 
});
