import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import {LinearGradient} from "expo";
import {MaterialCommunityIcons} from "@expo/vector-icons";
export default class BtnQuantity extends Component {
    onQuantityService = (type, id) =>{
        this.props.onPress(type, id);
    }

  render () {
    let  quantity = this.props.quantity;
    let  id = this.props.id;
    let selected = typeof(this.props.selected) != 'undefined' ? true : false; 
    let selectedStyle = {};
    if(selected){
        selectedStyle.color = '#fff';
    }
    return (
        <View style={{flex:1}}>
            <LinearGradient start={[0, 0]} end={[1, 0]} colors={["rgba(236,111,160, 1)", "rgba(249,193,152, 1)"]}  style={[styles.txtLoginFormborder]}>
                <View style={styles.border}>
                    <Text style={styles.textquntity}>{quantity}</Text>
                </View>
                <View style={{flexDirection:'column', alignItems:'stretch', alignSelf: 'flex-end',justifyContent: 'flex-end',}}> 
                    <TouchableOpacity style={styles.btnminus} onPress={()=>this.onQuantityService("plus", id)} activeOpacity={1}>
                        <MaterialCommunityIcons name='menu-up-outline' size={22} style={{color:"#fff", textAlign:"center"}}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnplus} onPress={()=>this.onQuantityService("minus", id)} activeOpacity={1}>
                    <MaterialCommunityIcons name='menu-down-outline' size={22} style={{color:"#fff", textAlign:"center"}} />
                    </TouchableOpacity>
                </View>
            </LinearGradient>
        </View>      
    )
  }
}

const styles = StyleSheet.create({
    btnplus:{
        borderRightColor:"transparent",
        borderTopColor:"#eee",
        borderLeftColor:"transparent",
        borderBottomWidth:0, 
        borderWidth:1, 
        height:21, 
        width:40,
    },
    btnminus:{
        borderRightColor:"transparent",
        borderTopColor:"transparent",
        borderLeftColor:"transparent",
        borderBottomWidth:0,                                   
        borderWidth:1, 
        height:19,
        width:40,
        marginBottom:3
    },
    textquntity:{
        fontSize:16, 
        height:40,
        width:40,
        padding:10,
        textAlign:"center",
        color:"#333",
        backgroundColor: "#fff",

    },
    border:{
        padding:2
    },
    txtLoginFormborder: {
        flexDirection:'row',
         width:85,
         borderColor:"#fff",
         borderWidth:1, 
    },
})