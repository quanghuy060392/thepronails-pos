import React from "react";
import ReactNative,{
    StyleSheet,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    TextInput,
    Platform,
    Image,
    Alert,
    KeyboardAvoidingView,
    Keyboard
} from "react-native";
import layout from "../../assets/styles/layout_checkin";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import { LinearGradient} from "expo";
import TextField from 'react-native-md-textinput';
import { formatPhone, inputBirthDate } from "../../helpers/Utils";
import ReturnCustomer from './ReturnCustomer';
import emailvalidator from "email-validator";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ScrollableTabView from "react-native-scrollable-tab-view";
import HideTabBar from "../scrolltab/HideTabBar";
import ClientTabs from "./clientTabs";
import EditClient from "./editClient";
import FloatSwitch from "../../components_checkin/FloatSwitch";
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
var columnWidth = width / 2;
var iPadPro = false;
if(width > 1024){
    iPadPro = true;
}

var screenOrientation = 'PORTRAIT';
if(width > height){
    screenOrientation = 'LANDSCAPE';
}

export default class Customer extends React.Component{
    stata = {
        appIsReady: false
    }

    clientData = {
        id : 0,
        firstname : "",
        lastname : "",
        email : this.props.clientSearchData.email,
        phone : this.props.clientSearchData.phone,
        birthdate : "",
        month: "",
        day: ""
    };

    clientSearchData = this.props.clientSearchData;
    isShowEdit = false;
    componentWillMount(){
        
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        width = Dimensions.get('window').width;
        height = Dimensions.get('window').height;
        columnWidth = width / 2;

        if(width > height){
            screenOrientation = 'LANDSCAPE';
        }else{
            screenOrientation = 'PORTRAIT'
        }
  
        let _this = this;
        Dimensions.addEventListener('change',function(){
            
            var screen = Dimensions.get('window');
            width = screen.width;
            //height = screen.height;
            columnWidth = width / 2;

            height = screen.height;
            if(width > height){
                screenOrientation = 'LANDSCAPE';
            }else{
                screenOrientation = 'PORTRAIT'
            }

            if(typeof(_this.refs.scrollnewcustomer) != 'undefined'){
                setTimeout(function(){
                    _this.refs.scrollnewcustomer.scrollTo({x: 0, y: 0, animated: true});  
                },2000);      
            }

            _this.setState({ appIsReady: true });
        })
    }

    

    componentWillUnmount () {
        this.keyboardDidHideListener.remove();
        Dimensions.removeEventListener("change", () => {});
      }

    _keyboardDidHide () {
        if(!iPadPro){
            if(screenOrientation == 'LANDSCAPE' && typeof(this.refs.scrollnewcustomer) != 'undefined'){
                this.refs.scrollnewcustomer.scrollTo({x: 0, y: 0, animated: true});        
            }
        }
        
    }

    changeFirstName = (firstname) => {
        this.clientData.firstname = firstname;
    }

    changeLastName = (lastname) => {
        this.clientData.lastname = lastname;
    }

    changePhone = (value) => {
        let formatValue = formatPhone(value);
        if (formatValue == "(") formatValue = "";
        this.refs.txtphoneinput.setState({ text: formatValue });
        this.clientData.phone = formatValue;        
    }

    changeEmail = (email) => {
        this.clientData.email = email;
    }

    changeBirthdate = (value) => {
        let formatValue = inputBirthDate(value);
        if (formatValue != "" && parseInt(formatValue) > 12) formatValue = "12";
        this.refs.txtmonthinput.setState({ text: formatValue });
        this.clientData.birthdate = formatValue;

        let birthdate = formatValue.split('/');
        this.clientData.month = birthdate[0];
        if(birthdate.length > 1){
            this.clientData.day = birthdate[1];
        }
    }
    onChangeOptOutSMS = () =>{
        let value = this.clientData.OptOutSMS == 1 ? true : false;
        this.refs.txtOptOutSMS.setState({ text: !value });
        this.clientData.OptOutSMS = !value == true ? 1 : 0;
    }
    onChangeOptOutMAIL = () =>{
        let value = this.clientData.OptOutMAIL == 1 ? true : false;
        this.refs.txtOptOutMAIL.setState({ text: !value });
        this.clientData.OptOutMAIL = !value == true ? 1 : 0;
    }
    saveClient = () => {
        let isValid = true;
        
       if (String.prototype.trim.call(this.clientData.firstname) == "") {
           isValid = false;
           Alert.alert("Error", "Please enter first name");
       } else if (String.prototype.trim.call(this.clientData.email) != "" && !emailvalidator.validate(String.prototype.trim.call(this.clientData.email))) {
           isValid = false;
           Alert.alert("Error", "Please enter valid email or leave empty");
       } else if (
           String.prototype.trim.call(this.clientData.phone) == "" &&
           this.clientData.phone.length != 14
       ) {
           isValid = false;
           Alert.alert(
               "Error",
               "Please enter a valid phone with mask (###) ###-####"
           );
       } else if (
           String.prototype.trim.call(this.clientData.month) != "" ||
           String.prototype.trim.call(this.clientData.day) != ""
       ) {
           if (String.prototype.trim.call(this.clientData.month) == "") {
               isValid = false;
               Alert.alert("Error", "Please enter birthdate with format MM / DD or leave empty");
           } else if (String.prototype.trim.call(this.clientData.day) == "") {
               isValid = false;
               Alert.alert("Error", "Please enter birthdate with format MM / DD or leave empty");
           }
       }
       if(isValid){
            if (
                String.prototype.trim.call(this.clientData.month) != "" &&
                String.prototype.trim.call(this.clientData.day) != ""
            ) {
                this.clientData.birthdate =
                    this.clientData.month + "/" + this.clientData.day;
            }
            this.clientData.id = 0;
            this.props.onPress(this.clientData);
       }
    }

    clientSelected = (data) => {
        this.props.onPress(data);
    }

    onPressEdit = (data) => {
        //console.log(data);
        /*
        this.refs.tabs.goToPage(1);
        let _this = this;
        setTimeout(function(){
            _this.refs.EditClient.setData(data);
        },0)*/
        
        this.isShowEdit = true;
        this.setState({rerender:true});
        let _this = this;
        setTimeout(function(){
            _this.refs.EditClient.setData(data);
        },0)
    }

    onPressTab = (tabIndex) => {
        this.refs.tabs.goToPage(tabIndex);
    }

    onBack = () => {
        this.isShowEdit = false;
        this.setState({rerender:true});
        /*
        this.refs.tabs.goToPage(0);
        */
    }

    clientUpdated = (client) => {
        //this.refs.tabs.goToPage(0);
        /*
        this.refs.ReturnCustomer.setState({data: client});
        this.props.onClientUpdated(client);
        let _this = this;
        setTimeout(function(){
            _this.refs.tabs.goToPage(0);
        },1000)*/

        this.props.onClientUpdated(client);
        this.isShowEdit = false;
        this.setState({rerender:true});
        let _this = this;
        setTimeout(function(){
            _this.refs.ReturnCustomer.setState({data: client});
        },1000)
    }

    onFocus = (refname) => {
        if(!iPadPro && screenOrientation == 'LANDSCAPE'){
            if(refname == 'firstname' || refname == 'lastname'){
                this.refs.scrollnewcustomer.scrollTo({x: 0, y: 10, animated: false});
            }
    
            if(refname == 'phone' || refname == 'email'){
                this.refs.scrollnewcustomer.scrollTo({x: 0, y: 85, animated: true});
            }
    
            if(refname == 'birthdate'){
                this.refs.scrollnewcustomer.scrollTo({x: 0, y: 150, animated: true});
            }
        }
        
        
    }

   
    render() {     
         
        return (
            <View style={styles.container}>
            
                <View style={styles.containerTab}>
                    {/*
                    <ClientTabs onPress={this.onPressTab} />
                    */}    

                    {this.props.isClientExists && !this.isShowEdit &&
                        <View style={styles.tabviewscontainer}>
                            <ScrollView style={styles.TabContent} keyboardShouldPersistTaps='always' key={1} tabLabel="React">
                                <ReturnCustomer clientSearchData={this.clientSearchData} ref='ReturnCustomer' token={this.props.token} onPress={this.clientSelected} onPressEdit={this.onPressEdit}/>
                            </ScrollView>
                        </View>
                         
                    }

                    {this.props.isClientExists && this.isShowEdit &&
                        <View style={styles.tabviewscontainer}>
                           <ScrollView style={styles.TabContent} keyboardShouldPersistTaps='always' key={2} tabLabel="React 1">
                                <EditClient ref='EditClient' onBack={this.onBack} token={this.props.token} onUpdated={this.clientUpdated}/>
                            </ScrollView>
                        </View>
                         
                    }    

                    {!this.props.isClientExists &&
                            <View style={styles.tabviewscontainer}>
                                <ScrollView style={[styles.TabContent]} keyboardShouldPersistTaps='always' ref='scrollnewcustomer'> 
                                    <View style={styles.twocolumns}>
                                        <TextField label={'First Name'} 
                                            onChangeText={(firstname) => this.changeFirstName(firstname)}
                                            highlightColor={'#F069A2'} 
                                            labelStyle={StyleSheet.flatten(styles.labelStyle)}
                                            inputStyle={StyleSheet.flatten([styles.textbox,{width: 270}])}    
                                            onFocus={() => this.onFocus('firstname')}
                                        />
                                        <TextField label={'Last Name'} 
                                            onChangeText={(lastname) => this.changeLastName(lastname)}
                                            highlightColor={'#F069A2'} 
                                            labelStyle={StyleSheet.flatten(styles.labelStyle)}
                                            inputStyle={StyleSheet.flatten([styles.textbox, {width: 270}])}
                                            onFocus={() => this.onFocus('lastname')}
                                        />
                                    </View>
                                    <View style={styles.twocolumns}>
                                        <TextField label={'Phone'} 
                                            onChangeText={(phone) => this.changePhone(phone)}
                                            highlightColor={'#F069A2'} 
                                            labelStyle={StyleSheet.flatten(styles.labelStyle)}
                                            inputStyle={StyleSheet.flatten([styles.textbox, {width: 270}])}
                                            ref={'txtphoneinput'}
                                            onFocus={() => this.onFocus('phone')}
                                            value={this.props.clientSearchData.phone}
                                        />
                                        <TextField label={'Email'} 
                                            onChangeText={(email) => this.changeEmail(email)}
                                            highlightColor={'#F069A2'} 
                                            labelStyle={StyleSheet.flatten(styles.labelStyle)}
                                            inputStyle={StyleSheet.flatten([styles.textbox, {width: 270}])}
                                            onFocus={() => this.onFocus('email')}
                                            value={this.props.clientSearchData.email}
                                        />
                                    </View>
                                    
                                    
                                    <TextField label={'Birthdate (MM / DD)'} 
                                        onChangeText={(birthdate) => this.changeBirthdate(birthdate)}
                                        highlightColor={'#F069A2'} 
                                        labelStyle={StyleSheet.flatten(styles.labelStyle)}
                                        inputStyle={StyleSheet.flatten([styles.textbox, {width: 560}])}
                                        ref={'txtmonthinput'}
                                        onFocus={() => this.onFocus('birthdate')}
                                    />
                                    <View style={[styles.twocolumns, styles.floatGroup]}>
                                        <FloatSwitch
                                            placeholder={'Marketing Opt-Out: SMS'}
                                            value={this.clientData.OptOutSMS == 1 ? true : false}
                                            onPress={this.onChangeOptOutSMS}
                                            ref="txtOptOutSMS"
                                        />
                                        <FloatSwitch
                                                placeholder={'Marketing Opt-Out: Email'}
                                                value={this.clientData.OptOutMAIL == 1 ? true : false}
                                                onPress={this.onChangeOptOutMAIL}
                                                ref="txtOptOutMAIL"
                                            />
                                    </View>
                                    <View style={[styles.btnSave,{width: 560}]}>
                                        <TouchableOpacity
                                            activeOpacity={1}
                                            style={styles.btnSaveWraper}
                                            onPress={this.saveClient}
                                        >
                                            <LinearGradient
                                                start={[0, 0]}
                                                end={[1, 0]}
                                                colors={["#F069A2", "#EEAEA2"]}
                                                style={styles.btnLinear}
                                            >
                                                <Text style={styles.btnSaveText}>Next</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                </ScrollView>
                            </View> 
                    }
                    
                    <View style={{height:20}}></View>
                </View>   
               

                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#f2f2f2',
        justifyContent: "center",
        alignItems: "center"
    },
    tabviewscontainer:{
        backgroundColor:'#fff',
        flex:1,
    },
    TabContent:{
     
        //backgroundColor:'#fff',
        //paddingLeft:20,
        //paddingRight:20,
        width:560,
        marginLeft:20
    },
    containerTab:{
        width:600,
        flex:1,
        marginTop:20
    },
    twocolumns:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    row:{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    title:{
        fontSize:22,
        fontFamily:'Futura',
        textAlign:'center',
        color:'#333'
    },
    column:{
        paddingTop:30,
        flex:1
    },
    contentsWrapperLeft:{
        marginTop:10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#fff',
        marginLeft:20,
        marginRight:10
    },
    labelStyle:{
        fontSize:16
    },
    textbox:{
        fontSize:20,
        height:45
    },
    contentsWrapperRight:{
        marginTop:10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#fff',
        marginLeft:10,
        marginRight:20
    },
    btnSave: {
        height: 50,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginBottom: 15
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 20,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1
    },
    vertical:{
        width:1,
        height:400,
        backgroundColor:'#ddd',
        position:'absolute',
        top:75
    },
    floatGroup:{
        height:50,
        marginTop:10
    },  
    
})