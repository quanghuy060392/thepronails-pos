import React from "react";
import {
    StyleSheet,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    TextInput,
    Alert
} from "react-native";
import layout from "../../assets/styles/layout_checkin";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import { LinearGradient, ScreenOrientation} from "expo";

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').width.height;
//var orientation = (width > height) ? 'LANDSCAPE' : 'PORTRAIT';
var columns = 2;
var serviceWidth = (width - 45) / columns;
var serviceRightWidth = 80;
var serviceLeftWidth = serviceWidth - serviceRightWidth;
//const orientation = (width > height) ? 'LANDSCAPE' : 'PORTRAIT';
var btnWidth = width - 30;
export default class ListCategories extends React.Component{
    state = {
        selectedService:[],
        showCloseSearchBox: false
    }

    columnWidth = width / columns;
    search = '';
    selectServices = [];
    price = 0;
    selectedServicesInCategory = [];

    componentWillUnmount(){
        Dimensions.removeEventListener("change", () => {});
    }

    componentWillMount(){
        width = Dimensions.get('window').width;
        height = Dimensions.get('window').width.height;
        serviceWidth = (width - 45) / columns;
        serviceLeftWidth = serviceWidth - serviceRightWidth;
        this.columnWidth = width / columns;

        let _this = this;
        btnWidth = width - 30;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            height = screen.height;
            /*
            orientation = (width > height) ? 'LANDSCAPE' : 'PORTRAIT';
            if(orientation == 'LANDSCAPE'){
                columns = 3;
            }*/

            serviceWidth = (width - 45) / columns;
            serviceLeftWidth = serviceWidth - serviceRightWidth;
            _this.columnWidth = width / columns;
            btnWidth = width - 30;
            _this.setState({ appIsReady: true });
        })
    }

    changeSearchText = (searchtext) => {
        this.search = searchtext;
        if (String.prototype.trim.call(searchtext) == '') {
            this.setState({showCloseSearchBox: false});
        } else {
            this.setState({showCloseSearchBox: true});
        }
    }

    clearSearch = () => {
        this.search = '';
        this.refs['searchtextinput'].clear();
        this.setState({showCloseSearchBox: false});
    }

    onPressService = (service) => {
        //console.log(service);
        let serviceSelected = this.selectServices.filter(function(item){
            return item.id == service.id;
        });
        let newList = [];
        if(serviceSelected.length){
            if(typeof(this.props.isBooking) == 'undefined'){
                this.selectServices.forEach(function(item) {
                    if(item.id != service.id){
                        newList.push(item);
                    }
                });
                this.selectServices = newList;
            }
        }else{
            this.selectServices.push(service);
        }
        
        this.props.onPress(this.selectServices, service);  
        this.setState({selectedService: this.selectServices});      
    }

    saveAppointment = () => {
        let isValid = Object.keys(this.selectServices).length;
        if(!isValid){
            Alert.alert('Error','Please choose service');        
        }else{
            this.props.onSave();
        }
        
    }

    renderCategories = () => {
        let arrKey = {};
        let categoryDisplay = this.props.categories.map((x, i) => {
            let isExists = this.selectServices.filter(function(item){
                return item.id == x.id
            }).length;
          
            let serviceTextColor = styles.defaultColor;
            let serviceDurationTextColor = styles.defaultColor;
            

            let isShow = true;
            if(this.search != ''){
                isShow = x.name.toLowerCase().indexOf(this.search.toLowerCase()) >= 0;
            }
            if(isShow){
                let customStyle = {};
                let customFontSize = {};
                let isChangeBackound = false;
                if(typeof(x.category_backgroundcolor) != 'undefined' && x.category_backgroundcolor != null 
                    && String.prototype.trim.call(x.category_backgroundcolor) != ''){
                    customStyle.backgroundColor = x.category_backgroundcolor;
                    isChangeBackound = true;
                }

                if(typeof(this.props.userData) != 'undefined'){
                    if(typeof(this.props.userData.checkinCategoryFontSize) != 'undefined'
                    && this.props.userData.checkinCategoryFontSize != null 
                    && String.prototype.trim.call(this.props.userData.checkinCategoryFontSize) != ''){
                        //console.log(this.props.userData.checkinCategoryFontSize);
                        if(this.props.userData.checkinCategoryFontSize > 0){
                            customFontSize.fontSize = this.props.userData.checkinCategoryFontSize;
                            if(isChangeBackound){
                                customFontSize.color = 'black';
                            }
                        }
                        
                    }
                }

                let listServiceSelected = this.selectedServicesInCategory.filter(function(itemService){
                    if(typeof(itemService.category_customname) != 'undefined' && String.prototype.trim.call(itemService.category_customname) != ''){
                        return itemService.category_customname == x.name;
                    }else{
                        return itemService.category_name == x.name;
                    }
                })

                

                let countService = 0;
                let lblCountService = '1 service';
                if(listServiceSelected.length){
                    isExists = true;
                    countService = listServiceSelected.length;
                    if(countService > 1){
                        lblCountService = countService + ' services';
                    }
                }
                
                if(typeof(this.props.isBooking) != 'undefined'){
                    isExists = false;
                }

                if(isExists){
                    serviceTextColor = styles.whiteColor;
                    serviceDurationTextColor =  styles.whiteDurationColor;
                }

                //backgroundColor:x.category_backgroundcolor
                return (
                    <TouchableOpacity key={x.name} activeOpacity={1} onPress={() => {this.onPressService(x)}}>
                        <View style={{ width: this.columnWidth  }}>
                            { isExists == 0 &&
                                <View style={[styles.serviceItem, i % columns == 0 ? styles.even : styles.odd,customStyle]}>
                                    
                                    <View style={[styles.serviceLeft]}>
                                        <Text style={[styles.serviceName,serviceTextColor,customFontSize]}>{x.name}</Text>
                                    </View>
                                    {
                                        countService > 0
                                        &&
                                        <View style={{paddingRight:15}}>
                                            <Text style={{color: '#000',backgroundColor:'transparent', fontSize:22}}>{lblCountService}</Text>
                                        </View>
                                    }
                                </View>
                            }

                            { isExists > 0 &&
                                <LinearGradient start={[0, 0]} end={[1, 0]} colors={['#F069A2', '#EEAEA2']} 
                                    style={[styles.containerItemSelected,i % 2 == 0 ? styles.even : styles.odd]}>
                                    <View style={[styles.serviceLeft]}>
                                        <Text style={[styles.serviceName,customFontSize,serviceTextColor]}>{x.name}</Text>
                                    </View>
                                    {
                                        countService > 0
                                        &&
                                        <View style={{paddingRight:15}}>
                                            <Text style={{color: '#fff',backgroundColor:'transparent', fontSize:22}}>{lblCountService}</Text>
                                        </View>
                                    }
                                </LinearGradient>  
                            }

                        </View>
                    </TouchableOpacity>    
                )
            }else{
                return false;
            }
            
        });
        return categoryDisplay;
    }

    reload = (selectedServicesInCategory) => {
        this.selectedServicesInCategory = selectedServicesInCategory;
        this.setState({rerender:true});
    }

    render() {
        //console.log(this.columnWidth);
        this.price = 0;
        let increase = 0;
  
        let categoryDisplay = this.renderCategories();
        let isBooking = typeof(this.props.isBooking) != 'undefined' ? true : false;

        
        return (
            <View style={styles.container}>
                <View style={styles.searchWrapper}>
                    <View style={layout.searchContainer}>
                        <MaterialCommunityIcons
                            name={'magnify'}
                            size={20}
                            color={'#6b6b6b'} style={layout.iconsearchbox}
                        />
                        <TextInput
                            placeholder='Search Service' placeholderTextColor='#6b6b6b'
                            underlineColorAndroid={'transparent'}
                            style={layout.searchbox}
                            onChangeText={(searchtext) => this.changeSearchText(searchtext)}
                            ref={'searchtextinput'}
                        />

                        {this.state.showCloseSearchBox &&
                        <TouchableOpacity style={layout.iconclosesearchbox} activeOpacity={1}
                                          onPress={() => this.clearSearch()}>
                            <MaterialCommunityIcons
                                name={'close-circle-outline'}
                                size={20}
                                color={'#6b6b6b'}
                            />
                        </TouchableOpacity>
                        }
                    </View>    
                </View>
                <View style={{flex:1}}>
                    <ScrollView contentContainerStyle={styles.dataContainer} keyboardShouldPersistTaps="always">
                        {categoryDisplay}
                        {/*
                            this.props.listcombo.length > 0 && isAnyCombo &&
                            <View  style={{width: width}}>
                                <View style={[styles.categoryContainer,{width: width}]}>
                                    <Text style={styles.categoryLabel}>Combos</Text>
                                </View>    
                            </View>
                        */}
                        { /* listcombo */}
                    </ScrollView>
                    <View style={[styles.btnBlockWraper,{width:btnWidth}]}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={styles.btnSaveWraper}
                            onPress={this.saveAppointment}
                        >
                            <LinearGradient
                                start={[0, 0]}
                                end={[1, 0]}
                                colors={["#F069A2", "#EEAEA2"]}
                                style={styles.btnLinearConfirm}
                            >
                                {isBooking && 
                                    <Text style={styles.btnSaveTextConfirm}>Choose Time</Text>
                                }
                                
                                {!isBooking && 
                                    <Text style={styles.btnSaveTextConfirm}>View Summary</Text>
                                }
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    categoryContainer:{
        height:40,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:15
    },
    categoryLabel:{
        color:'#F069A2',
        fontSize:24,
        fontFamily:'Futura'
    },
    selectedServices:{
        height:80,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    selectedServicesCount:{
        fontSize:22,
        fontFamily:'Futura',
        color:'#fff',
        backgroundColor:'transparent'
    },
    selectedServicesPrice:{
        color:'#fff',
        fontSize:22,
        fontFamily:'Futura',
        backgroundColor:'transparent'
    },
    searchWrapper:{
        //marginBottom:15
    },
    dataContainer:{
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    serviceItem:{
        
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor:'#fff',
        flexDirection: 'row',
        marginBottom:15,
        paddingTop:20,
        paddingBottom:20,
        borderRadius:4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2

    },
    even:{
        marginLeft:15,
        marginRight:7.5,        
    },
    odd:{
        marginLeft:7.5,
        marginRight:15,      
    },
    whiteColor:{
        color:'#fff',
        backgroundColor:'transparent'
    },
    whiteDurationColor:{
        color:'rgba(255,255,255,0.6)',
        backgroundColor:'transparent'
    },
    defaultColor:{
        backgroundColor:'transparent'
    },
    serviceName:{
        fontSize:20,
        fontFamily:'Futura',
        color:'#6c6c6c'
    },
    servicePrice:{
        textAlign:'right',
        fontSize:22,
        paddingRight:20,
        color:'#F069A2',
    },
    serviceRight:{
        width:serviceRightWidth
    },
    serviceLeft:{
        paddingLeft:20
    },
    serviceDuration:{
        color:'#aeaeae',
        fontSize:16,
        marginTop:5
    },
    serviceItemSelected:{
        backgroundColor:'transparent',
    },
    containerItemSelected:{
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor:'#fff',
        flexDirection: 'row',
        marginBottom:15,
        paddingTop:20,
        paddingBottom:20,
        borderRadius:4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2
    },
    btnSave: {
        height: 60,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        marginBottom: 0,
        marginLeft:20,
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:40
    },
    btnSaveText: {
        color: "#EF75A4",
        fontSize: 30,
        zIndex: 1,
        backgroundColor: "transparent",
        fontFamily:'Futura'
        
    },
    btnSaveWraper: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:40
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden",
        flex: 1
        
    },
    btnBlockWraper:{
        height:60,
        marginLeft:15,
        marginBottom:15
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    btnLinearConfirm: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1
        
    },
    btnSaveTextConfirm: {
        color: "#fff",
        fontSize: 30,
        zIndex: 1,
        backgroundColor: "transparent",
        fontFamily:'Futura'
    }
    
})