import React from "react";
import { StyleSheet ,Text, View, TouchableOpacity } from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons";
//import layout from "../assets/styles/layout";

export default class TechnicianSearchItem extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.id,this.props.name);
    };

    render() {
        return(
            <TouchableOpacity activeOpacity={1} onPress={() => this._onPress()}>
                <View style={styles.itemContainer}>
                    <MaterialCommunityIcons
                        name={'account-outline'}
                        size={20}
                        color={(this.props.selected ? '#6b6b6b' : '#CFD4DA')}
                    />
                    <Text style={styles.technicianname}>{this.props.name}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        height:50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#CFD4DA',
        borderBottomWidth: 0.5,
        paddingLeft:10,
        paddingRight:10
    },
    technicianname:{
        marginLeft:5,
        color:'#6b6b6b',
        fontSize:16
    }
});
