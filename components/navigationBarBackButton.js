import React from "react";
import { StyleSheet, Text, View, TouchableOpacity,Platform } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import layout from "../assets/styles/layout";
import { withNavigation } from "@expo/ex-navigation";

@withNavigation
export default class NavigationBarBackButton extends React.Component {
    back = () => {
        let rootNavigator = this.props.navigation.getNavigator(this.props.navigatorid);
        rootNavigator.pop();
    }
    
    render() {
        //console.log(this.props.technicianList);
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={
                        layout.headerNavLeftContainer
                    }
                    activeOpacity={1}
                    onPress={() => this.back()}
                >
                    <View style={layout.headerNavLeft}>
                        <MaterialCommunityIcons
                            name={"arrow-left"}
                            size={30}
                            color={
                                "rgba(255,255,255,1)"
                            }
                            style={
                                Platform.OS ===
                                "android"
                                    ? layout.navIcon
                                    : layout.navIconIOS
                            }
                        />
                    </View>
                </TouchableOpacity>
                <View style={styles.navContainer}>
                    <Text style={layout.navTitleText}>
                        {typeof this.props.title == 'object' ? this.props.title.main : this.props.title}
                    </Text>
                    {
                        typeof this.props.title == 'object' 
                        && this.props.title.alt != "" &&
                        <Text style={styles.navTitleText}>
                            {this.props.title.alt}
                        </Text>
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    navContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navTitleText:{
        color: '#fff',
        fontSize:14
    }
});
