import React from "react";
import { StyleSheet ,Text, View, TouchableOpacity } from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons";
//import layout from "../assets/styles/layout";

export default class AppointmentTechnicianSearchItem extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.id,this.props.name);
    };

    render() {
        let turnCountDisplay = false;
        let isServingDisplay = false;
        if(this.props.userData.isManageTurn && this.props.data.id > 0){
            turnCountDisplay = (
                <Text style={styles.technicianturn}>
                    ({this.props.data.turn})
                </Text>    
            );
        }

        if(this.props.userData.isManageTurn && this.props.data.id > 0 && this.props.data.isServing){
            isServingDisplay = (
                <Text style={styles.technicianserving}>
                    Serving
                </Text>    
            );
        }else if(this.props.userData.isManageTurn && this.props.data.id > 0 && !this.props.data.isOnline){
            isServingDisplay = (
                <Text style={styles.technicianstatus}>
                    Not Online
                </Text>    
            );
        }

        
       
        return(
            <TouchableOpacity activeOpacity={1} onPress={() => this._onPress()}>
                <View style={styles.itemContainer}>
                    <MaterialCommunityIcons
                        name={'account-outline'}
                        size={20}
                        color={(this.props.selected ? '#6b6b6b' : '#CFD4DA')}
                    />
                    <Text style={styles.technicianname}>
                        {this.props.name}
                    </Text>
                    {turnCountDisplay}
                    {isServingDisplay}
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        height:50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#CFD4DA',
        borderBottomWidth: 0.5,
        paddingLeft:10,
        paddingRight:10
    },
    technicianname:{
        marginLeft:5,
        color:'#6b6b6b',
        fontSize:16
    },
    technicianturn:{
        color:'red',
        fontSize:16,
        marginLeft:5
    },
    technicianserving:{
        color:'#fff',
        fontSize:16,
        marginLeft:5,
        backgroundColor:'blue',
        paddingTop:2,
        paddingBottom:2,
        paddingLeft:5,
        paddingRight:5
    },
    technicianstatus:{
        color:'#7878',
        fontSize:16,
        marginLeft:5,
        
        paddingTop:2,
        paddingBottom:2,
        paddingLeft:5,
        paddingRight:5
    }
});
