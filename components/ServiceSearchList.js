import React from "react";
import {StyleSheet, Text, TouchableOpacity, View, Modal, Platform, TextInput, FlatList, SectionList } from "react-native";
import ServiceSearchItem from "./ServiceSearchItem";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {LinearGradient} from "expo";
import layout from "../assets/styles/layout";
import { getTextByKey } from "../helpers/language";

export default class ServiceSearchList extends React.Component {

    state = {
        modalVisible: false,
        showCloseSearchBoxClient: false,
        search:'',
        selected: this.props.selected,
        currentService: this.props.currentService
    }

    services = this.props.data;
    categories = this.props.categories;
    servicelist = [];
    selectedId = 0;
    async componentWillMount(){
        this.processData('');
        this.setState({rerender: true});
    }

    show = (refdata,id) => {
        this.selectedId = id;
        this.setState({currentService: refdata, modalVisible: true});
    }

    processData = (searchtext) => {
        
        let servicesClone = [...this.services];
        let _this = this;
        this.categories.forEach(function(category){
            let serviceInCategories = servicesClone.filter(function(item){
                if(category != 'Combo'){
                    if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                        if(searchtext != ''){
                            //if( (typeof this.state.search == 'undefined' || (this.state.search != 'undefined' && item.fullname.toLowerCase().indexOf(this.state.search.toLowerCase()) >= 0)))
                            return item.category_customname == category && item.service_name.toLowerCase().indexOf(searchtext.toLowerCase()) >= 0;
                        }
                        return item.category_customname == category;
                    }else{
                        if(searchtext != ''){
                            return item.category_name == category && item.service_name.toLowerCase().indexOf(searchtext.toLowerCase()) >= 0;
                        }
                        return item.category_name == category;
                    }
                }else{
                    if(searchtext != ''){
                        return item.isCombo && item.service_name.toLowerCase().indexOf(searchtext.toLowerCase()) >= 0;
                    }
                    return item.isCombo;
                }
            });

            if(serviceInCategories.length){
                serviceData = {};
                serviceData.key = category;
                serviceData.data = serviceInCategories;
                _this.servicelist.push(serviceData);
            }
        });
    }

    close() {
        this.props.onClose();
        this.setState({modalVisible: false});
    }

    getText(key){
        return getTextByKey(this.props.language,key);
    }

    /*
    show = () => {
        this.setState({modalVisible: true});
    }*/

    setCurrentService = (id) => {
        this.props.currentService = id;
    }

    _keyExtractor = (item, index) => item.id;

    _onPressItem = (id, name, price,duration,isCombo,rewardpoint, rewardpointvip) => {
        // updater functions are preferred for transactional updates
        //console.log(id);

        this.props.onSelected(id, name, price,duration, isCombo, this.state.currentService, rewardpoint, rewardpointvip);
        //this.setState({modalVisible: false});
        //this.props.selected = id;
        //this.setState({selected:id});
        //console.log(this.state);
    };

    _renderItem = ({item}) => {

        return (
            <ServiceSearchItem
                id={item.id}
                onPressItem={this._onPressItem}
                selected={(item.id == this.selectedId)}
                name={item.service_name}
                price={item.price}
                isCombo={item.isCombo}
                duration={item.duration}
                rewardpoint={item.rewardpoint}
                rewardpointvip={item.rewardpointvip}
            />
        )

    };

    clearSearch = () => {
        this.refs['searchtextinput'].clear();
        this.setState({showCloseSearchBoxClient: false});
        //this.state.search = '';
        this.setState({search: ''});
    }

    changeSearchText = (searchtext) => {
        this.servicelist = [];
        this.processData(searchtext);
      
        if (String.prototype.trim.call(searchtext) == '') {
            this.setState({showCloseSearchBoxClient: false});
        } else {
            this.setState({showCloseSearchBoxClient: true});
        }
        //console.log(searchtext);
        //this.setState({search: searchtext});
        //this.state.search = searchtext;
        //this.setState({modalVisible: true});
        //this.refs['listtechnician'].props.search = searchtext;
    }

    onDelete = () => {
        this.props.onDelete(this.state.currentService);
        //console.log(this.amount);
    };

    _renderSectionHeader = sectionHeader => {
        /*
        var sectionName = "";
        var profilepicture = false;
        var picture = "";
        // console.log(sectionHeader.section);
        if (this.state.byday) {
            let sectionInfo = this.props.sectiondata.first(tech => {
                return tech.id == sectionHeader.section.key;
            });
            this.currentTechId = sectionInfo.id;

            sectionName = sectionInfo.fullname;
            picture = sectionInfo.picture;
        } else {
            sectionName = moment(sectionHeader.section.key).format(
                "ddd DD MMM"
            );
            //console.log(sectionHeader);
        }*/
        //console.log(sectionHeader.section.data.length);
//{sectionName}
        //console.log(sectionHeader);

        //console.log(sectionHeader.section.key);
        return (
            <View>
                <View style={styles.sectionheader}>
                    <View style={styles.sectionheadertext}>
                        <View style={styles.sectionheadertextcontainer}>
                            <Text style={styles.sectionheadertextcontent}>
                                {sectionHeader.section.key}
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    };

    render() {
        //console.log(this.props.data);
        let serviceidrep = this.selectedId.toString().replace('service_','').replace('combo_','');
        if(serviceidrep != ''){
            serviceidrep = parseInt(serviceidrep);
        }
        return(
            <Modal
                animationType={"none"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}

            >
                <View style={(Platform.OS === 'android' ? layout.headercontainerAndroid : layout.headercontainer )}>
                    <LinearGradient start={[0, 0]} end={[1, 0]} colors={['#F069A2', '#EEAEA2']}
                                    style={( Platform.OS === 'android' ? layout.headerAndroid : layout.header)}>
                        <View style={layout.headercontrols}>
                            <TouchableOpacity style={layout.headerNavLeftContainer} activeOpacity={1}
                                              onPress={() => this.close()}>
                                <View style={layout.headerNavLeft}>
                                    <MaterialCommunityIcons
                                        name={'close'}
                                        size={30}
                                        color={'rgba(255,255,255,1)'} style={(Platform.OS === 'android' ? layout.navIcon : layout.navIconIOS)}
                                    />
                                </View>
                            </TouchableOpacity>
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={layout.headertitle}>{this.getText('selectserviceaddappointment')}</Text>
                            </View>
                            {
                                serviceidrep > 0 &&
                                <TouchableOpacity
                                    style={layout.headerNavRightContainer}
                                    activeOpacity={1}
                                    onPress={() => this.onDelete()}
                                >
                                    <View style={layout.headerNavRight}>
                                        <Text style={layout.headerNavText}>
                                        {this.getText('delete')}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.searchContainer}>
                    <View style={[layout.searchContainer]}>
                        <MaterialCommunityIcons
                            name={'magnify'}
                            size={20}
                            color={'#6b6b6b'} style={layout.iconsearchbox}
                        />
                        <TextInput
                            placeholder={this.getText('SearchService')} placeholderTextColor='#6b6b6b'
                            underlineColorAndroid={'transparent'}
                            style={layout.searchbox}
                            onChangeText={(searchtext) => this.changeSearchText(searchtext)}
                            ref={'searchtextinput'}
                        />

                        {this.state.showCloseSearchBoxClient &&
                        <TouchableOpacity style={layout.iconclosesearchbox} activeOpacity={1}
                                          onPress={() => this.clearSearch()}>
                            <MaterialCommunityIcons
                                name={'close-circle-outline'}
                                size={20}
                                color={'#6b6b6b'}
                            />
                        </TouchableOpacity>
                        }
                    </View>
                </View>
                <View style={{flex:1}}>
                {/*
                    <SectionList
                        data={this.props.data}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                        extraData={(this.state )}
                        initialNumToRender={10}
                        renderSectionHeader={this._renderSectionHeader}
                    />
                */}

                    <SectionList
                        ref="sectionService"
                        renderItem={this._renderItem}
                        renderSectionHeader={this._renderSectionHeader}
                        keyExtractor={this._keyExtractor}
                        sections={this.servicelist}
                        //getItemLayout={this._getItemLayout}
                        stickySectionHeadersEnabled={true}
                    />
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionheader: {
        height: 35,
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: "#F2F2F2"
    },
    sectionheadertext: {
        marginLeft: 15,
        flexDirection: "row"
    },
    sectionheadertextcontainer: {
        height: 35,
        justifyContent: "center"
    },
    sectionheadertextcontent: {
        color: "#808080",
        fontSize:16
    },
    searchContainer:{
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd'
    }
});
