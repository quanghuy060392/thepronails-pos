import React from "react";
import { StyleSheet, Text, TouchableOpacity, View, Modal, Platform, TextInput, FlatList, Dimensions, ScrollView } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo";
import layout from "../assets/styles/layout";
import moment from "moment";
import CalendarStrip from 'react-native-calendar-strip';
import CalendarStripHeader from "../components/CalendarStripHeader";
import ModalCalendar from '../components/ModalCalendar';
import { getTextByKey } from "../helpers/language";

export default class AppointmentSelectTime extends React.Component {
    state = {
        modalVisible: true,
        dateselect: this.props.selectedDate,
        hourselect: this.props.selectedHour,
        istime: ''
    }
    /*
    datesWhitelist = [{
        start: moment().startOf('week').add(0, 'days'),
        end: moment().startOf('week').add(6, 'days')  // total 4 days enabled
    }];*/
    columnWidth = Dimensions.get('window').width / 3;
    startDateWeek = moment().startOf('week');
    focusedDay = this.props.selectedDate;
    close() {
        this.props.onClose();
        this.setState({ modalVisible: false });
    }

    show = (istime) => {
        this.setState({ modalVisible: true, istime: istime });
    }

    calendarHeader = (title) => {
        return (
            <CalendarStripHeader toDayOnPress={this.toDayOnPress} onPress={this.calendarStripHeaderPress} headerTitle={title} />
        )
    }

    toDayOnPress = () => {
        this.focusedDay = moment();
        this.setState({ dateselect: moment() })
    }

    calendarStripHeaderPress = () => {
        //console.log(this.refs.CalendarStripByWeek.getSelectedDate());
        //console.log(this.refs.CalendarStripByWeek.getSelectedDate());
        //this.refs.modalCalendarByWeek.props.date = this.refs.CalendarStripByWeek.getSelectedDate();
        this.refs.modalCalendarByWeek.show(this.refs.CalendarStripByWeek.getSelectedDate());
    }

    onSelectDateModalByWeek = (date) => {
        //this.refs.CalendarStripByWeek.setSelectedDate(date);
        this.focusedDay = moment(date);
        this.setState({ dateselect: moment(date) })
        //console.log(date);
    }

    calendarStripSelectedDate = (date) => {
        this.focusedDay = date;
        this.setState({ dateselect: date })
    }

    onPressTime = (hour) => {
        //console.log(this.state.dateselect.format('DD'));
        //console.log(this.convertTo24Hour(hour));
        this.props.onPress(this.state.dateselect,hour, this.state.istime);
    }

    changeWeek = week => {
        this.startDateWeek = week.clone();
        //this.endDateWeek = week.clone().add(6, "days");
        this.focusedDay = this.startDateWeek
            .clone()
            .add(this.focusedDay.day(), "days");
  
        this.refs.CalendarStripByWeek.setSelectedDate(this.focusedDay);
        
        //startDateWeek
        //this.refs.CalendarStripByWeek.setSelectedDate(date);
        //this.focusedDay = date;
    };

    /*
    convertTo24Hour(time) {
        time = time.toLowerCase();
        var hours = time.substr(0, 2);
        if (time.indexOf('am') != -1 && hours == 12) {
            time = time.replace('12', '0');
        }
        if (time.indexOf('pm') != -1 && parseInt(hours) < 12) {
            time = time.replace(hours, (parseInt(hours) + 12));
        }
        return time.replace(/(am|pm)/, '');
    }*/

    getText(key){
        return getTextByKey(this.props.language,key);
    }

    render() {
        //console.log(this.state.dateselect);
        let dayname = this.state.dateselect.format('dddd').toLowerCase();
        let availablehours = this.props.data[dayname];
        //let appointmentHours = '';
        //console.log(dayname);
        let hourSelected = this.state.hourselect;
        let appointmentHours = availablehours.map((x, i) => {
            let hourstyle = hourSelected == x ? styles.hourSelected : styles.hours;
            let hourTextstyle = hourSelected == x ? styles.hourTextSelected : styles.hoursText;
            return (
                <TouchableOpacity key={x} activeOpacity={1} onPress={() => {this.onPressTime(x)}}>
                    <View style={[hourstyle, { width: this.columnWidth  }]}>
                        <Text style={hourTextstyle}>{x}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
        )
        //console.log(availablehours);
        //let hour =
        //console.log(this.columnWidth);
        return (
            <Modal
                animationType={"none"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}

            >
                <View style={(Platform.OS === 'android' ? layout.headercontainerAndroid : layout.headercontainer)}>
                    <LinearGradient start={[0, 0]} end={[1, 0]} colors={['#F069A2', '#EEAEA2']}
                        style={(Platform.OS === 'android' ? layout.headerAndroid : layout.header)}>
                        <View style={layout.headercontrols}>
                            <TouchableOpacity style={layout.headerNavLeftContainer} activeOpacity={1}
                                onPress={() => this.close()}>
                                <View style={layout.headerNavLeft}>
                                    <MaterialCommunityIcons
                                        name={'close'}
                                        size={30}
                                        color={'rgba(255,255,255,1)'} style={(Platform.OS === 'android' ? layout.navIcon : layout.navIconIOS)}
                                    />
                                </View>
                            </TouchableOpacity>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={layout.headertitle}>{this.getText('selecttimetitle')}</Text>
                            </View>
                        </View>
                    </LinearGradient>
                </View>

                <View style={{ flex: 1 }}>
                    <View style={styles.calendarcontainer}>
                        <CalendarStrip 
                        //datesWhitelist={this.datesWhitelist}
                            startingDate={this.startDateWeek}
                            useIsoWeekday={false}
                            calendarHeaderFormat="MMM Y" style={{ flex: 1 }}
                            dateNameStyle={styles.dateNameStyle}
                            dateNumberStyle={styles.dateNumberStyle}
                            dateNumberStyleText={styles.dateNumberStyleText}
                            calendarHeaderStyle={styles.calendarHeaderStyle}
                            selectedDate={this.state.dateselect}
                            highlightDateNameStyle={styles.selectedDate}
                            highlightDateNumberStyle={styles.highlightDateNumberStyle}
                            highlightDateNumberStyleText={styles.highlightDateNumberStyleText}
                            calendarHeader={this.calendarHeader}
                            onDateSelected={this.calendarStripSelectedDate}
                            onWeekChanged={this.changeWeek}
                            ref="CalendarStripByWeek" />
                    </View>
                    <View style={{ height: 35, alignItems: 'center', backgroundColor: '#F2F2F2', justifyContent: 'center' }}>
                        <Text style={{ color: '#808080', }}>{this.getText('availablehours')}</Text>
                    </View>
                    <View style={{flex:1}}>
                        <ScrollView contentContainerStyle={styles.hourscontainer}>
                            {appointmentHours}
                        </ScrollView>
                    </View>
                </View>
                <ModalCalendar ref="modalCalendarByWeek" onPress={this.onSelectDateModalByWeek} />
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    calendarcontainer: {
        height: 93,
        paddingTop: 10,
        paddingBottom: 0,
        borderWidth: 0.5,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd',
    },
    dateNameStyle: {
        color: '#333',
        fontWeight: 'normal',
        fontSize: 14
    },
    dateNumberStyle: {
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    dateNumberStyleText: {
        color: '#333',
        fontWeight: 'normal',
        fontSize: 14,
    },
    calendarHeaderStyle: {
        color: '#333',
        fontWeight: 'normal',
        fontSize: 16
    },
    selectedDate: {
        color: '#F069A2',
        fontWeight: 'normal',
        fontSize: 14
    },
    highlightDateNumberStyle: {
        width: 24,
        height: 24,
        borderRadius: 24,
        backgroundColor: '#F069A2',

        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    highlightDateNumberStyleText: {
        color: '#fff',
        fontSize: 14,
        fontWeight: 'normal',
    },
    hourscontainer: {
       
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    hours: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        //borderRightWidth: 0,
        borderColor: '#ddd',
    },
    hourSelected:{
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        backgroundColor: '#F069A2',
        borderColor: '#ddd',
        
    },
    hourTextSelected:{
        color:'#fff'
    },
    hoursText:{
        color:'#333'
    }
});
