import React from "react";
import { StyleSheet ,Text, View, TouchableOpacity } from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons";
//import layout from "../assets/styles/layout";

export default class ServiceSearchItem extends React.Component {
    _onPress = () => {
   
        //this.props.onPressItem(this.props.id,this.props.name,this.props.price,this.props.duration,this.props.rewardpoint, this.props.isCombo);
        this.props.onPressItem(this.props.id,this.props.name,this.props.price,this.props.duration,this.props.isCombo, this.props.rewardpoint, this.props.rewardpointvip);
    };

    render() {
        return(
            <TouchableOpacity activeOpacity={1} onPress={() => this._onPress()}>
                <View style={styles.itemContainer}>
                    {this.props.selected &&
                        <MaterialCommunityIcons
                            name={'check'}
                            size={20}
                            color={'#F069A2'} 
                        />
                    }
                    <Text style={styles.technicianname}>{this.props.name}</Text>
                    <Text style={styles.serviceprice}>${this.props.price}</Text>
                    <Text style={styles.servicepoint}>{this.props.point} Points</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        height:50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#CFD4DA',
        borderBottomWidth: 0.5,
        paddingLeft:10,
        paddingRight:10
    },
    technicianname:{
        marginLeft:5,
        color:'#6b6b6b',
        fontSize:16
    },
    serviceprice:{
        position:'absolute',
        right:100,
        color:'#F069A2',
        fontSize:16
    },
    servicepoint:{
        position:'absolute',
        right:15,
        color:'#F069A2',
        fontSize:16
    }
});
