import React, {Component} from "react";
import {StyleSheet, Text, View, TextInput, Animated, Platform, TouchableOpacity} from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons";

class FloatingLabel extends Component {
    
    render() {
        return (
            <View
                style={[styles.floatingLabel]}>
                {this.props.children}
            </View>
        );
    }
}

class TextFieldHolder extends Component {
    
    render() {
        return (
            <View>
                {this.props.children}
            </View>
        );
    }
}

class FloatLabelSelectField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            text: this.props.value,
            //id: this.props.id,
            //refdata: this.props.refdata
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.hasOwnProperty('value') && newProps.value !== this.state.text) {
            this.setState({text: newProps.value})
        }
    }

    withBorder() {
        if (!this.props.noBorder) {
            return styles.withBorder;
        }
    }

    onPress = () => {
        if(this.props.hasOwnProperty('onPress'))
        {
            this.props.onPress();
        }

        if(this.props.hasOwnProperty('onPressDynamic'))
        {
            this.props.onPressDynamic(this.props.id,this.props.refdata);
        }
    }

    render() {
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.onPress} style={styles.container}>
            <View style={{flex:1}}>
                <View style={[styles.viewContainer,this.withBorder()]}>
                    <View style={[styles.paddingView,this.props.paddingStyle]} />
                    <View style={[styles.fieldContainer]}>
                        

                        <TextFieldHolder withValue={(this.state.text && this.props.placeholder)} style={[styles.valueContainer]}>
                            
                                <View>
                                    <Text style={[(this.state.text ? styles.valueText :  styles.valueTextPlaceHolder)]}>
                                        {this.state.text ? this.state.text  : this.props.placeholder }
                                    </Text>

                                </View>
                            
                        </TextFieldHolder>
                        {
                            (this.props.placeholder != "") &&
                            <FloatingLabel visible={this.state.text}>
                                <Text style={[styles.fieldLabel, this.labelStyle()]}>{this.placeholderValue()}</Text>
                            </FloatingLabel>
                        }
                        <View style={styles.iconRight}>
                            <MaterialCommunityIcons
                                name={'chevron-right'}
                                size={22}
                                color={'rgba(0,0,0,0.6)'}
                            />
                        </View>
                    </View>
                </View>
            </View>
            </TouchableOpacity>
        );
    }



    labelStyle() {
        if (this.state.focused) {
            return styles.focused;
        }
    }

    placeholderValue() {
        if (this.state.text) {
            return this.props.placeholder;
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 45,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    viewContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    paddingView: {
        width: 15
    },
    floatingLabel: {
        position: 'absolute',
        top: 0,
        right:35,
        bottom:0,
        justifyContent: 'center',
    },
    fieldLabel: {
        /*
        height: 15,
        fontSize: 10,
        color: '#F069A2'*/
        fontSize: 16,
    },
    fieldContainer: {
        flex: 1,
        justifyContent: 'center',
        position: 'relative'
    },
    withBorder: {
        borderBottomWidth: 1 / 2,
        borderColor: '#C8C7CC',
    },
    valueContainer:{
        height: (Platform.OS == 'ios' ? 20 : 60),
        backgroundColor:'red'
    },
    valueText: {
        /*height: (Platform.OS == 'ios' ? 20 : 60),
        fontSize: 16,
        color: '#111111',
        backgroundColor:'red'*/
        fontSize:16,
        color:"#333",
        marginRight:80
    },
    valueTextPlaceHolder:{
        color:'#808080',
        fontSize:16
    },
    focused: {
        color: "#F069A2"
    },
    iconRight: {
        position:'absolute',
        right:10,
        bottom:0,
        top:0,
        justifyContent: 'center'
    }
});

export default FloatLabelSelectField;