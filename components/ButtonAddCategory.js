import React, { Component } from 'react';
import {StyleSheet, Text, TouchableOpacity, View } from "react-native";
import layout from "../assets/styles/layout";
import { getTextByKey } from '../helpers/language';

export default class ButtonAddCategory extends React.Component{
    state = {
        isShowPlusService: true,
        count: 0
    }
    userData = this.props.userData;
    onPress = () => {
        this.props.onPress(0,"");
    }

    render(){
        if(this.userData.rewardpointDailyCheckInType == "bycategories")
        {
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.onPress}>
                    <View style={[layout.floatGroup,styles.plusservicecontainer]}>
                        <Text style={styles.plusservice}>+ {getTextByKey(this.props.language,'addcategory')}</Text>
                    </View>
                </TouchableOpacity>

            );
        }
        return false;
    }
}


const styles = StyleSheet.create({
    plusservicecontainer:{
        justifyContent: 'center',
        borderBottomWidth: 1 / 2,
        borderColor: '#C8C7CC',
        paddingLeft:15
    },
    plusservice:{
        color:'#F069A2',
    }
});