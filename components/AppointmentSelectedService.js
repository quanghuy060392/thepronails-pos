import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import layout from "../assets/styles/layout";
import FloatLabelSelect from "../components/FloatSelectInputPriceMultiple";

export default class AppointmentSelectedService extends React.Component {
    state = {
        selectServices: this.props.selectServices
    };

    /*
    onPress = () => {
        this.props.onPress(0,'service_' + this.state.count);
    }*/

    userData = this.props.userData;
    services = this.props.services;
    combos = this.props.combos;
    

    onPressService = (id, refdata) => {
        this.props.onPress(id, refdata);
    };

    onPressTechnician = (id, refdata) => {
        this.props.onPressTechnician(id, refdata);
    };

    /*
    selectedService = (name,id) => {

    }*/

    render() {
        //console.log(this.state.selectServices);
        let selectServicesData = this.state.selectServices;
        let selectServices = Object.keys(
            this.state.selectServices
        ).map((x, i) => {
            let data = selectServicesData[x];
            let dataId = parseInt(data.id.toString().replace('service_','').replace('combo_',''));
            let pricedisplay = "";
            //let layoutstyle = layout.floatGroup;
            let layoutstyle = '';
            /*
            if(dataId > 0 && this.userData.role == 4) {
                layoutstyle = layout.floatDoubleGroup;
                if(i > 0){
                    layoutstyle = layout.floatDoubleGroupMaginTop;
                }
            };*/
            //styles.hrline
            if(i > 0){
                layoutstyle = styles.hrline;
            }
            if (data.price > 0) pricedisplay = "$" + data.price;
            let comboDisplay = '';
            

            return (
                <View key={x} style={layoutstyle}>
                    <View style={layout.floatGroup}>
                            <FloatLabelSelect
                                placeholder={pricedisplay}
                                value={data.service_name}
                                id={data.id}
                                refdata={x}
                                onPressDynamic={this.onPressService}
                                ref={x}
                                //ref="clientInput"
                            />
                        </View>
                    
                    {
                        dataId > 0 && this.userData.role == 4 && !data.isCombo
                        &&
                        <View style={layout.floatGroup}>
                            <FloatLabelSelect
                                placeholder={''}
                                value={data.technicianName}
                                id={data.technicianId}
                                refdata={x}
                                onPressDynamic={this.onPressTechnician}
                                ref={x}
                                //ref="clientInput"
                            />
                        </View>
                    }

                    {
                        dataId > 0 && this.userData.role == 4 && data.isCombo
                        && typeof data.servicesIncombo != 'undefined' &&
                        data.servicesIncombo.map((serviceitem, i) => {
                            return (
                                <View style={layout.floatDoubleGroup}
                                     key={serviceitem.serviceid + '_' + serviceitem.sp_combo_id}>
                                    <View style={[layout.floatGroup,styles.lblservicewraper]}>
                                        <Text style={styles.lblservice}>{serviceitem.servicename}</Text>
                                    </View>
                                    <View style={layout.floatGroup}>
                                        <FloatLabelSelect
                                            placeholder={''}
                                            value={serviceitem.technicianName}
                                            id={serviceitem.technicianId}
                                            refdata={x + '@combo' + serviceitem.serviceid}
                                            onPressDynamic={this.onPressTechnician}
                                            ref={serviceitem}
                                            //ref="clientInput"
                                        />
                                    </View>
                                </View>
                                
                            )        
                        })
                        
                    }
                </View>
            );
        });

        return (
            <View style={{backgroundColor:'#f2f2f2'}}>
                {selectServices}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    plusservicecontainer: {
        justifyContent: "center",
        borderBottomWidth: 1 / 2,
        borderColor: "#C8C7CC",
        paddingLeft: 15
    },
    plusservice: {
        color: "#F069A2"
    },
    lblservicewraper:{
        paddingLeft:15,
        justifyContent:'center',
        backgroundColor:'#fff',
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd'
    },
    lblservice:{
        
        fontSize:16
        
    },
    hrline:{
        marginTop: 10,
        borderWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd'
    },
    servicetext:{
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        backgroundColor:'#fff',
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd',
        alignItems:'center'
    },
    lefttext:{
        position:'absolute',
        left:15,
        fontSize:16
    },
    righttext:{
        position:'absolute',
        right:35,
        fontSize:16
    }
    
});
