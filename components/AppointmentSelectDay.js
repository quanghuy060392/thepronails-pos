import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { getLanguageName, getLanguage, getTextByKey } from "../helpers/language";

export default class AppointmentSelectDay extends React.Component {
    state = {
        byDayText: this.props.byDayText,
        isToday: this.props.isToday
    };

    changeDate = (rule) => {
        this.props.changeDate(rule);
    }

    _today = () => {
        this.props.today();    
    }

    render() {
        return (
            <View style={styles.tabHeader}>
                <View style={styles.tabHeaderControlsInline}>
                    <Text
                        style={[
                            styles.ByDateToday,
                            this.state.isToday
                                ? styles.isToday
                                : styles.notToday
                        ]}
                        onPress={() => this._today()}
                    >
                        {getTextByKey(this.props.language,'today')}
                    </Text>
                    <TouchableOpacity
                        style={styles.tabHeaderControlsInlineIconLeft}
                        onPress={() => this.changeDate("prev")}
                        activeOpacity={0.5}
                    >
                        <MaterialCommunityIcons
                            name={"chevron-left"}
                            size={22}
                            color={"rgba(0,0,0,0.6)"}
                        />
                    </TouchableOpacity>
                    <Text style={styles.bydaytext}>
                        {this.state.byDayText}
                    </Text>
                    <TouchableOpacity
                        style={styles.tabHeaderControlsInlineIconRight}
                        onPress={() => this.changeDate("next")}
                        activeOpacity={0.5}
                    >
                        <MaterialCommunityIcons
                            name={"chevron-right"}
                            size={22}
                            color={"rgba(0,0,0,0.6)"}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabHeader: {
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: "#ddd"
    },
    tabHeaderControlsInline: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    tabHeaderControlsInlineIconLeft: {
        marginRight: 10,
        marginTop: 2
    },
    tabHeaderControlsInlineIconRight: {
        marginLeft: 10,
        marginTop: 2
    },
    ByDateToday: {
        position: "absolute",
        left: 15
    },
    isToday: {
        color: "#F069A2"
    },
    notToday: {
        color: "rgba(0,0,0,0.5)"
    }
});
