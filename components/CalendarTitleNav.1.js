import React from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
    Platform,
    TextInput,
    Alert
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo";
import layout from "../assets/styles/layout";
import TechnicianSearchList from "./TechnicianSearchList";

export default class CalendarTitleNav extends React.Component {
    state = {
        modalVisible: false,
        showCloseSearchBox: false,
        technicianSelected: 0,
        technicianFilterName: "All Technician"
    };

    userdata = this.props.userdata;

    close() {
        this.setState({ modalVisible: false });
        //Alert.alert('Error', 'Please enter password');
    }

    showListTechnician = () => {
        if (this.userdata.role == 4) {
            if (this.state.modalVisible) {
                this.setState({ modalVisible: false });
            } else this.setState({ modalVisible: true });
        }
    };

    changeSearchText = searchtext => {
        if (String.prototype.trim.call(searchtext) == "") {
            this.setState({ showCloseSearchBox: false });
        } else {
            this.setState({ showCloseSearchBox: true });
        }
        this.searchtext = searchtext;
        this.refs["listtechnician"].props.search = searchtext;
    };

    clearSearch = () => {
        this.refs["searchtextinput"].clear();
        this.setState({ showCloseSearchBox: false });
        this.searchtext = "";
    };

    _onSelectedTechnician = (id, name) => {
        //console.log(name);
        this.props.onSelectTechnician(id);
        //this.setState({technicianSelected: id});
        this.setState({
            technicianSelected: id,
            technicianFilterName: name,
            modalVisible: false
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    onPress={() => this.showListTechnician()}
                    activeOpacity={1}
                >
                    <View style={layout.navContainer}>
                        {typeof this.userdata != "undefined" &&
                            this.userdata.role == 4 &&
                            <Text style={layout.navTitleText}>
                                {this.state.technicianFilterName}
                            </Text>}
                        {typeof this.userdata != "undefined" &&
                            this.userdata.role == 4 &&
                            <MaterialCommunityIcons
                                name={"menu-down"}
                                size={20}
                                color={"rgba(255,255,255,0.5)"}
                                style={layout.navIcon}
                            />}
                        {typeof this.userdata != "undefined" &&
                            this.userdata.role == 9 &&
                            <Text style={layout.navTitleText}>
                                Appointments
                            </Text>}
                    </View>
                </TouchableOpacity>

                <Modal
                    animationType={"slide"}
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.close()}
                >
                    <View
                        style={
                            Platform.OS === "android"
                                ? layout.headercontainerAndroid
                                : layout.headercontainer
                        }
                    >
                        <LinearGradient
                            start={[0, 0]}
                            end={[1, 0]}
                            colors={["#F069A2", "#EEAEA2"]}
                            style={
                                Platform.OS === "android"
                                    ? layout.headerAndroid
                                    : layout.header
                            }
                        >
                            <View style={layout.headercontrols}>
                                <TouchableOpacity
                                    style={layout.headerNavLeftContainer}
                                    activeOpacity={1}
                                    onPress={() => this.close()}
                                >
                                    <View style={layout.headerNavLeft}>
                                        <MaterialCommunityIcons
                                            name={"close"}
                                            size={30}
                                            color={"rgba(255,255,255,1)"}
                                            style={
                                                Platform.OS === "android"
                                                    ? layout.navIcon
                                                    : layout.navIconIOS
                                            }
                                        />
                                    </View>
                                </TouchableOpacity>
                                <View
                                    style={{
                                        flex: 1,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}
                                >
                                    <Text style={layout.headertitle}>
                                        Select Technician
                                    </Text>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>
                    <View>
                        <View style={layout.searchContainer}>
                            <MaterialCommunityIcons
                                name={"magnify"}
                                size={20}
                                color={"#6b6b6b"}
                                style={layout.iconsearchbox}
                            />
                            <TextInput
                                placeholder="Search Technician"
                                placeholderTextColor="#6b6b6b"
                                underlineColorAndroid={"transparent"}
                                style={layout.searchbox}
                                onChangeText={searchtext =>
                                    this.changeSearchText(searchtext)}
                                ref={"searchtextinput"}
                            />

                            {this.state.showCloseSearchBox &&
                                <TouchableOpacity
                                    style={layout.iconclosesearchbox}
                                    activeOpacity={1}
                                    onPress={() => this.clearSearch()}
                                >
                                    <MaterialCommunityIcons
                                        name={"close-circle-outline"}
                                        size={20}
                                        color={"#6b6b6b"}
                                    />
                                </TouchableOpacity>}
                        </View>
                    </View>
                    <View style={layout.listviewcontainer}>
                        <View style={layout.listview}>
                            <TechnicianSearchList
                                data={this.props.technicianList}
                                selected={this.state.technicianSelected}
                                search={this.searchtext}
                                onSelectedTechnician={
                                    this._onSelectedTechnician
                                }
                                ref={"listtechnician"}
                            />
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});
