import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
    Platform,
    TextInput,
    Alert,
    ScrollView,
    AsyncStorage,
    Dimensions,
    Keyboard
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo";
import layout from "../assets/styles/layout";
import ScrollableTabView from "react-native-scrollable-tab-view";
import HideTabBar from "../components/HideTabBar";
import FloatLabelTextInput from "../components/FloatTextInput";
import FloatLabelSelect from "../components/FloatSelectInput";
import ClientSearchList from "../components/ClientSearchList";
import ClientSearchModal from "../components/ClientSearchModal";
import ServiceSearchList from "../components/ServiceSearchList";
import AppointmentSelectTime from "../components/AppointmentSelectTime";
import ButtonAddService from "../components/ButtonAddService";
import CouponAppointment from "../components/CouponAppointment";
import AppointmentSelectedService from "../components/AppointmentSelectedService";
import moment from "moment";
import AppointmentTechnicianSearchList from "../components/AppointmentTechnicianSearchList";
import StatusSearchList from "../components/StatusSearchList";
import SubmitLoader from "../helpers/submitloader";
import IconLoader from "../helpers/iconloader";
import Colors from "../constants/Colors";
import setting from "../constants/Setting";
import { StackNavigation } from "@expo/ex-navigation";
import SpinnerLoader from "../helpers/spinner";
import FloatLabelSelectPrice from "../components/FloatSelectInputPrice";
import PaymentCash from "../components/PaymentCash";
import PaymentCreditCard from "../components/PaymentCreditCard";
import Paymentphysical from "../components/PaymentPhysical";
import AppointmentPaymentTotal from "../components/AppointmentPaymentTotal";
import AppointmentPaymentRemaining from "../components/AppointmentPaymentRemaining";
import { getDeviceId } from "../helpers/authenticate";
import { formatPhone } from "../helpers/Utils";
import emailvalidator from "email-validator";
import { getTextByKey } from "../helpers/language";
import {fetchLoadingCouponAvailable} from "../helpers/fetchdata";
export default class AddAppointment extends React.Component {
    InfoClient = "";
    clientName = "";
    technicianName = "";
    statusName = "";
    selectedTime = "";
    selectedClient = 0;
    selectedTechnician = 0;
    selectedStatus = "";
    selectedDate = moment();
    selectedHour = "";
    appointmentDate = "";
    appointmentHour = "";
    selectServices = {};
    combos = this.props.combos;
    payments = [];
    total = 0;
    remaining = 0;
    paid_total = 0;
    newPaymentAnimation = "none";
    cashAmount = "";
    creditcardAmount = "";
    physicalAmount = "";
    ccnumber = "";
    ccholdername = "";
    ccexpiredate = "";
    cccvv = "";
    ccnumberCN = "";
    ccholdernameCN = "";
    ccexpiredateCN = "";
    isShowLoaderAppointmentDetails = false;
    title = this.props.title;
    columnWidth = Dimensions.get("window").width / 2;
    fullWidth = Dimensions.get("window").width;
    deviceid = this.props.deviceid;
    userData = this.props.userData;
    clients = this.props.clients;
    isClientPhone = false;
    isValidClient = false;
    turns = [];
    promotions = {};
    promotionsApplied = {};
    paidcoupon = 0;
    state = {
        modalVisible: false,
        clientSelected: 0,
        serviceSelected: 0,
        appointmentId: 0
    };
    promocodegiftapply = "";
    giftcodeapply = {};
    rewardpointsalon = "";
    async componentWillMount() {
        this.selectServices = this.props.selectServices;
    }

    close() {
        //this.resetService();
        this.setState({ modalVisible: false });
    }

    show = () => {
        this.setState({ modalVisible: true });
    };

    /*
    open = async (appointmentId) => {
        this.setState({ modalVisible: true, appointmentId: appointmentId });
    };*/

    showLoaderAppointmentDetail = () => {
        if (typeof (this.refs.appointmentdetailloader == "undefined")) {
            this.isShowLoaderAppointmentDetails = true;
        } else {
        }
        //this.refs.appointmentdetailloader.setState({visible:true});
        this.clearForm();
        this.title = getTextByKey(this.props.language,'editappointmenttitle');
        this.setState({ modalVisible: true });
    };

    clearForm = () => {
        this.InfoClient = "";
        this.clientName = "";
        this.technicianName = "";
        this.statusName = "";
        this.selectedTime = "";
        this.selectedClient = 0;
        this.selectedTechnician = 0;
        this.selectedStatus = "";
        this.selectedDate = moment();
        this.selectedHour = "";
        this.appointmentDate = "";
        this.appointmentHour = "";
        this.payments = [];
        this.remaining = 0;
        this.total = 0;
        this.paid_total = 0;
        this.newPaymentAnimation = "none";
        this.cashAmount = "";
        this.creditcardAmount = "";
        this.physicalAmount = "";
        this.ccnumber = "";
        this.ccholdername = "";
        this.ccexpiredate = "";
        this.cccvv = "";
        this.ccnumberCN = "";
        this.ccholdernameCN = "";
        this.ccexpiredateCN = "";
        this.paidcoupon = 0;
        let firstService = {
            id: 0,
            service_name: getTextByKey(this.props.language,'selectserviceappointment'),
            price: 0,
            technicianId: 0,
            technicianName: getTextByKey(this.props.language,'anytech'),
            isCombo: false,
            rewardpoint: 0
        };
        this.promocodegiftapply = "";
        this.rewardpointsalon = "";
        this.giftcodeapply = {};
        this.promotions = {};
        this.promotionsApplied = {};
        this.selectServices = {};
        this.selectServices["service_0"] = firstService;
        if(this.userData.role == 9){
            this.selectedTechnician = this.userData.id;
        }
    };

    onPressClient = async () => {
        this.refs.tabs.goToPage(1);
        if (typeof this.refs.clientlist != "undefined") {
            this.refs.clientlist.setState({ selected: this.selectedClient });
            await this.refs.clientlist.show();
        }
        //this.refs.clientlist.show();
    };

    onCloseModalClientList = () => {
        this.refs.tabs.goToPage(0);
    };

    onCloseModalServiceList = () => {
        this.refs.tabs.goToPage(0);
    };
    /*
    _onSelectedClient = (id, name) => {
        this.refs.clientInput.setState({ text: name });
        this.clientName = name;
        this.refs.clientlist.setState({ modalVisible: false });
        //this.setState({clientSelected: id});
        this.selectedClient = id;
        this.refs.tabs.goToPage(0);
    };*/

    _onSelectedClient = (client, lbl) => {
        this.refs.clientInput.setState({text: lbl});
        Keyboard.dismiss();
        this.refs.clientlistmodal.show([],false,false, false,'');
        this.selectedClient = client.id;
        this.InfoClient = client;
        if(this.appointmentDate != ""){
            this._onGetCoupon();
        }
    }
    _onAppliedCoupon = (price, promotion) => {
        this.paidcoupon = price;
        this.promotionsApplied = promotion;
    }
    _onGetCoupon = async () =>{
        if(this.appointmentDate != '' && this.selectedClient != ""){
            var coupon = await fetchLoadingCouponAvailable(this.appointmentDate, this.selectedClient);
            this.promotions = {};
            if(coupon.membership.length > 0){
                let _this = this;
                coupon.membership.forEach(function(item){
                    let membershipitem = item;
                    if(typeof(_this.promotions['membership-'+membershipitem.id]) == 'undefined'){
                        _this.promotions['membership-'+membershipitem.id] = {
                            id: 'membership-'+membershipitem.id,
                            checked: false,
                            services: membershipitem.services,
                            name: membershipitem.title
                        }
                    }
                })
            } 
            if(coupon.discount.length > 0){
                let _this = this;
                coupon.discount.forEach(function(item){
                    let couponitem = item;
                    if(typeof(_this.promotions['coupon-'+couponitem.code]) == 'undefined'){
                        _this.promotions['coupon-'+couponitem.code] = {
                            id: 'coupon-'+couponitem.code,
                            checked: false,
                            discountvalue: parseFloat(couponitem.amount),
                            discounttype: couponitem.discounttype,
                            code: couponitem.code,
                            service_type: couponitem.service_type,
                            services: couponitem.services
                        }
                    }
                })
            } 
                if(parseFloat(coupon.rewardpoint) > 0){
                    let availableRewardPoint = 0;
                    let client = this.InfoClient;
                    if(client.is_vip == true){
                        if(this.userData.MaximumApplyRewardPointType == "percent"){
                            var percent = this.userData.MaximumApplyRewardPointVip; 
                            var amount = roundprice(client.reward_point * (percent / 100)); 
                            availableRewardPoint = amount;
                        }else{
                            if(this.userData.MaximumApplyRewardPointVip <= client.reward_point){
                                availableRewardPoint = this.userData.MaximumApplyRewardPointVip;
                            }else{
                                availableRewardPoint = client.reward_point;
                            }
                        }
                    }else{
                        if(this.userData.MaximumApplyRewardPointType == "percent"){
                            var percent = this.userData.MaximumApplyRewardPoint; 
                            var amount = roundprice(client.reward_point * (percent / 100)); 
                            availableRewardPoint = amount;
                        }else{
                            if(this.userData.MaximumApplyRewardPoint <= client.reward_point){
                                availableRewardPoint = this.userData.MaximumApplyRewardPoint;
                            }else{
                                availableRewardPoint = client.reward_point;
                            }
                        }
                    }
                    if(typeof(this.promotions['reward_point']) == 'undefined'){ 
                        this.promotions['reward_point'] = {
                            id: 'rewardpoint',
                            amount: parseFloat(availableRewardPoint),
                            checked: false
                        }    
                    }
                }
    
                let gift = coupon.giftbalance;
                if(parseFloat(gift) > 0){
                    if(typeof(this.promotions['gift']) == 'undefined'){
                        this.promotions['gift'] = {
                            id: 'gift',
                            checked: false,
                            amount: parseFloat(gift),
                        }
                    }
                }
    
    
            this.refs.coupon.setDataCoupon(this.promotions);
        }
    }

    createAsNewClient = (value) => {
        this.clientName = value;
        this.selectedClient = 0;
        this.refs.clientlistmodal.show([],false,false, false,'');
    }

    refreshClients = async () => {
        var clientList = await fetch(setting.apiUrl + 'get_clients', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.props.token,
            }
        }).then((response) => response.json()).then((responseJson) => {

            if (responseJson.success) {
                AsyncStorage.setItem('list-client', JSON.stringify(responseJson.data));
                return responseJson.data;
            } else {
                Alert.alert('Error', responseJson.message);
                return [];
            }
        }).catch((error) => {
            return [];
        });
        
        if(clientList.length){
            this.clients = clientList;
        }

        this.setTextClient(this.clientName);
    }

    setTextClient = (value) => {
        value = String.prototype.trim.call(value);
        value = value.replace('(','');
        value = value.replace(')','');
        value = value.replace(' ','');
        value = value.replace('-','');
        let isPhone = false;
        if(value.length >= 3 && !isNaN(value)){
            let formatValue = formatPhone(value);
            this.refs.clientInput.setState({text: formatValue});
            isPhone = true;
            value = formatValue.replace(/[^\d]+/g, '');
        }
        this.clientName = value;
        if(value.length >= 4){
            let clientsFiltered = [];
            let isActive = false;
            if(isPhone){
                clientsFiltered = this.clients.filter(function(item){
                    let phone = '';
                    if (typeof item.phone != 'undefined' && item.phone != '' && item.phone != null) {
                        phone = item.phone.replace(/[^\d]+/g, '');
                    }
                    return phone.indexOf(value) >= 0;
                });
                isActive = value.length == 10;
            }else{
                clientsFiltered = this.props.clients.filter(function(item){
                    return item.email.toLowerCase().indexOf(value.toString().toLowerCase()) >= 0;
                });
                isActive = emailvalidator.validate(String.prototype.trim.call(value));
            }
            this.isClientPhone = isPhone;
            this.isValidClient = isActive;
            this.refs.clientlistmodal.show(clientsFiltered.slice(0, 5),isPhone,true, isActive,value);
        }else{
            this.isClientPhone = false;
            this.clientName = '';
            this.isValidClient = false;
            this.refs.clientlistmodal.show([],false,false,false,value);
        }
        
        //this.refs.tabs.goToPage(1);
   
        /*
        if (typeof this.refs.clientlistmodal != "undefined") {
            this.refs.clientlistmodal.setState({ selected: this.selectedClient, modalVisible: true });
            this.refs.clientlistmodal.show();
        }*/
        
        //console.log(value);
    }

    /*
    onPressTechnician = () => {
        this.refs.tabs.goToPage(3);
        if (typeof this.refs.technicianlist != "undefined") {
            this.refs.technicianlist.setState({
                selected: this.selectedTechnician
            });
            this.refs.technicianlist.show();
        }
        //this.refs.clientlist.show();
    };*/

    onPressStatus = () => {
        this.refs.tabs.goToPage(4);
        if (typeof this.refs.statuslist != "undefined") {
            this.refs.statuslist.setState({ selected: this.selectedStatus });
            this.refs.statuslist.show();
        }
    };

    onCloseModalTechnicianList = () => {
        this.refs.tabs.goToPage(0);
    };

    onCloseModalStatusList = () => {
        this.refs.tabs.goToPage(0);
    };

    _onSelectedStatus = (id, name) => {
        this.refs.statusInput.setState({ text: this.getText(name) });
        this.statusName = name;
        this.refs.statuslist.setState({ modalVisible: false });
        this.selectedStatus = id;
        this.refs.tabs.goToPage(0);
    };

    _onSelectedTechnician = (id, name, keyInList) => {
        let serviceId = 0;
        let serviceName = getTextByKey(this.props.language,'selectserviceappointment');
        let price = 0;
        let duration = 0;
        let rewardpoint = 0;
        let rewardpointvip = 0;
        let isCombo = keyInList.indexOf('@combo') >= 0;
        if(!isCombo){
            if(typeof this.selectServices[keyInList] != 'undefined'){
                serviceId = this.selectServices[keyInList].id;
                serviceName = this.selectServices[keyInList].service_name;
                price = this.selectServices[keyInList].price;
                duration = this.selectServices[keyInList].duration;
                rewardpoint = this.selectServices[keyInList].rewardpoint;
                rewardpointvip = this.selectServices[keyInList].rewardpointvip;
            }
            //console.log(keyInList);
            let selectedData = {
                id: serviceId,
                service_name: serviceName,
                price: price,
                technicianId: id,
                technicianName: name,
                isCombo: false,
                duration: duration,
                rewardpoint: rewardpoint,
                rewardpointvip: rewardpointvip
            };
            this.selectServices[keyInList] = selectedData;
        }else{
            let keydata = keyInList.split('@combo')[0];
            let serviceItemId = keyInList.split('@combo')[1];
            let servicesData = [];
            if(typeof this.selectServices[keydata] != 'undefined'){
                serviceId = this.selectServices[keydata].id;
                serviceName = this.selectServices[keydata].service_name;
                price = this.selectServices[keydata].price;
                servicesData = this.selectServices[keydata].servicesIncombo;
            }

            let selectedData = {
                id: serviceId,
                service_name: serviceName,
                price: price,
                technicianId: id,
                technicianName: name,
                isCombo: true,
                duration: duration,
                rewardpoint: rewardpoint
            };

            let _this = this;
            let comboSelected = this.combos.filter(function(item){
                return 'combo_' + item.id == serviceId;
            });

            if(comboSelected.length){
                selectedData.servicesIncombo = comboSelected[0].services;
                selectedData.servicesIncombo.forEach(function(itemService){
                    let serviceCombo = _this.props.services.filter(function(serviceData){
                        return serviceData.id == 'service_' + itemService.serviceid;        
                    });
                  
                    if(serviceCombo.length){
                        itemService.servicename = serviceCombo[0].service_name;
                        itemService.duration = serviceCombo[0].duration;
                        if('service_' + serviceItemId == serviceCombo[0].id){
                            itemService.technicianId = id;
                            itemService.technicianName = name;

                            let serviceItemComboSelected = servicesData.filter(function(serviceData){
                                return 'service_' + serviceData.serviceid == serviceCombo[0].id;        
                            });
                            if(serviceItemComboSelected.length){
                                itemService.duration = serviceItemComboSelected[0].duration;
                            }
                            //itemService.duration = 
                        }else{
                            //'service_' + itemService.serviceid;
                            let serviceItemComboSelected = servicesData.filter(function(serviceData){
                                return 'service_' + serviceData.serviceid == serviceCombo[0].id;        
                            });
                   
                            if(serviceItemComboSelected.length){
                                itemService.technicianId = serviceItemComboSelected[0].technicianId;
                                itemService.technicianName = serviceItemComboSelected[0].technicianName;
                            }else{
                                itemService.technicianId = 0;
                                itemService.technicianName = getTextByKey(this.props.language,'anytech');
                            }
                            
                        }
                        
                    }
                })
            }
            this.selectServices[keydata] = selectedData;
        }
        
        this.refs.AppointmentSelectedService.setState({
            selectServices: this.selectServices
        });
        this.refs.coupon.setServices(this.selectServices);
        this.refs.technicianlist.setState({ modalVisible: false });
        this.refs.tabs.goToPage(0);
  
        /*
        this.refs.technicianInput.setState({ text: name });
        this.technicianName = name;
        this.refs.technicianlist.setState({ modalVisible: false });
        //this.setState({clientSelected: id});
        this.selectedTechnician = id;
        this.refs.tabs.goToPage(0);*/
    };

    _onSelectedService = (id, name, price,duration, isCombo, keyInList,rewardpoint, rewardpointvip) => {0
        let technicianId = 0;
        let technicianName = getTextByKey(this.props.language,'anytech');
        if(typeof this.selectServices[keyInList] != 'undefined'){
            technicianId = this.selectServices[keyInList].technicianId;
            technicianName = this.selectServices[keyInList].technicianName;
        }

        let selectedData = {
            id: id,
            service_name: name,
            price: price,
            technicianId: technicianId,
            technicianName: technicianName,
            isCombo: isCombo,
            duration: duration,
            rewardpoint: rewardpoint,
            rewardpointvip: rewardpointvip
        };
        let technicianName1 = getTextByKey(this.props.language,'anytech');
        if(isCombo){
            let _this = this;
            let comboSelected = this.combos.filter(function(item){
                return 'combo_' + item.id == id;
            });
            if(comboSelected.length){
                selectedData.servicesIncombo = comboSelected[0].services;
                selectedData.servicesIncombo.forEach(function(itemService){
                    let serviceCombo = _this.props.services.filter(function(serviceData){
                        return serviceData.id == 'service_' + itemService.serviceid;        
                    });
                  
                    if(serviceCombo.length){
                        itemService.servicename = serviceCombo[0].service_name;
                        itemService.duration = serviceCombo[0].duration;
                        itemService.price = serviceCombo[0].price;
                        itemService.technicianId = 0;

                        itemService.technicianName = technicianName1;
                    }
                })
            }
        }
        //console.log(selectedData);
        this.selectServices[keyInList] = selectedData;
        this.refs.AppointmentSelectedService.setState({
            selectServices: this.selectServices
        });
        this.refs.coupon.setServices(this.selectServices);
        this.refs.servicelist.close();
        this.refs.tabs.goToPage(0);
        this.refs.btnAddService.setState({
            isShowPlusService: true,
            count: this.refs.btnAddService.state.count + 1
        });
    };

    removeService = keyInList => {
        var services = {};
        var count = 0;
        for (var key in this.selectServices) {
            if (key != keyInList) {
                count++;
                services[key] = this.selectServices[key];
            }
        }
        this.selectServices = services;

        if (!count) {
            let firstService = {
                id: 0,
                service_name: getTextByKey(this.props.language,'selectserviceappointment'),
                price: 0,
                technicianId : 0,
                technicianName: getTextByKey(this.props.language,'anytech'),
                isCombo: false,
                rewardpoint: 0,
                rewardpointvip:0
            };

            this.selectServices["service_0"] = firstService;
            this.refs.btnAddService.setState({
                isShowPlusService: false,
                count: 0
            });
        } else {
            this.refs.btnAddService.setState({
                isShowPlusService: true,
                count: this.refs.btnAddService.state.count - 1
            });
        }

        this.refs.AppointmentSelectedService.setState({
            selectServices: this.selectServices
        });
        this.refs.coupon.setServices(this.selectServices);
        this.refs.servicelist.close();
        this.refs.tabs.goToPage(0);

        /*
        if(this.selectServices.length > 1){

        }*/
    };

    onPressSelectTime = () => {
        this.refs.tabs.goToPage(1);
        if (typeof this.refs.selecttime != "undefined") {
            this.refs.selecttime.show();
        }
    };

    onPressSelectCash = () => {
        //this.newRemaining = this.creditcardAmount
        if (this.creditcardAmount != "") {
            this.newRemaining =
                parseFloat(this.remaining) - parseFloat(this.creditcardAmount);
        }else if(this.physicalAmount != ""){
            this.newRemaining =
                parseFloat(this.remaining) - parseFloat(this.physicalAmount);
        } else {
            this.newRemaining = this.remaining;
        }
       
        this.refs.paymentCashInput.amount = this.cashAmount;
        this.refs.paymentCashInput.setState({
            modalVisible: true,
            value: this.cashAmount,
            remaining: this.newRemaining
        });
        //this.refs.cashInput.setState({text:'yes',placeholder:'ok'})
    };
    onPressSelectPhyshical = () => {
        if (this.creditcardAmount != "") {
            this.newRemaining =
                parseFloat(this.remaining) - parseFloat(this.creditcardAmount);
        }else if (this.cashAmount != "") {
            this.newRemaining =
                parseFloat(this.remaining) - parseFloat(this.cashAmount);
        } else {
            this.newRemaining = this.remaining;
        }
        this.refs.paymentPhysicalInput.amount = this.physicalAmount;
        this.refs.paymentPhysicalInput.setState({
            modalVisible: true,
            value: this.physicalAmount,
            remaining: this.newRemaining,
            ccnumber: this.ccnumberCN,
            ccholdername: this.ccholdernameCN,
            ccexpiredate: this.ccexpiredateCN,
        });
        //this.refs.cashInput.setState({text:'yes',placeholder:'ok'})
    };
    onPressSelectCreditCard = () => {
        if (this.cashAmount != "") {
            this.newRemaining =
                parseFloat(this.remaining) - parseFloat(this.cashAmount);
        }else if(this.physicalAmount != ""){
            this.newRemaining =
                parseFloat(this.remaining) - parseFloat(this.physicalAmount);
        }else {
            this.newRemaining = this.remaining;
        }
        this.refs.paymentCreditCardInput.amount = this.creditcardAmount;
        this.refs.paymentCreditCardInput.ccnumber = this.ccnumber;
        this.refs.paymentCreditCardInput.ccholdername = this.ccholdername;
        this.refs.paymentCreditCardInput.ccexpiredate = this.ccexpiredate;
        this.refs.paymentCreditCardInput.cccvv = this.cccvv;
        this.refs.paymentCreditCardInput.setState({
            modalVisible: true,
            value: this.creditcardAmount,
            remaining: this.newRemaining
        });
        //this.refs.cashInput.setState({text:'yes',placeholder:'ok'})
    };

    onCloseSelectTime = () => {
        this.refs.tabs.goToPage(0);
    };

    onPressService = (id, refdata) => {

        //console.log(refdata);
        this.refs.tabs.goToPage(2);
        let _this = this;
        if (typeof this.refs.servicelist == "undefined") {
            setTimeout(function() {
                /*
                _this.refs.servicelist.setState({
                    selected: id,
                    currentService: refdata,
                    modalVisible: true
                });*/
                _this.refs.servicelist.show(refdata,id);
            }, 10);
        } else {
            /*
            _this.refs.servicelist.setState({
                selected: id,
                currentService: refdata,
                modalVisible: true
            });*/
            _this.refs.servicelist.show(refdata,id);
        }
    };

    onPressTechnicianService = (id, refdata) => {
        if(this.appointmentDate == '' && this.userData.isManageTurn){
            Alert.alert('Error','Please select Time to calculate technicians turn');
        }else{
            this.refs.tabs.goToPage(3);
            let _this = this;
            if (typeof this.refs.technicianlist != "undefined") {
                this.refs.technicianlist.setState({
                    selected: id,
                    currentService: refdata
                });
                this.refs.technicianlist.show(_this.selectServices,_this.appointmentDate, id, refdata);
            }else{
                setTimeout(function() {
                    _this.refs.technicianlist.setState({
                        selected: id,
                        currentService: refdata
                    });
                    _this.refs.technicianlist.show(_this.selectServices,_this.appointmentDate, id, refdata);
                }, 0);
            }
        }
        
    }

    onSelectedTime = (apptDate, apptHour) => {
        this.refs.timeInput.setState({
            text: apptDate.format("MM-DD-Y") + " " + apptHour
        });
        this.refs.selecttime.setState({
            modalVisible: false,
            hourselect: apptHour,
            dateselect: apptDate
        });
        //this.setState({clientSelected: id});
        this.selectedDate = apptDate;
        this.selectedHour = apptHour;
        this.appointmentDate = apptDate.format("DD-MM-Y");
        this.appointmentHour = this.convertTo24Hour(apptHour);
        this.refs.tabs.goToPage(0);
        if(this.selectedClient != ""){
            this._onGetCoupon();
        }
    };

    convertTo24Hour(time) {
        time = time.toLowerCase();
        var hours = time.substr(0, 2);
        if (time.indexOf("am") != -1 && hours == 12) {
            time = time.replace("12", "0");
        }
        if (time.indexOf("pm") != -1 && parseInt(hours) < 12) {
            time = time.replace(hours, parseInt(hours) + 12);
        }
        return time.replace(/(am|pm)/, "");
    }

    getText(key){

        return getTextByKey(this.props.language,key);
    }

    showPayment = () => {
        let isValid = false;
        if (this.selectedClient == 0 && !this.isValidClient) {
            Alert.alert("Error", this.getText('clientappointmentrequire'));
        } else if (this.selectedHour == "") {
            Alert.alert("Error", this.getText('timeappointmentrequire'));
        } else if (this.selectedStatus == "") {
            Alert.alert("Error", this.getText('statusappointmentrequire'));
        } else {
            isValid = true;
            this.total = 0;
            /*
            Object.values(this.selectServices).map(data => {
                if (data.id > 0) {
                    isValid = true;
                    this.total += parseFloat(data.price);
                    //return;
                }
            });*/

       
            Object.values(this.selectServices).map(data => {
                let dataId = parseInt(data.id.toString().replace('service_','').replace('combo_',''));
                if (dataId > 0) {
                    this.total += parseFloat(data.price);
                }else{
                    isValid = false;
                }
            });

            this.remaining = this.total - parseFloat(this.paid_total) - parseFloat(this.paidcoupon);
            this.remaining = this.remaining.toFixed(2).replace('.00','');
            //remaining
         
            if (!isValid) {
                Alert.alert("Error", getTextByKey(this.props.language,'requireservicesappointment'));
            }
        }
        if (isValid) {
            //console.log('ok');
            this.refs.tabs.goToPage(5);
            let _this = this;
            if (this.payments.length) {
                setTimeout(function() {
                    _this.refs.tabspayment.goToPage(0);
                    _this.refs.AppointmentHistoryPaymentTotal.setState({
                        total: _this.total
                    });
                    _this.refs.AppointmentHistoryPaymentRemaining.setState({
                        total: _this.remaining
                    });
                }, 0);
            } else {
                setTimeout(function() {
                    _this.refs.tabspayment.goToPage(1);
                    _this.refs.AppointmentNewPaymentTotal.setState({
                        total: _this.total
                    });
                    _this.refs.AppointmentNewPaymentRemaining.setState({
                        total: _this.remaining
                    });
                }, 0);
            }
        }

        //this.refs.tabs.goToPage(6);
    };

    getTimeFromMins(mins) {
        if (mins >= 24 * 60 || mins < 0) {
            throw new RangeError("Valid input should be greater than or equal to 0 and less than 1440.");
        }
        var h = mins / 60 | 0,
            m = mins % 60 | 0;
        return (h * 100) + m;
    }

    getEndHour(startHour, duration) {
        var startInHour = startHour.split(':')[0] + "00";
        var startInMinute = startHour.split(':')[1];
        var totalMinute = parseInt(startInMinute) + parseInt(duration);
        var calculateEndHour = parseInt(startInHour) + parseInt(this.getTimeFromMins(totalMinute));
        return calculateEndHour;
    }

    formatHourFromNumber(calculateEndHour) {
        let prefix = '';
        if (calculateEndHour.toString().length == 4) {
            hour = calculateEndHour.toString().substring(0, 2);
            minute = calculateEndHour.toString().substring(2, 4);
        } else {
            hour = calculateEndHour.toString().substring(0, 1);
            minute = calculateEndHour.toString().substring(1, 3);
            prefix = '0';
        }
        return  prefix + hour + ':' + minute;
    }

    calculateEndHour = (start) => {
        let arrTechEndHour = {};
        let end = 0;
        let _this = this;

        Object.values(this.selectServices).map(data => {
            //let dataid = parseInt(data.id.toString().replace('service_','').replace('combo_',''))
            if(!data.isCombo){
                if(typeof(arrTechEndHour[data.technicianId]) == 'undefined'){
                    let EndTimeService = this.formatHourFromNumber(this.getEndHour(start,data.duration));    
                    arrTechEndHour[data.technicianId] = parseInt(EndTimeService.replace(':',''));        
                }else{
                    arrTechEndHour[data.technicianId] = this.getEndHour(this.formatHourFromNumber(arrTechEndHour[data.technicianId]),data.duration);
                } 
                if(end < arrTechEndHour[data.technicianId]){
                    end = arrTechEndHour[data.technicianId];
                }       
            }else{
               
                data.servicesIncombo.forEach(function(itemService){
                    if(typeof(arrTechEndHour[itemService.technicianId]) == 'undefined'){
                        let EndTimeService = _this.formatHourFromNumber(_this.getEndHour(start,itemService.duration));    
                        arrTechEndHour[itemService.technicianId] = parseInt(EndTimeService.replace(':',''));        
                    }else{
                        arrTechEndHour[itemService.technicianId] = _this.getEndHour(_this.formatHourFromNumber(arrTechEndHour[itemService.technicianId]),itemService.duration);
                    } 
                    if(end < arrTechEndHour[itemService.technicianId]){
                        end = arrTechEndHour[itemService.technicianId];
                    }
                });
            }
            
        });
        return this.formatHourFromNumber(end);
        
        //console.log(this.selectedTechnician);
    }

    submitAppointment = () => {
        if (this.selectedClient == 0 && !this.isValidClient) {
            Alert.alert("Error", this.getText('clientappointmentrequire'));
        } else if (this.selectedHour == "") {
            Alert.alert("Error", this.getText('timeappointmentrequire'));
        } else if (this.selectedStatus == "") {
            Alert.alert("Error", this.getText('statusappointmentrequire'));
        } else {
            let isValid = true;
            let isValidTech = true;
            Object.values(this.selectServices).map(data => {
                let dataId = parseInt(data.id.toString().replace('service_','').replace('combo_',''));
                if (dataId > 0) {
                    /*
                    if(!data.isCombo){
                        if(data.technicianId == 0){
                            isValidTech = false;
                        }        
                    }else{
                        data.servicesIncombo.forEach(function(itemService){
                            if(itemService.technicianId == 0){
                                isValidTech = false;
                            } 
                        })
                    }*/
                    
                }else{
                    isValid = false;
                }
            });

            if (!isValid) {
                Alert.alert("Error", getTextByKey(this.props.language,'requireservicesappointment'));
            }else {
                
                let submitData = {};
                submitData.promotions = this.promotionsApplied;
                submitData.isClientPhone = this.isClientPhone ? 1 : 0;
                submitData.clientName = this.clientName;
                submitData.appointment_id = this.state.appointmentId;
                submitData.client_id = this.selectedClient;
                submitData.appointment_hour = String.prototype.trim.call(
                    this.convertTo24Hour(this.selectedHour)
                );
                submitData.appointment_day = this.appointmentDate;
                submitData.appointment_status = this.selectedStatus;
                submitData.technician_id = this.selectedTechnician;
                submitData.appointment_endhour = this.calculateEndHour(submitData.appointment_hour);
                submitData.rewardpointsalon = this.rewardpointsalon == "" ? 0 : this.rewardpointsalon;
                let services = [];
                let _this = this;
                Object.values(this.selectServices).map(data => {
                    let dataid = parseInt(data.id.toString().replace('service_','').replace('combo_',''))
                    if(!data.isCombo){
                        let EndTimeService = this.formatHourFromNumber(this.getEndHour(submitData.appointment_hour,data.duration));
                    
                        let submitDataService = {
                            serviceid: dataid,
                            price: data.price,
                            isCombo:0,
                            technicianId: data.technicianId,
                            endTime: EndTimeService,
                            startTime: submitData.appointment_hour,
                            rewardpoint: data.rewardpoint,
                            rewardpointvip: data.rewardpointvip
                        };
                        if (this.state.appointmentId > 0) {
                            if (typeof data.appointment_service_id == "undefined")
                                data.appointment_service_id = 0;
                            submitDataService.appointment_service_id = data.appointment_service_id;
                        }
                    
                        
                        if(this.userData.role == 9 && data.technicianId == 0){
                            submitDataService.technicianId = this.userData.id;
                        }
                        services.push(submitDataService);        
                    }else{
                        let submitDataService = {
                            comboid: dataid,
                            price: data.price,
                            isCombo:1,
                            services: [],
                            rewardpoint: data.rewardpoint
                        };

                        if (this.state.appointmentId > 0) {
                            if (typeof data.appointment_combo_id == "undefined")
                                data.appointment_combo_id = 0;
                            submitDataService.appointment_combo_id = data.appointment_combo_id;
                        }

                        data.servicesIncombo.forEach(function(itemService){
                            let EndTimeService = _this.formatHourFromNumber(_this.getEndHour(submitData.appointment_hour,itemService.duration));
                            let serviceSubmit = {
                                id: itemService.serviceid,
                                price: itemService.price,
                                technicianId: itemService.technicianId,
                                startTime: submitData.appointment_hour,
                                endTime: EndTimeService
                            };

                            if (_this.state.appointmentId > 0) {
                                if (typeof itemService.appointment_service_id == "undefined")
                                    itemService.appointment_service_id = 0;
                                serviceSubmit.appointment_service_id = itemService.appointment_service_id;
                            }

                            if(_this.userData.role == 9 && itemService.technicianId == 0){
                                serviceSubmit.technicianId = _this.userData.id;
                            } 

                            submitDataService.services.push(serviceSubmit);
                        });
                        
                        if(this.userData.role == 9 && data.technicianId == 0){
                            submitDataService.technicianId = this.userData.id;
                        }   
                        services.push(submitDataService);
                    }
                    
                });
                submitData.services = services;
                


                if (this.cashAmount != "") {
                    submitData.isCash = "true";
                    submitData.cashAmount = this.cashAmount;
                }
                if (this.creditcardAmount != "") {
                    submitData.isCreditCard = "true";
                    submitData.ccAmount = this.creditcardAmount;
                    submitData.expireDate = this.ccexpiredate;
                    submitData.cardnumber = this.ccnumber;
                    submitData.Cardholdername = this.ccholdername;
                    submitData.cvc = this.cccvv;
                }
                if(this.physicalAmount != ""){
                    submitData.isPhysical = "true";
                    submitData.ccAmount = this.physicalAmount;
                    submitData.ccexpiredateCN = this.ccexpiredateCN;
                    submitData.ccnumberCN = this.ccnumberCN;
                    submitData.ccholdernameCN = this.ccholdernameCN;
                }
                let deviceid = this.deviceid;
                submitData.device_id = deviceid;
                this.refs.appointmentLoader.setState({ visible: true });
                fetch(setting.apiUrl + "book_appointment", {
                    method: "POST",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + this.props.token
                    },
                    body: JSON.stringify(submitData)
                })
                    .then(response => response.json())
                    .then(responseJson => {
                        if (!responseJson.success) {
                            let _this = this;
                            //console.log(responseJson);

                            this.refs.appointmentLoader.setState({
                                visible: false
                            });
                            //console.log(responseJson);
                            setTimeout(function() {
                                _this.fetchError(responseJson);
                            }, 100);
                            
                            //Alert.alert('Error', responseJson.message);
                            //return [];
                        } else {
                            this.refs.appointmentLoader.setState({
                                visible: false
                            });
                            let successMessage = this.getText('bookedappointmentmessage');
                            if (submitData.appointment_id > 0) {
                                successMessage = this.getText('updatedappointmentmessage');
                            }
                            this.refs.appointmentSuccessLoader.setState({
                                textContent: successMessage,
                                visible: true
                            });
                            //console.log(responseJson.data);    
                            let _this = this;
                            setTimeout(function() {
                                _this.refs.appointmentSuccessLoader.setState({
                                    visible: false
                                });
                                _this.props.SaveAppointmentSuccess(
                                    _this.state.appointmentId,
                                    responseJson.data
                                );
                            }, 2000);

                            //Alert.alert("OK", "Appointment Booked");
                        }
                    })
                    .catch(error => {
                        console.error(error);

                        //return [];
                    });
            }

            //for()
        }
        //this.refs.appointmentLoader.setState({visible:true});
    };

    fetchError(responseJson) {
        if (
            responseJson.message == "token_expired" ||
            responseJson.message == "token_invalid"
        ) {
            let rootNavigator = this.props.navigation.getNavigator("root");
            rootNavigator.replace("login");
        } else {
            Alert.alert("Error", responseJson.message);
            //console.log(responseJson.message);
        }
    }

    takeNewPayment = () => {
        this.refs.tabspayment.goToPage(1);
        this.total = 0;
        Object.values(this.selectServices).map(data => {
            if (parseInt(data.id.toString().replace('service_','').replace('combo_','')) > 0) {
                isValid = true;
                this.total += parseFloat(data.price);
                //return;
            }
        });

        this.remaining = this.total - parseFloat(this.paid_total) - parseFloat(this.paidcoupon);
        this.remaining = this.remaining.toFixed(2).replace('.00','');
        let _this = this;
        setTimeout(function() {
            _this.refs.AppointmentNewPaymentTotal.setState({
                total: _this.total
            });

            _this.refs.AppointmentNewPaymentRemaining.setState({
                total: _this.remaining
            });
        }, 0);
    };

    closePaymentHistory = () => {
        this.refs.tabs.goToPage(0);
    };

    closeNewPayment = () => {
        if (this.payments.length) {
            this.refs.tabspayment.goToPage(0);
        } else {
            this.refs.tabs.goToPage(0);
        }
    };

    _onSaveCash = amount => {
        this.refs.paymentCashInput.setState({ modalVisible: false });
        this.refs.tabspayment.goToPage(1);

        if (amount != "") {
            this.refs.cashInput.setState({ text: "Cash", placeholder: amount });
            amount = amount.replace("$", "");
            amount = amount.replace(",", "");
        } else {
            this.refs.cashInput.setState({ text: "", placeholder: "Cash" });
        }
        this.cashAmount = amount;
        //console.log(amount);
    };

    _onSavePhysical = (
        amount,
        ccnumberCN,
        ccholdernameCN,
        ccexpiredateCN,
    ) => {
        this.refs.paymentPhysicalInput.setState({ modalVisible: false });
        this.refs.tabspayment.goToPage(1);

        if (amount != "") {
            this.refs.physicalInput.setState({
                text: "Physical",
                placeholder: amount
            });
            amount = amount.replace("$", "");
            amount = amount.replace(",", "");
        } else {
            this.refs.physicalInput.setState({
                text: "",
                placeholder: "Physical"
            });
        }
        this.physicalAmount = amount;
        this.ccnumberCN = ccnumberCN;
        this.ccholdernameCN = ccholdernameCN;
        this.ccexpiredateCN = ccexpiredateCN;
    };
    _onSaveCreditCard = (
        amount,
        ccnumber,
        ccholdername,
        ccexpiredate,
        cccvv
    ) => {
        this.refs.paymentCreditCardInput.setState({ modalVisible: false });
        this.refs.tabspayment.goToPage(1);

        if (amount != "") {
            this.refs.creditcardInput.setState({
                text: "Credit Card",
                placeholder: amount
            });
            amount = amount.replace("$", "");
            amount = amount.replace(",", "");
        } else {
            this.refs.creditcardInput.setState({
                text: "",
                placeholder: "Credit Card"
            });
        }
        this.creditcardAmount = amount;
        this.ccnumber = ccnumber;
        this.ccholdername = ccholdername;
        this.ccexpiredate = ccexpiredate;
        this.cccvv = cccvv;
    };

    setapplygift = (value) => {
        value = String.prototype.trim.call(value);
        this.promocodegiftapply = value;
    }
    setrewardpoint = (value) => {
        value = String.prototype.trim.call(value);
        this.rewardpointsalon = value;
    }

    checkCode = async () => {
        let isvalid = true;
        if(String.prototype.trim.call(this.promocodegiftapply) == ''){
            Alert.alert('Error','Please enter code');
            isvalid = false;
        }else if (this.selectedClient == 0 && !this.isValidClient) {
            Alert.alert("Error", this.getText('clientappointmentrequire'));
            isvalid = false;
        } else if (this.selectedHour == "") {
            Alert.alert("Error", this.getText('timeappointmentrequire'));
            isvalid = false;
        }
        if(isvalid){
            let code = this.promocodegiftapply;
            this.refs.appointmentLoader.setState({ visible: true });
            let postData = {
                clientid: this.selectedClient,
                code: code
            };
            await fetch(setting.apiUrl + 'promocode/check',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.token
                },
                body: JSON.stringify(postData)
            }).then((response) => response.json()).then((responseJson) => {
                this.refs.appointmentLoader.setState({ visible: false });
                if(!responseJson.success)
                {
                    setTimeout(function(){
                        Alert.alert('Error', 'Invalid Code');        
                    },100);
        
                }else{
                    let isReloadGift = false;
                    if(typeof(this.promotions['giftcode']) == 'undefined'){
                        this.promotions.gift = {
                            id: 'giftcode',
                            checked: false,
                            amount: responseJson.amount,
                            giftid: responseJson.id,
                            code: responseJson.code
                        }
                        isReloadGift = true;
                    }
                    if(isReloadGift){
                        this.refs.coupon.setDataCoupon(this.promotions);
                    }
                }
                
            }).catch((error) => {
                console.error(error);
            });
        }
    }
    render() {
        if(this.selectedStatus == ""){
            this.statusName = "confirmed";
            this.selectedStatus = "confirmed";
        }
        return (
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}
            >
                <View style={{ flex: 1 }}>
                    <ScrollableTabView
                        ref="tabs"
                        renderTabBar={() => <HideTabBar />}
                        locked={true}
                    >
                        <View style={{ flex: 1 }}>
                            <View
                                style={
                                    Platform.OS === "android"
                                        ? layout.headercontainerAndroid
                                        : layout.headercontainer
                                }
                            >
                                <LinearGradient
                                    start={[0, 0]}
                                    end={[1, 0]}
                                    colors={["#F069A2", "#EEAEA2"]}
                                    style={
                                        Platform.OS === "android"
                                            ? layout.headerAndroid
                                            : layout.header
                                    }
                                >
                                    <View style={layout.headercontrols}>
                                        <TouchableOpacity
                                            style={
                                                layout.headerNavLeftContainer
                                            }
                                            activeOpacity={1}
                                            onPress={() => this.close()}
                                        >
                                            <View style={layout.headerNavLeft}>
                                                <MaterialCommunityIcons
                                                    name={"close"}
                                                    size={30}
                                                    color={
                                                        "rgba(255,255,255,1)"
                                                    }
                                                    style={
                                                        Platform.OS ===
                                                        "android"
                                                            ? layout.navIcon
                                                            : layout.navIconIOS
                                                    }
                                                />
                                            </View>
                                        </TouchableOpacity>
                                        <View
                                            style={{
                                                flex: 1,
                                                justifyContent: "center",
                                                alignItems: "center"
                                            }}
                                        >
                                            <Text style={layout.headertitle}>
                                                {this.title}
                                            </Text>
                                        </View>
                                        <TouchableOpacity
                                            style={
                                                layout.headerNavRightContainer
                                            }
                                            activeOpacity={1}
                                            onPress={() => this.showPayment()}
                                        >
                                            <View style={layout.headerNavRight}>
                                                <Text
                                                    style={layout.headerNavText}
                                                >
                                                    {this.getText('payment')}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </LinearGradient>
                            </View>
                            <ScrollView style={{flex:1}} keyboardShouldPersistTaps='always'>
                                <View style={layout.floatGroupsection}>
                                    <Text style={{ color: "#808080" }}>
                                        { getTextByKey(this.props.language,'detailappointmenttitle') }
                                    </Text>
                                </View>
                                <View style={layout.floatGroup}>
                                    <FloatLabelTextInput
                                        placeholder={getTextByKey(this.props.language,'clientphoneoremail')}
                                        value={this.clientName}
                                        onChangeTextValue={(value) => this.setTextClient(value)}
                                        //onPress={async () => {await this.onPressClient()}}
                                        ref="clientInput"
                                    />
                                </View>
                                <ClientSearchModal
                                    ref={"clientlistmodal"}
                                    //selected={this.selectedClient}
                                    onSelectedClient={this._onSelectedClient}
                                    createAsNewClient={this.createAsNewClient}
                                    token={this.props.token}
                                    refresh={async () => { await this.refreshClients() }}
                                    language={this.props.language}
                                    //onClose={this.onCloseModalClientList}
                                    //token={this.props.token}
                                />
                                <View style={layout.floatGroup}>
                                    <FloatLabelSelect
                                        placeholder={this.getText('time')}
                                        value={this.selectedTime}
                                        onPress={this.onPressSelectTime}
                                        ref="timeInput"
                                    />
                                </View>

                                <View style={layout.floatGroupsection}>
                                    <Text style={{ color: "#808080" }}>
                                        { getTextByKey(this.props.language,'servicesappointmenttitle') }
                                    </Text>
                                </View>

                                <AppointmentSelectedService
                                    ref="AppointmentSelectedService"
                                    selectServices={this.selectServices}
                                    onPress={this.onPressService}
                                    onPressTechnician={this.onPressTechnicianService}
                                    userData={this.userData}
                                    services={this.services}
                                    combos={this.combos}
                                    
                                />

                                <ButtonAddService
                                    ref="btnAddService"
                                    onPress={this.onPressService}
                                    language={this.props.language}
                                />

                                <View style={layout.floatGroupSeperate} />

                                <View style={layout.floatGroup}>
                                    <FloatLabelSelect
                                        placeholder={this.getText('status')}
                                        value={this.getText(this.statusName)}
                                        onPress={this.onPressStatus}
                                        ref="statusInput"
                                    />
                                </View>
                                <View style={layout.floatGroupsection}>
                                    <Text style={{ color: "#808080" }}>
                                        Reward point
                                    </Text>
                                </View>
                                <View style={layout.floatGroup}>
                                <TextInput
                                            style={[styles.textbox]}
                                            placeholder='Enter reward point' 
                                            placeholderTextColor='#ccc'
                                            onChangeText={(value) => this.setrewardpoint(value)}
                                            ref="rewardpointInput"
                                            value={this.rewardpointsalon} 
                                            underlineColorAndroid={'transparent'}
                                        />
                                </View>
                                <View style={layout.floatGroupsection}>
                                    <Text style={{ color: "#808080" }}>
                                        { getTextByKey(this.props.language,'redeemgiftcode') }
                                    </Text>
                                </View>

                            
                                <View style={[styles.searchContainer,{width:"100%", alignSelf: 'stretch',}]} >
                                        <TextInput
                                            style={[styles.textbox]}
                                            placeholder='Enter gift card code' 
                                            placeholderTextColor='#ccc'
                                            onChangeText={(value) => this.setapplygift(value)}
                                            ref="applygiftInput"
                                            value={this.promocodegiftapply} 
                                            underlineColorAndroid={'transparent'}
                                        />
    
                                        <TouchableOpacity style={styles.searchbox} activeOpacity={1} onPress={async () => { await this.checkCode()}}>
                                            <LinearGradient
                                                    start={[0, 0]}
                                                    end={[1, 0]}
                                                    colors={["#F069A2", "#EEAEA2"]}
                                                    style={[styles.btnLinearPromo, styles.active ]}
                                                >
                                                <Text style={styles.txtsearchtext}> { getTextByKey(this.props.language,'applygiftcode') }</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                         

                                <View style={layout.floatGroupsection}>
                                    <Text style={{ color: "#808080" }}>
                                        { getTextByKey(this.props.language,'coupon') }
                                    </Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <CouponAppointment  ref="coupon" language={this.props.language} paid_total={this.paid_total} payments={this.payments} selectServices={this.selectServices} onPress={this._onAppliedCoupon} /> 
                                </View>
                                
                                <View style={layout.floatGroupSeperate} />  
                                <View style={styles.btnSave}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.btnSaveWraper}
                                        onPress={this.submitAppointment}
                                    >
                                        <LinearGradient
                                            start={[0, 0]}
                                            end={[1, 0]}
                                            colors={["#F069A2", "#EEAEA2"]}
                                            style={styles.btnLinear}
                                        >
                                            <Text style={styles.btnSaveText}>
                                                {this.getText('saveappointment')}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>  
                        </View>
                        



                        <View style={{ flex: 1 }}>
                            <AppointmentSelectTime
                                selectedDate={this.selectedDate}
                                onPress={this.onSelectedTime}
                                selectedHour={this.selectedHour}
                                data={this.props.availablehours}
                                ref="selecttime"
                                onClose={this.onCloseSelectTime}
                                language={this.props.language}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <ServiceSearchList
                                ref={"servicelist"}
                                data={this.props.services}
                                combos={this.combos}
                                //selected={this.state.serviceSelected}
                                currentService={"service_0"}
                                onSelected={this._onSelectedService}
                                onClose={this.onCloseModalServiceList}
                                onDelete={this.removeService}
                                categories={this.props.categories}
                                language={this.props.language}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <AppointmentTechnicianSearchList
                                ref={"technicianlist"}
                                data={this.props.technicians}
                                currentService={"service_0"}
                                selected={this.selectedTechnician}
                                onSelected={this._onSelectedTechnician}
                                onClose={this.onCloseModalTechnicianList}
                                turns={this.turns}
                                services={this.props.services}
                                userData={this.userData}
                                language={this.props.language}
                            />
                        </View>

                        <View style={{ flex: 1 }}>
                            <StatusSearchList
                                ref={"statuslist"}
                                data={this.props.listStatus}
                                selected={this.selectedStatus}
                                onSelected={this._onSelectedStatus}
                                onClose={this.onCloseModalStatusList}
                                language={this.props.language}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <ScrollableTabView
                                ref="tabspayment"
                                renderTabBar={() => <HideTabBar />}
                                locked={true}
                            >
                                <View
                                    style={{ flex: 1 }}
                                    tabLabel="tab1"
                                    ref="tabpaymenthistory"
                                >
                                    <View
                                        style={
                                            Platform.OS === "android"
                                                ? layout.headercontainerAndroid
                                                : layout.headercontainer
                                        }
                                    >
                                        <LinearGradient
                                            start={[0, 0]}
                                            end={[1, 0]}
                                            colors={["#F069A2", "#EEAEA2"]}
                                            style={
                                                Platform.OS === "android"
                                                    ? layout.headerAndroid
                                                    : layout.header
                                            }
                                        >
                                            <View style={layout.headercontrols}>
                                                <TouchableOpacity
                                                    style={
                                                        layout.headerNavLeftContainer
                                                    }
                                                    activeOpacity={1}
                                                    onPress={() =>
                                                        this.closePaymentHistory()}
                                                >
                                                    <View
                                                        style={
                                                            layout.headerNavLeft
                                                        }
                                                    >
                                                        <MaterialCommunityIcons
                                                            name={
                                                                "chevron-left"
                                                            }
                                                            size={30}
                                                            color={
                                                                "rgba(255,255,255,1)"
                                                            }
                                                            style={
                                                                Platform.OS ===
                                                                "android"
                                                                    ? layout.navIcon
                                                                    : layout.navIconIOS
                                                            }
                                                        />
                                                    </View>
                                                </TouchableOpacity>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        justifyContent:
                                                            "center",
                                                        alignItems: "center"
                                                    }}
                                                >
                                                    <Text
                                                        style={
                                                            layout.headertitle
                                                        }
                                                    >
                                                        {this.getText('paymenthistory')}
                                                    </Text>
                                                </View>
                                            </View>
                                        </LinearGradient>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <ScrollView keyboardShouldPersistTaps='always'>
                                            <View
                                                style={
                                                    styles.summaryTotalContainer
                                                }
                                            >
                                                <View
                                                    style={[
                                                        styles.summaryTotalLeft,
                                                        {
                                                            width: this
                                                                .columnWidth
                                                        }
                                                    ]}
                                                >
                                                    <Text
                                                        style={
                                                            styles.summaryTotalLeftTitle
                                                        }
                                                    >
                                                        {this.getText('paymenttotal')}
                                                    </Text>
                                                    <AppointmentPaymentTotal
                                                        total={this.total}
                                                        ref="AppointmentHistoryPaymentTotal"
                                                    />
                                                </View>
                                                <View
                                                    style={[
                                                        styles.summaryTotalRight,
                                                        {
                                                            width: this
                                                                .columnWidth
                                                        }
                                                    ]}
                                                >
                                                    <Text
                                                        style={
                                                            styles.summaryTotalRightTitle
                                                        }
                                                    >
                                                        {this.getText('paymentremaining')}
                                                    </Text>
                                                    <AppointmentPaymentRemaining
                                                        total={this.remaining}
                                                        ref="AppointmentHistoryPaymentRemaining"
                                                    />
                                                </View>
                                            </View>
                                            {this.payments.map(
                                                (data, index) => {
                                                    if(data.payment_method == 'cash' && data.amount == 0){
                                                        return false;
                                                    }
                                                    return (
                                                        <View
                                                            key={data.id}
                                                            style={
                                                                styles.paymenthistoryrow
                                                            }
                                                        >
                                                            <View
                                                                style={[
                                                                    styles.paymenthistoryrowleft,
                                                                    {
                                                                        width:
                                                                            this
                                                                                .fullWidth -
                                                                            110
                                                                    }
                                                                ]}
                                                            >
                                                                <Text>
                                                                    {
                                                                        data.description
                                                                    }
                                                                </Text>
                                                                <Text
                                                                    style={
                                                                        styles.paymenthistoryrowdate
                                                                    }
                                                                >
                                                                    {data.date}
                                                                </Text>
                                                            </View>
                                                            <View
                                                                style={[
                                                                    styles.paymenthistoryrowright,
                                                                    {
                                                                        width: 110
                                                                    }
                                                                ]}
                                                            >
                                                                <Text
                                                                    style={
                                                                        styles.paymenthistoryrowprice
                                                                    }
                                                                >
                                                                    ${data.amount}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    );
                                                }
                                            )}

                                            <View style={styles.btnSave}>
                                                <TouchableOpacity
                                                    activeOpacity={1}
                                                    style={styles.btnSaveWraper}
                                                    onPress={
                                                        this.takeNewPayment
                                                    }
                                                >
                                                    <LinearGradient
                                                        start={[0, 0]}
                                                        end={[1, 0]}
                                                        colors={[
                                                            "#F069A2",
                                                            "#EEAEA2"
                                                        ]}
                                                        style={styles.btnLinear}
                                                    >
                                                        <Text
                                                            style={
                                                                styles.btnSaveText
                                                            }
                                                        >
                                                            {this.getText('takenewpayment')}
                                                        </Text>
                                                    </LinearGradient>
                                                </TouchableOpacity>
                                            </View>
                                        </ScrollView>
                                    </View>
                                </View>

                                <View
                                    style={{ flex: 1 }}
                                    tabLabel="tab2"
                                    ref="tabnewpayment"
                                >
                                    <View
                                        style={
                                            Platform.OS === "android"
                                                ? layout.headercontainerAndroid
                                                : layout.headercontainer
                                        }
                                    >
                                        <LinearGradient
                                            start={[0, 0]}
                                            end={[1, 0]}
                                            colors={["#F069A2", "#EEAEA2"]}
                                            style={
                                                Platform.OS === "android"
                                                    ? layout.headerAndroid
                                                    : layout.header
                                            }
                                        >
                                            <View style={layout.headercontrols}>
                                                <TouchableOpacity
                                                    style={
                                                        layout.headerNavLeftContainer
                                                    }
                                                    activeOpacity={1}
                                                    onPress={() =>
                                                        this.closeNewPayment()}
                                                >
                                                    <View
                                                        style={
                                                            layout.headerNavLeft
                                                        }
                                                    >
                                                        <MaterialCommunityIcons
                                                            name={
                                                                "chevron-left"
                                                            }
                                                            size={30}
                                                            color={
                                                                "rgba(255,255,255,1)"
                                                            }
                                                            style={
                                                                Platform.OS ===
                                                                "android"
                                                                    ? layout.navIcon
                                                                    : layout.navIconIOS
                                                            }
                                                        />
                                                    </View>
                                                </TouchableOpacity>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        justifyContent:
                                                            "center",
                                                        alignItems: "center"
                                                    }}
                                                >
                                                    <Text
                                                        style={
                                                            layout.headertitle
                                                        }
                                                    >
                                                        {this.getText('takenewpayment')}
                                                    </Text>
                                                </View>
                                            </View>
                                        </LinearGradient>
                                    </View>

                                    <View style={{ flex: 1 }}>
                                        <View
                                            style={styles.summaryTotalContainer}
                                        >
                                            <View
                                                style={[
                                                    styles.summaryTotalLeft,
                                                    { width: this.columnWidth }
                                                ]}
                                            >
                                                <Text
                                                    style={
                                                        styles.summaryTotalLeftTitle
                                                    }
                                                >
                                                    {this.getText('paymenttotal')}
                                                </Text>
                                                <AppointmentPaymentTotal
                                                    total={this.total}
                                                    ref="AppointmentNewPaymentTotal"
                                                />
                                            </View>
                                            <View
                                                style={[
                                                    styles.summaryTotalRight,
                                                    { width: this.columnWidth }
                                                ]}
                                            >
                                                <Text
                                                    style={
                                                        styles.summaryTotalRightTitle
                                                    }
                                                >
                                                    {this.getText('paymentremaining')}
                                                </Text>
                                                <AppointmentPaymentRemaining
                                                    total={this.remaining}
                                                    ref="AppointmentNewPaymentRemaining"
                                                />
                                            </View>
                                        </View>
                                        <ScrollView keyboardShouldPersistTaps='always'>
                                            <View
                                                style={layout.floatGroupsection}
                                            >
                                                <Text
                                                    style={{ color: "#808080" }}
                                                >
                                                    {this.getText('paymentmethod')}
                                                </Text>
                                            </View>
                                            <View style={layout.floatGroup}>
                                                <FloatLabelSelectPrice
                                                    placeholder={this.getText('paymentcash')}
                                                    value={""}
                                                    onPress={
                                                        this.onPressSelectCash
                                                    }
                                                    ref="cashInput"
                                                />
                                            </View>
                                            <View style={layout.floatGroup}>
                                                <FloatLabelSelectPrice
                                                    placeholder={this.getText('paymentcreditcard')}
                                                    value={""}
                                                    onPress={
                                                        this
                                                            .onPressSelectCreditCard
                                                    }
                                                    ref="creditcardInput"
                                                />
                                            </View>
                                            {/* <View style={layout.floatGroup}>
                                                <FloatLabelSelectPrice
                                                    placeholder={this.getText('paymentphysical')}
                                                    value={""}
                                                    onPress={
                                                        this
                                                            .onPressSelectPhyshical
                                                    }
                                                    ref="physicalInput"
                                                />
                                            </View> */}
                                            <View style={styles.btnSave}>
                                                <TouchableOpacity
                                                    activeOpacity={1}
                                                    style={styles.btnSaveWraper}
                                                    onPress={
                                                        this.submitAppointment
                                                    }
                                                >
                                                    <LinearGradient
                                                        start={[0, 0]}
                                                        end={[1, 0]}
                                                        colors={[
                                                            "#F069A2",
                                                            "#EEAEA2"
                                                        ]}
                                                        style={styles.btnLinear}
                                                    >
                                                        <Text
                                                            style={
                                                                styles.btnSaveText
                                                            }
                                                        >
                                                            {this.getText('savepaymentandcharge')}
                                                        </Text>
                                                    </LinearGradient>
                                                </TouchableOpacity>
                                            </View>
                                        </ScrollView>
                                    </View>
                                </View>
                            </ScrollableTabView>
                            <PaymentCash
                                visible={false}
                                ref="paymentCashInput"
                                onSave={this._onSaveCash}
                                value={""}
                                remaining={""}
                                language={this.props.language}
                                
                            />
                            <PaymentCreditCard
                                visible={false}
                                ref="paymentCreditCardInput"
                                onSave={this._onSaveCreditCard}
                                value={""}
                                remaining={""}
                                language={this.props.language}
                            />
                            <Paymentphysical
                                visible={false}
                                ref="paymentPhysicalInput"
                                onSave={this._onSavePhysical}
                                value={""}
                                remaining={""}
                                language={this.props.language}
                                
                            />
                            
                        </View>
                    </ScrollableTabView>

                    <SubmitLoader
                        ref="appointmentLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={this.getText('processing')}
                        color={Colors.spinnerLoaderColorSubmit}
                    />

                    <IconLoader
                        ref="appointmentSuccessLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmitSucccess}
                        textContent={"Appointment Booked"}
                        color={Colors.spinnerLoaderColorSubmit}
                    />

                    <SpinnerLoader
                        visible={this.isShowLoaderAppointmentDetails}
                        textStyle={layout.textLoaderScreen}
                        overlayColor={"#fff"}
                        textContent={this.getText('loadingappointment')}
                        color={Colors.spinnerLoaderColor}
                        ref="appointmentdetailloader"
                    />
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //height: 110
    },
    plusservicecontainer: {
        justifyContent: "center",
        borderBottomWidth: 1 / 2,
        borderColor: "#C8C7CC",
        paddingLeft: 15
    },
    plusservice: {
        color: "#F069A2"
    },
    btnSave: {
        height: 45,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15,
        marginBottom: 15
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 16,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 15,
        right: 15,
        zIndex:1
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1
    },
    summaryTotalContainer: {
        height: 60,
        //backgroundColor: "red",
        flexDirection: "row",
        flexWrap: "wrap",
        borderWidth: 1,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderColor: "#ddd"
        //marginTop: 10
    },
    summaryTotalLeft: {
        justifyContent: "center",
        borderWidth: 1,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderColor: "#ddd",
        alignItems: "center"
    },
    summaryTotalRight: {
        justifyContent: "center",
        alignItems: "center"
    },
    summaryTotalLeftTitle: {
        fontSize: 14
    },
    summaryTotalLeftValue: {
        color: "#F069A2",
        fontSize: 24
    },
    summaryTotalRightValue: {
        fontSize: 24
    },
    paymenthistoryrow: {
        height: 50,
        flexDirection: "row",
        justifyContent: "center",
        borderWidth: 1,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderColor: "#ddd"
    },
    paymenthistoryrowleft: {
        paddingLeft: 15,
        justifyContent: "center"
    },
    paymenthistoryrowright: {
        paddingRight: 15,
        justifyContent: "center"
    },
    paymenthistoryrowdate: {
        color: "#A2A4A7",
        fontSize: 14
    },
    paymenthistoryrowprice: {
        textAlign: "right",
        fontSize: 22
    },
    searchContainer: {     
        height:40,
        justifyContent: 'flex-end'
    },
         textbox:{
        height:40,
        color:'#000',
        paddingRight:20,
        paddingLeft:20,
        fontSize:16,
        backgroundColor:'#fff',
        borderBottomLeftRadius:5,
        borderTopLeftRadius:5,
        fontFamily:'Futura'
    },
          searchbox:{
        position:'absolute',
        zIndex:1,
        right:0,
        width:100,
        height:40,
        justifyContent: 'center',
        alignItems: 'center',
    },
          btnLinearPromo:{
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden",
        width:100,
        height:40,

        borderBottomRightRadius:5,
        borderTopRightRadius:5
    },
    txtsearchtext:{
        color:"#fff"
    }
});
