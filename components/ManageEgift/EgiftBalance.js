import React from "react";
import { StyleSheet, Text, View, TouchableOpacity,Dimensions,Alert, Keyboard, ScrollView, UIManager, TextInput, SectionList } from "react-native";

export default class EgiftBalance extends React.Component {

    state = {
        appIsReady: false
    };
    datalist = [];
    componentWillMount() {
        data = {};
        data.key = "header";
        data.data = this.props.data;
        this.datalist.push(data);
    }
    RefreshData = (datarefresh) =>{
        this.datalist = [];
        data = {};
        data.key = "header";
        data.data = datarefresh;
        this.datalist.push(data);
        this.setState({appIsReady: true});
    }
    _keyExtractor = (item, index) => "egiftsold-"+item.id;

    _renderItem = ({ item }) => {  
        return (
            <View>
            <View style={styles.clientrow}>
                <View style={[styles.bookingrow1]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>Name</Text>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                        <Text style={[styles.clientlbl]}>{item.clientname}</Text>
                    </View>
                </View>
                <View style={[styles.bookingrow1]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>Email</Text>
                    </View>

                    <View >
                        <Text style={styles.clientlbl}>{item.email}</Text>
                    </View>
                    
                </View>

                <View style={[styles.bookingrow1,{marginRight:30}]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>Phone</Text>
                    </View>
                    <View >
                        <Text style={styles.clientlbl}>{item.phone} </Text>
                    </View>
                </View>
                
                <View style={[styles.bookingrow1,{marginRight:30, width: 150}]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>Balance</Text>
                    </View>

                    <View >
                        <Text style={styles.clientlbl}>${item.balance}</Text>
                    </View>

                </View>
            </View>
            <View style={[styles.line]}></View>
            </View>
            
        );
    };
    render() {
        return (
            <SectionList
            ref="egiftsold"
            renderItem={this._renderItem}
            keyExtractor={this._keyExtractor}
            sections={this.datalist}
        />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerContainer:{
        height:90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle:{
        color:'#fff',
        backgroundColor:'transparent',
        fontSize:22,
        fontFamily:'Futura',
        marginTop:10
    },
    row:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    booksuccesstext:{
        fontFamily:'Futura',
        fontSize:22,
        color:'#333',
        textAlign:'center',
        marginTop:10
    },
    confirmbtn:{
        justifyContent: "center",
        alignItems: "center",
        width: 350
    },
    btnSave: {
        height: 45,
        width: 230,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginBottom: 15
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 20,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1,
    },
   
    columnWraperLeft:{
        backgroundColor:'#fff',
        borderWidth: 0.5,
        borderColor: '#ddd',
        marginBottom:15,
    },
    clientrow:{
        flexDirection:'row',
        backgroundColor:'#fff',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:15,
        paddingBottom:15,
        position:'relative',
        // justifyContent: 'center',
        // alignItems: 'center',
        // justifyContent: 'space-between',
    },
    bookingrow:{
        position:'relative', 
    },
    bookingrow1:{
        position:'relative', 
        flex: 4,
        paddingLeft:10
    },
    line:{
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd',
        position:'relative'
    },
    clientlblheader:{
        fontSize:22,
  
        // textTransform: 'uppercase',

    },
    clientlbl:{
        fontSize:16,
        marginBottom:5
    },
    settings:{
        position: 'absolute',
        zIndex: 1,
        left: 10,
        top:40,
        alignItems: 'center', 
        justifyContent: 'center', 
        color:"#fff"
    },
    btnbooknow:{
        color:'#fff',
        textAlign:'center',
        fontSize:10,
        padding:5, 

      },
      colortime:{
          color:'#757373'
      },
      colorused:{
        color:'#F069A2'
    },
      
      righticon:{
          position:'absolute',
          right:0,
          top:8
      },
      removeservice:{
    
        backgroundColor:'red',
        justifyContent:'center',
        paddingRight:10,
        position:'absolute',
        alignItems: "center",
        width:100,
        height:79
    },
    removeservicebtnText:{
        color:'#fff'
    },
});
