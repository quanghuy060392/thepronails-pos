import React from "react";
import { StyleSheet, Text } from "react-native";

export default class AppointmentPaymentTotal extends React.Component {
    state = {
        total: this.props.total
    };

    render() {
        return (
            <Text style={styles.summaryTotalLeftValue}>
                ${this.state.total}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    summaryTotalLeftValue: {
        color: "#F069A2",
        fontSize: 24
    },
});
