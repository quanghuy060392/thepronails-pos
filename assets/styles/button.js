import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    login: {
        backgroundColor:'#fff',
        height:40,
        borderRadius:20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginsocial: {
        height:38,
        borderRadius:20,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor:'#fff',
        borderWidth:1,
    },
    txtLogin: {
        color:'#EF75A4'
    },
    txtLoginSocial: {
        color:'#fff'
    },
    txtSignUp: {
        color:'#fff',
        fontSize:12
    }
});
