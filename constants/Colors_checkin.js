const tintColor = '#2f95dc';

export default {
    tintColor,
    tabIconDefault: '#808080',
    tabIconSelected: '#F069A2',
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
    spinnerLoaderColor: '#F069A2',
    spinnerLoaderColorSubmit:'#fff'
};
