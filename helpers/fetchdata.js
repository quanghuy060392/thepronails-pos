import setting from "../constants/Setting";
import { AsyncStorage, Alert } from "react-native";
import { StackNavigation } from "@expo/ex-navigation";
import {
    getUserData
} from "../helpers/authenticate";
import moment from "moment";
import "../helpers/timezone";
import { getUSState2Digit, get_time_zone } from "../helpers/Utils";
//import collect from "collect.js";

export async function prepareDataForCalendarScreen(token) {
    let prepareData = {};
    //let isLoadedTechnician = false;

    //prepare technicians data
    /*
    var techniciansData = await AsyncStorage.getItem('list-technician').then((data) => {
        //if (data == null) {
        //var technicianList = [];
        var technicianList = await fetchTechniciansData(token);
        console.log(technicianList);
        //return [];

         } else {
         isLoadedTechnician = true;
         prepareData.technicians = JSON.parse(data);
         checkIsLoaded();
         }
        //this.setState({isCheckAuthenticate: true});
    });
*/
    var technicianList = await fetchTechniciansData(token);
    prepareData.technicians = technicianList;
    return prepareData;
    /*
    checkIsLoaded = () => {
        if (isLoadedTechnician) {
            callback(prepareData);
        }
    };*/
}

export async function fetchTechniciansData(token) {
    let userData = await getUserData();
    let revisionData = await getRevisionByKey('technician','technician_revision',userData);
    var technicianList = await fetch(setting.apiUrl + 'get_technicians', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            fetchError(responseJson);
            //Alert.alert('Error', responseJson.message);
            return [];
        } else {
            AsyncStorage.setItem('list-technician', JSON.stringify(responseJson.data));
            updateRevisionData('technician_revision',revisionData,userData);
            
            return responseJson.data;
            // prepareData.technicians = responseJson.data;
            //checkIsLoaded();
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return technicianList;
}
export async function fetchCategoriesData(token) {
    var technicianList = await fetch(setting.apiUrl + 'categories', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            fetchError(responseJson);
            return [];
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return technicianList;
}
export async function fetchListCombo(token) {
    let userData = await getUserData();
    let revisionData = await getRevisionByKey('combo','combo_revision',userData);     
    var services = await fetch(setting.apiUrl + 'get_listcombo', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then((response) => response.json()).then((responseJson) => {

        if (!responseJson.success) {
            fetchError(responseJson);
            //Alert.alert('Error', responseJson.message);
            return [];
        } else {
            AsyncStorage.setItem('listcombo', JSON.stringify(responseJson.data));
            updateRevisionData('combo_revision',revisionData,userData);
            
            return responseJson.data;
            // prepareData.technicians = responseJson.data;
            //checkIsLoaded();
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return services;
}

export async function fetchAppointments(token, date) {  

    var appointments = await fetch(setting.apiUrl + 'appointment/get_all', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            date: date
        })
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            fetchError(responseJson);
            return [];
        } else {
            AsyncStorage.setItem('appointments', JSON.stringify(responseJson.data));
            return responseJson.data;
            // prepareData.technicians = responseJson.data;
            //checkIsLoaded();
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return appointments;
}
export async function fetchCustomercheckins(token, date) {  

    var appointments = await fetch(setting.apiUrl + 'check-in/customercheckin_getall', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            date: date
        })
    }).then((response) => response.json()).then((responseJson) => {

        if (!responseJson.success) {
            fetchError(responseJson);
            return [];
        } else {
            AsyncStorage.setItem('customercheckins', JSON.stringify(responseJson.data));
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return appointments;
}
export async function fetchClientsData(token) {
    let userData = await getUserData();
    let revisionData = await getRevisionByKey('client','client_revision',userData);
    var clientList = await fetch(setting.apiUrl + 'get_clients', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then((response) => response.json()).then((responseJson) => {

        if (!responseJson.success) {
            fetchError(responseJson);
            //Alert.alert('Error', responseJson.message);
            return [];
        } else {
            AsyncStorage.setItem('list-client', JSON.stringify(responseJson.data));
            updateRevisionData('client_revision',revisionData,userData);
            
            return responseJson.data;
            // prepareData.technicians = responseJson.data;
            //checkIsLoaded();
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return clientList;
}

export async function fetchBusinesshours(token) {
    let userData = await getUserData();
    let revisionData = await getRevisionByKey('businesshour','businesshour_revision',userData);

        var businesshours = await fetch(setting.apiUrl + 'get_businesshours', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        }).then((response) => response.json()).then((responseJson) => {
            if (!responseJson.success) {
                fetchError(responseJson);
                //Alert.alert('Error', responseJson.message);
                return [];
            } else {
                AsyncStorage.setItem('businesshours', JSON.stringify(responseJson.data));
                updateRevisionData('businesshour_revision',revisionData,userData);
                
                return responseJson.data;
                // prepareData.technicians = responseJson.data;
                //checkIsLoaded();
            }
        }).catch((error) => {
            console.error(error);
            return [];
        });
        return businesshours;
}

export async function fetchServices(token) {
    let userData = await getUserData();
    let revisionData = await getRevisionByKey('service','service_revision',userData);  
    var services = await fetch(setting.apiUrl + 'app_get_services', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then((response) => response.json()).then((responseJson) => {

        if (!responseJson.success) {
            fetchError(responseJson);
            //Alert.alert('Error', responseJson.message);
            return [];
        } else {
            AsyncStorage.setItem('services', JSON.stringify(responseJson.data));
            updateRevisionData('service_revision',revisionData,userData);
            
            return responseJson.data;
            // prepareData.technicians = responseJson.data;
            //checkIsLoaded();
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return services;
}

export async function fetchTurns(token) {
    let userData = await getUserData();
    stateData = getUSState2Digit(userData.state);
    timezone = get_time_zone('US',stateData);
    let now = moment().tz(timezone).format('YYYY-MM-DD');
    
    var dataKey = await AsyncStorage.getItem(setting.turnkeyday + '_' + now + '_' + userData.id).then(jsonData => {
        if (jsonData == null || jsonData == "") {
            return '';
        } else {
            return jsonData;
        }
    });

    if(dataKey != '' && dataKey != now){
        AsyncStorage.removeItem(setting.turnkeyday + '_' + userData.id);
    }
    
    var data = await AsyncStorage.getItem(setting.turnkey + '_' + userData.id).then(jsonData => {
        if (jsonData == null || jsonData == "") {
            return '';
        } else {
            return JSON.parse(jsonData);
        }
    });
    if(data == ''){
        var dataTurn = await fetch(setting.apiUrl + 'app_get_turns', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        }).then((response) => response.json()).then((responseJson) => {
           
            AsyncStorage.setItem(setting.turnkeyday + '_' + userData.id,now);
            AsyncStorage.setItem(setting.turnkey + '_' + now + '_' + userData.id,JSON.stringify(responseJson.data));    
            return responseJson.data;
        }).catch((error) => {
            console.error(error);
            return [];
        });
        data = dataTurn;
    }
    return data;
}

async function getRevisionByKey(key,storage_key,userData){
   
    let storage_key_owner = userData.id;
    if(userData.role == 9){
        storage_key_owner = userData.serviceprovider_id;
    }

    
    let revision = 0;
    if (typeof (userData.settings[key]) != 'undefined') {
        revision = userData.settings[key] == "" ? 0 : parseInt(userData.settings[key]);
    }
    var data_revision = await AsyncStorage.getItem(storage_key + '_' + storage_key_owner).then(service_revision => {
        if (service_revision == null) return "";
        else return service_revision;
    });
    return {revision,data_revision};
}

function updateRevisionData(storage_key,data,userData){
    
    let storage_key_owner = userData.id;
    if(userData.role == 9){
        storage_key_owner = userData.serviceprovider_id;
    }
    if (data.data_revision == "") {
        AsyncStorage.setItem(storage_key + '_' + storage_key_owner, "0");
    } else AsyncStorage.setItem(storage_key + '_' + storage_key_owner, data.revision.toString());
}


function fetchError(responseJson) {
    if (responseJson.message == 'token_expired' || responseJson.message == 'token_invalid') {

        AsyncStorage.setItem(setting.jwtkey, '');
        this.props.navigator.push('login');
    } else {
        Alert.alert('Error', responseJson.message);
    }
}

export function fetchNotification(token,callback) {
    fetch(setting.apiUrl + "getallnotification", {
        method: "GET",
        headers: {
            Authorization: "Bearer " + token
        }
    })
    .then(response => response.json())
    .then(responseJson => {
        if (!responseJson.success) {
            callback();
        } else {
            callback(responseJson.data);
        }
    })
    .catch(error => {
        console.error(error);
    });
}
export async function fetchLoadingCouponAvailable(bookingday, clientid) {
    var slideList = await fetch(setting.apiUrl + 'loadingCouponAvailable_merchantapp?'+"bookingday="+bookingday+"&clientid="+clientid, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            return [];
        } else {     
            
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });

    return slideList;

}

export async function fetchBlockedTime(token, date) {  
    var blockedtime = await fetch(setting.apiUrl + 'blockedtime/get_all', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            date: date
        })
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            fetchError(responseJson);
            return [];
        } else {
            AsyncStorage.setItem('blockedtime', JSON.stringify(responseJson.data));
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return blockedtime;
}

export async function fetchBlockedTimeDetail(token, id) {  
    var blockedtime = await fetch(setting.apiUrl + 'blockedtime/get_detail', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: id
        })
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            fetchError(responseJson);
            return {};
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return {};
    });
    return blockedtime;
}

export async function fetchGetServicDetail(token, id) {  
    var service = await fetch(setting.apiUrl + 'service/GetserviceDetailMerchantApp', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: id
        })
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            fetchError(responseJson);
            return {};
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return {};
    });
    return service;
}

export async function fetchGetCommissionTachnician(token, id, type = '') {  
    var commission = await fetch(setting.apiUrl + 'technician/GetcommissionTechnician', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: id, 
            date_range: type
        })
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {
            fetchError(responseJson);
            return {};
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return {};
    });
    return commission;
}
export async function fetchCustomercheckinsDetail(token, id) {  
    var appointments = await fetch(setting.apiUrl + 'customercheckin_getdetail', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: id
        })
    }).then((response) => response.json()).then((responseJson) => {

        if (!responseJson.success) {
            fetchError(responseJson);
            return [];
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return appointments;
}
export async function fetchSellEgift(token) {

    var gift = await fetch(setting.apiUrl + 'get_sellegift', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {

            fetchError(responseJson);
            return [];
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return gift;

}
export async function fetchBalanceEgift(token) {

    var gift = await fetch(setting.apiUrl + 'get_balanceegift', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {

            fetchError(responseJson);
            return [];
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return gift;

}

export async function fetchSmsTracking(token, fromdate, todate, timezone) {
    var sms = await fetch(setting.apiUrl + 'smstracking', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            fromdate: fromdate,
            todate: todate,
            timezone: timezone
        })
    }).then((response) => response.json()).then((responseJson) => {
        if (!responseJson.success) {

            fetchError(responseJson);
            return [];
        } else {
            return responseJson.data;
        }
    }).catch((error) => {
        console.error(error);
        return [];
    });
    return sms;

}