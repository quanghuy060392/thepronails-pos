//  Created by react-native-create-bridge
//#import <CardConnectConsumerSDK/CardConnectConsumerSDK.h>
#import "CardConnect.h"
/*
// import RCTBridge
#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#elif __has_include(“RCTBridge.h”)
#import “RCTBridge.h”
#else
#import “React/RCTBridge.h” // Required when used as a Pod in a Swift project
#endif

// import RCTEventDispatcher
#if __has_include(<React/RCTEventDispatcher.h>)
#import <React/RCTEventDispatcher.h>
#elif __has_include(“RCTEventDispatcher.h”)
#import “RCTEventDispatcher.h”
#else
#import “React/RCTEventDispatcher.h” // Required when used as a Pod in a Swift project
#endif*/
#import <React/RCTLog.h>


@interface CardConnect  ()  <CCCSwiperDelegate>

@property (nonatomic, strong) CCCSwiperController *swiper;

@end

@implementation CardConnect
//@synthesize bridge = _bridge;

// Export a native module
// https://facebook.github.io/react-native/docs/native-modules-ios.html
RCT_EXPORT_MODULE();

// Export constants
// https://facebook.github.io/react-native/releases/next/docs/native-modules-ios.html#exporting-constants
/*
- (NSDictionary *)constantsToExport
{
  return @{
           @"EXAMPLE": @"example"
         };
}*/

// Export methods to a native module
// https://facebook.github.io/react-native/docs/native-modules-ios.html
/*
RCT_EXPORT_METHOD(connect: (RCTResponseSenderBlock)callback)
{
    _swiper = [[CCCSwiperController alloc] initWithDelegate:self loggingEnabled:YES];
    
    NSString* volumeString = @"ok";
    //NSNumber* volumeString = [NSNumber numberWithDouble:CardConnectConsumerSDKVersionNumber];
    //NSLog(@"%f",CardConnectConsumerSDKVersionNumber);
    //[CCCAPI instance].endpoint = @"fts.cardconnect.com:6443";
    //[CCCAPI instance].enableLogging = YES;
    callback(@[volumeString]);
    //resolve(volumeString);
  //[self emitMessageToRN:@"EXAMPLE_EVENT" :nil];
    
    //NSLog(@"Worked");
    //console.log('stderr');
    //NSLog(@"%@", @"Worked");
    //[self tellJS];
    RCTLogInfo(@"just log");
}
*/
RCT_EXPORT_METHOD(connect: (NSString *) endpoint)
{
    [CCCAPI instance].endpoint = endpoint;
    [CCCAPI instance].enableLogging = YES;
    _swiper = [[CCCSwiperController alloc] initWithDelegate:self loggingEnabled:YES];
    
}

- (void)deviceStatusHasChanged:(NSString *)state {
    [self sendEventWithName:@"connectionStateHasChanged" body:@{@"state": state}];
}

- (void)swipeError:(NSString *)error {
    [self sendEventWithName:@"swipeError" body:@{@"message": error}];
}

- (void)swipeStarted {
    [self sendEventWithName:@"swipeStarted" body:@""];
}

- (void)swipeSuccess:(CCCAccount *)account {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMyy"];
    NSString *expirationDate = [dateFormatter stringFromDate:account.expirationDate];    
    [self sendEventWithName:@"swipeSuccess" body:@{@"token": account.token, @"expiry": expirationDate, @"accountType": account.accountType, @"name": account.name}];
}


- (NSArray<NSString *> *)supportedEvents {
    return @[@"connectionStateHasChanged", @"swipeError", @"swipeSuccess", @"swipeStarted"];
}


//Async Await
/*
RCT_REMAP_METHOD(exampleMethod,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    NSString* volumeString = @"ok";
    resolve(volumeString);
    
}*/

/*
// List all your events here
// https://facebook.github.io/react-native/releases/next/docs/native-modules-ios.html#sending-events-to-javascript
- (NSArray<NSString *> *)supportedEvents
{
  return @[@"SampleEvent"];
}

#pragma mark - Private methods

// Implement methods that you want to export to the native module
- (void) emitMessageToRN: (NSString *)eventName :(NSDictionary *)params {
  // The bridge eventDispatcher is used to send events from native to JS env
  // No documentation yet on DeviceEventEmitter: https://github.com/facebook/react-native/issues/2819
  [self sendEventWithName: eventName body: params];
}*/

- (void)swiper:(CCCSwiper *)swiper didFailWithError:(NSError *)error completion:(void (^)())completion

{
    RCTLogInfo(@"Error");
    [self swipeError:error.localizedDescription];
    completion();
}

- (void)swiper:(CCCSwiper *)swiper didGenerateTokenWithAccount:(CCCAccount *)account completion:(void (^)())completion
{
    //NSLog(@"ErrorGenerate");
    //RCTLogInfo(@"didGenerateTokenWithAccount");
    if (account)
    {
        //printf("%s\n", account);
        [self swipeSuccess:account];
        completion();
    }
    else
    {
        [self swipeError:@"Read Failed"];
        completion();
    }
}

- (void)swiperDidStartMSR:(CCCSwiper *)swiper
{
    [self swipeStarted];
    //RCTLogInfo(@"MSR Started");
    //[self.view endEditing:YES];
    //[self i_startActivityIndicator];
}

- (void)swiper:(CCCSwiper *)swiper connectionStateHasChanged:(CCCSwiperConnectionState)state
{
    NSString* connectState = @"";
    switch (state) {
        case CCCSwiperConnectionStateConnected:
            connectState = @"Connected";
            break;
        case CCCSwiperConnectionStateDisconnected:
            connectState = @"Disconnected";
            break;
        case CCCSwiperConnectionStateConnecting:
        default:
            connectState = @"Connecting";
            break;
    }
    [self deviceStatusHasChanged:connectState];
     //RCTLogInfo(@"%@", connectState);
    //NSLog(@"Did Connect Swiper");
}

@end
