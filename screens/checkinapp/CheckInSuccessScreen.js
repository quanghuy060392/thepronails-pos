import React from "react";
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Dimensions    
} from "react-native";
import { LinearGradient} from "expo";
import Router from "../../navigation/Router";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import { getUserData } from "../../helpers/authenticate";

var {height, width} = Dimensions.get('window');

export default class CheckInSuccessScreen extends React.Component {
    static route = {
        navigationBar: {
            visible: false
        }
    };

    state = {
        count : 10
    }

    businessName = '';
    interval = 0;


    componentWillUnmount(){
        clearInterval(this.interval);
        Dimensions.removeEventListener("change", () => {});
    }

    componentWillMount(){
        let _this = this;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            _this.setState({ appIsReady: true });
        })
    }

    async componentDidMount(){

        let userData = await getUserData();
        this.businessName = userData.businessname; 
        this.isShowStaffCheckIn = userData.isManageTurn;
        let _this = this;
        this.interval = setInterval(function(){
            let stateData = {..._this.state};
            stateData.count--;
            _this.setState({count: stateData.count});
            if(stateData.count == 0){
                _this.props.navigator.replace(Router.getRoute('home',{businessname: _this.businessName, isBooked: true, isShowStaffCheckIn: _this.isShowStaffCheckIn,logo_app: _this.props.route.params.logo_app}));
            }
        },1000);
    }

    backToHome = () => {
        clearInterval(this.interval);
        this.props.navigator.replace(Router.getRoute('home',{businessname: this.businessName, isBooked: true, isShowStaffCheckIn: this.isShowStaffCheckIn,logo_app: this.props.route.params.logo_app}));
    }

    render() {
        return(
            <View style={{flex:1}}>
                <LinearGradient start={[0, 0]} end={[1, 0]} colors={['#F069A2', '#EEAEA2']} style={styles.containerHeaderSteps}>
                    <View style={styles.headerContainer}>
                        <Text style={styles.headerTitle}>Checked In Successfully</Text>
                    </View>
                </LinearGradient>  
                <View style={styles.container}>
                    <View style={[styles.row,{width: width - 70}]}>
                        <MaterialCommunityIcons
                            name={'calendar-check'}
                            size={100}
                            color={'#F069A2'}
                        />
                        <Text style={styles.booksuccesstext}>Checked In Successfully. Thank you!</Text>
                        <View style={styles.confirmbtn}>
                            <View style={styles.btnSave}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={styles.btnSaveWraper}
                                    onPress={this.backToHome}
                                >
                                    <LinearGradient
                                        start={[0, 0]}
                                        end={[1, 0]}
                                        colors={["#F069A2", "#EEAEA2"]}
                                        style={styles.btnLinear}
                                    >
                                        <Text style={styles.btnSaveText}>Back To Home Screen</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>        
                        </View>
                        <Text style={styles.countDown}>Automatically redirect to home screen in {this.state.count} seconds</Text>
                    </View>
                </View> 
            </View>
            
        )
        
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:-45
    },
    row:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    booksuccesstext:{
        fontFamily:'Futura',
        fontSize:30,
        color:'#333',
        textAlign:'center',
        marginTop:10
    },
    headerContainer:{
        height:90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle:{
        color:'#fff',
        backgroundColor:'transparent',
        fontSize:22,
        fontFamily:'Futura',
        marginTop:10
    },
    confirmbtn:{
        justifyContent: "center",
        alignItems: "center",
        width: 350
    },
    btnSave: {
        height: 45,
        width: 230,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginBottom: 15
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 20,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1
    },
    countDown:{
        marginTop:15,
        fontSize:20,
        fontFamily:'Futura',
        color:'#808080'
         
    }
});
