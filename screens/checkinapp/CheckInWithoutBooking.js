import React from "react";
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Alert,
    AsyncStorage,
    Platform 
} from "react-native";
import { LinearGradient} from "expo";
import ScrollableTabView from "react-native-scrollable-tab-view";
import HideTabBar from "../../components_checkin/scrolltab/HideTabBar";
import ListCategories from "../../components_checkin/services/ListCategories";
import Steps from "../../components_checkin/steps/stepswithoutbooking";
import moment from 'moment';
import layout from "../../assets/styles/layout_checkin";
import Colors from "../../constants/Colors_checkin";
import SubmitLoader from "../../helpers/submitloader";
import setting from "../../constants/Setting";
import Router from "../../navigation/Router";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {getDeviceId} from "../../helpers/authenticate";
import Customer from "../../components_checkin/clients/CustomerWithoutBooking";
import Summary from "../../components_checkin/summaryWithoutBooking";

export default class CheckInWithoutBookingScreen extends React.Component{
    static route = {
        navigationBar: {
            visible: false
        }
    };
    
    clientData = '';

    deviceid = 0;
    selectServices = [];

    userData = this.props.route.params.userData;
    isClientExists = this.props.route.params.isClientExists;
    clientSearchData = this.props.route.params.clientData;
    
 
    async componentWillMount() {
        this.deviceid = await getDeviceId();
        
    }

    onPressClient = (client) => {
        
        this.clientData = client;
        this.refs.steps.setStep(2);
        this.refs.tabs.goToPage(1);
    }

    onClientUpdated = (client) => {
        this.clientData = client;
        this.refs.steps.setStep(2);
        this.refs.tabs.goToPage(1);
    }

    onPressService = (services) => {

        //this.serviceId = service.id;
        //this.refs.steps.setStep(3);
        //this.refs.tabs.goToPage(2);
        //this.selectServices[service.id] = service;

        this.selectServices = services;
        //this.setTechnicianByService();
        //this.processServiceTechnician();
    }

    async onPressStep(stepNumber,currentStep){
        switch(stepNumber){
            case 1:
                this.refs.steps.setStep(1);
                this.refs.tabs.goToPage(0);
                break;
            case 2:
                if(currentStep == 1){
                    Alert.alert('Warning','Please input customer details then press Next');         
                }else{
                    this.refs.steps.setStep(2);
                    this.refs.tabs.goToPage(1); 
                }
                
                break;
            case 3:
                let isValid = Object.keys(this.selectServices).length;
                if(!isValid){
                    Alert.alert('Error','Please choose service');        
                }else{
                    this.refs.steps.setStep(3);
                    this.refs.tabs.goToPage(2);
                    let _this = this;
                    setTimeout(function(){
                        _this.setSummaryData(_this);
                    },0);
                }

                
                
                break;
            
        }
          
    }

    onShowSummary = () => {
        this.refs.steps.setStep(3);
        this.refs.tabs.goToPage(2);
        let _this = this;
        setTimeout(function(){
            _this.setSummaryData(_this);
        },0);
    }

    setSummaryData = (_this) => {
        _this.refs.summary.setClient(_this.clientData, _this.props.route.params.userData);
        _this.refs.summary.setService(_this.selectServices);
        //console.log(this.techniciansSelected);
        //console.log(_this.selectedHour);
        //let hourformat = _this.convertTo24Hour(_this.selectedHour).replace(' ','');
        //console.log(hourformat);
        //_this.refs.summary.setTime(moment(this.selectedDay.format('Y-MM-DD') + ' ' + hourformat + ':00').format('dddd, MMMM DD, h:mm A'),
        //    hourformat, this.selectedDay.format('dddd'), this.selectedDay.format('Y-MM-DD'));
        _this.refs.summary.setReady();    
        
    }

    onCheckIn = (reward_point) => {
        this.refs.appointmentLoader.setState({ visible: true });
        let submitData = {};
        submitData.client_id = this.clientData.id;
        submitData.rewardpoint = reward_point;
        if(this.clientData.id == 0){
            submitData.client_data = this.clientData;        
        }
        submitData.categories = this.selectServices;
      
        fetch(setting.apiUrl + "customer/checkin", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.props.route.params.token
            },
            body: JSON.stringify(submitData)
        })
        .then(response => response.json())
        .then(responseJson => {
            
            if (!responseJson.success) {
                //console.log(responseJson);
                this.refs.appointmentLoader.setState({
                    visible: false
                });
                let _this = this;
                setTimeout(function(){
                    _this.fetchError(responseJson);
                },0);
                
                //Alert.alert('Error', responseJson.message);
                //return [];
            } else {
                
                this.refs.appointmentLoader.setState({
                    visible: false
                });
                this.props.navigator.push(Router.getRoute('WithoutBookingSuccessScreen',{logo_app: this.props.route.params.logo_app, data: responseJson}));
            }
        })
        .catch(error => {
            console.error(error);
            //return [];
        });
    }

    

    fetchError(responseJson) {
        if (
            responseJson.message == "token_expired" ||
            responseJson.message == "token_invalid"
        ) {
            this.props.navigator.push('login');
        } else {
            Alert.alert("Error", responseJson.message);
            //console.log(responseJson.message);
        }
    }

   

    close = () => {
        this.props.navigator.push(Router.getRoute('home',{businessname: this.props.route.params.userData.businessname, isBooked: true, isShowStaffCheckIn: this.props.route.params.isShowStaffCheckIn,
            logo_app: this.props.route.params.logo_app}));
    }

    render() {
        let containerHeaderStepsStyle = styles.containerHeaderSteps;
        let headerStyle = styles.headerContainer;
        if(Platform.OS != 'ios'){
            headerStyle = styles.headerContainerAndroid;
            containerHeaderStepsStyle = styles.containerHeaderStepsAndroid;
        }
        return (
            <View style={styles.container}>
                <LinearGradient start={[0, 0]} end={[1, 1]} colors={['#F069A2', '#EEAEA2']} style={containerHeaderStepsStyle}>
                    <View style={headerStyle}>
                        <Text style={styles.headerTitle}>Check In</Text>
                        <TouchableOpacity style={styles.closebtn} activeOpacity={1}
                            onPress={this.close}>
                            <MaterialCommunityIcons
                                name={'close'}
                                size={30}
                                color={'rgba(255,255,255,1)'} style={styles.navIconIOS}
                            />
                        </TouchableOpacity>
                    </View>
                    <Steps step={1} ref='steps' onPress={async(stepNumber,currentStep) => { await this.onPressStep(stepNumber,currentStep)}}/>
                </LinearGradient>    
                <View style={styles.containerTabs}>
                
                    <ScrollableTabView
                        ref="tabs"
                        renderTabBar={() => <HideTabBar />}
                        locked={true}
                    >
                        
                        <View style={{flex:1}}>
                            <Customer clientSearchData={this.clientSearchData} isClientExists={this.isClientExists} onPress={this.onPressClient} token={this.props.route.params.token} onClientUpdated={this.onClientUpdated}/>
                        </View>
                        
                        <View style={{ flex: 1 }}>
                            <ListCategories onPress={this.onPressService} onSelectedServices={this.goToTimePickerTab} 
                                listcombo={this.props.route.params.listcombo} categories={this.props.route.params.categories} 
                                onSave={this.onShowSummary} userData={this.userData}/>    
                        </View>

                        <View style={{ flex: 1 }}>
                            <Summary ref='summary' onPress={this.onCheckIn}/>
                        </View>
                        {/*
                        <View style={{ flex: 1 }}>
                            <TimePicker availablehours={this.props.route.params.availablehours} onPress={async (day,hour,arrTechniciansAndHours) => { await this.onPressTime(day,hour,arrTechniciansAndHours);}}
                                blockedTime={this.props.route.params.blockedTime}
                                TechniciansWorkingHour={this.props.route.params.TechniciansWorkingHour}
                                userData={this.props.route.params.userData}
                                loadedDataYM={this.props.route.params.blockedTimeYM}
                                token={this.props.route.params.token}
                                services={this.props.route.params.services}
                                opening_hours={this.props.route.params.opening_hours}
                                ref='timepicker'/>
                        </View>
                        <View style={{ flex: 1 }}>
                            <ListTechnician ref='technician' technicians={this.props.route.params.technicians} onPress={this.onPressTechnician}
                                    services={this.props.route.params.services} showSummary={() => {this.onPressStep(5)}}
                                    onCheckValidHour={this.checkValidHour}
                                 />
                        </View>
                        <View style={{ flex: 1 }}>
                            <Summary ref='summary' onPress={this.saveAppointment} token={this.props.token}/>
                        </View>
                        */}
                    </ScrollableTabView>
                    
                    <SubmitLoader
                        ref="appointmentLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={"Processing..."}
                        color={Colors.spinnerLoaderColorSubmit}
                    />
                </View>       
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerTabs:{
        flex: 1,
        backgroundColor:'#f2f2f2'
    },
    containerHeaderSteps:{
        height:170
    },
    containerHeaderStepsAndroid:{
        height:185
    },
    headerContainer:{
        height:90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerContainerAndroid:{
        height:90,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop:20
    },
    headerTitle:{
        color:'#fff',
        backgroundColor:'transparent',
        fontSize:24,
        fontFamily:'Futura'
    },
    closebtn:{
        position:'absolute',
        right:20,
        backgroundColor:'transparent',
        top:30
    }
})