import React from "react";
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    ActivityIndicator,
    TouchableOpacity,
    AlertIOS,
    Alert,
    Image,
    TextInput,
    ScrollView,
    Keyboard,
    Platform,
    AsyncStorage
} from "react-native";
import { LinearGradient , Permissions, ScreenOrientation} from "expo";
import { getBackground, getLogo } from "../../api/Assets";
import Router from "../../navigation/Router";
import {
    fetchTechniciansData,
    fetchClientsData,
    fetchBusinesshours,
    fetchServices,
    fetchBlockedTime,
    fetchTechniciansWorkingHour,
    fetchListCombo,
    getCurrentLocation
} from "../../api/fetchdata";

import {
    jwtToken,
    getUserData
} from "../../helpers/authenticate";
import moment from 'moment';
import SpinnerLoader from "../../helpers/spinner";
import layout from "../../assets/styles/layout_checkin";
import Colors from "../../constants/Colors_checkin";
import SubmitLoader from "../../helpers/submitloader";
import AlertLoader from "../../helpers/loaderalert";
import setting from "../../constants/Setting";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import ImageResponsive from 'react-native-scalable-image';
import ScreenSaver from './ScreenSaver';
import { formatPhone } from "../../helpers/Utils";
import emailvalidator from "email-validator";
import { getUSState2Digit, get_time_zone } from "../../helpers/Utils";
import "../../helpers/timezone";
import Prompt from "../../components_checkin/Prompt";
import ModalBarCode from "../../components_checkin/ModalBarCode";
import ProfileTechnicianCheckIn from "../../components_checkin/ProfileTechnicianCheckIn";

var {height, width} = Dimensions.get('window');
var scrollviewpadding = 0;
let checkinfontsize = 40;
if(width > 768){
    checkinfontsize = 45;
}
export default class HomeScreen extends React.Component {
    static route = {
        navigationBar: {
            visible: false
        }
    };
    background_app = '';
    logo_app = '';
    logo_width = 0;
    logo_height = 0;
    technicians = [];
    clients = [];
    availablehours = [];
    services = [];
    blockedTime = [];
    categories = [];
    listcombo = [];
    TechniciansWorkingHour = [];
    userData = {};
    YM = moment().format('YMM');
    loadedYM = {};
    isLoaded = false;
    sized = false;
    state = {
        appIsReady: false,
        search: '',
        maxLength: 10000
    };
    isShowStaffCheckIn = false;
    intervalCount = 0;
    isOnScreenSaver = false;
    isAvoidKeyBoard = false;
    keyboardheight = 0;
    opening_hours = [];
    checkintext = 'Check In';
    qrCodePermissionStatus = false;
    businessname= '';

    componentWillUnmount(){
        //console.log('ok');
        this.keyboardDidShowListener.remove();
        Dimensions.removeEventListener("change", () => {});
    }



    async componentWillMount() {
        /*
        setTimeout(() => {
            ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
            //_this.setState({ appIsReady: true });
        },2000)*/
        //console.log('home');
        let qrCode = await Permissions.askAsync(Permissions.CAMERA);
        this.qrCodePermissionStatus = qrCode.status == 'granted';
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));

        this.businessname = this.props.route.params.businessname;
        if(typeof(this.props.route.params.isShowStaffCheckIn) != 'undefined'){
            this.isShowStaffCheckIn = this.props.route.params.isShowStaffCheckIn;
        }
        
        var screen = Dimensions.get('window');
        width = screen.width;
        height = screen.height;
        if(width > 768){
            checkinfontsize = 45;
        }else{
            checkinfontsize = 40;
        }
        if(typeof(this.props.route.params.isBooked) != 'undefined'){
            if(typeof(this.props.route.params.logo_app) != 'undefined'){
                this.logo_app = this.props.route.params.logo_app;
            }
            if(typeof(this.props.route.params.isDisableBooking) != 'undefined'){
                this.checkintext = 'Search';
            }
            
            this.setState({ appIsReady: true });
            await this.loadData(true);
        }else{
            await this.loadData(false);
        }
        
        let _this = this;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            height = screen.height;
            if(width > 768){
                checkinfontsize = 84;
            }else{
                checkinfontsize = 74;
            }
            
            _this.setState({ appIsReady: true });
        })
        if(!this.userData.isDisableHowItWork){
            this.keepawake();
        }

        
        //console.log(this.qrCodePermissionStatus);

        //this.refs.scrollview.scrollTo({x: 0, y: 300, animated: true});
        //this.setState({ appIsReady: true });
    }
    /*
    async componentDidMount() {
        this.refs.scrollview.scrollTo({x: 0, y: 300, animated: true});
    }*/

    async loadData(isReady){
        //let background_app_link = await getBackground();
        let logo_app = await getLogo();
        //let logo_app = '';
        let background_app_link = '';
        if(background_app_link != '' && background_app_link != null){
            this.background_app = background_app_link;
        }
        
        if(logo_app != '' && logo_app != null){
            this.logo_app = logo_app;
            /*
            Image.getSize(logo_app, async (width, height) => {
                this.logo_width = width;
                this.logo_height = height;
                if(this.logo_height > 100 && this.logo_height > this.logo_width){
                    this.logo_width *= 100 / this.logo_height;   
                    this.logo_height = 100;   
                }
                console.log(this.logo_height );
                this.sized = true;
                this.setState({ appIsReady: true });
            });
            */
        }
     
        this.token = await jwtToken();
        this.userData = await getUserData();
        this.stateData = getUSState2Digit(this.userData.state);
        this.timezone = get_time_zone('US',this.stateData);
        this.technicians = [];
        this.location = await getCurrentLocation();
        this.locationId = 0;
        if(typeof(this.location) != 'undefined' && typeof(this.location.id) != 'undefined'){
            this.multipleLocation = true;
            this.locationId = this.location.id;
        }
        
        if(typeof(this.userData.showLessServicesWhenDisableCheckInBooking) != 'undefined'){
            if(this.userData.showLessServicesWhenDisableCheckInBooking){
                this.userData.isDisableCheckInAppBooking = 0;
            }
        }
        
        if(this.userData.isDisableCheckInAppBooking){
            this.checkintext = 'Search';
        }

        if(!this.userData.isManageTurn){
            this.technicians = await fetchTechniciansData(this.token, this.locationId);  
        }else{
            this.isShowStaffCheckIn = true;
        }
        //this.clients = await fetchClientsData(this.token);
        this.availablehours = await fetchBusinesshours(this.token);
        this.opening_hours = this.availablehours.opening_hours;

        
        this.blockedTime = await fetchBlockedTime(this.token,this.YM);
        this.TechniciansWorkingHour = await fetchTechniciansWorkingHour(this.token);   
        //console.log(this.TechniciansWorkingHour);
        this.loadedYM[this.YM] = this.YM;

        
        
        
        //opening_hours
        //console.log(this.opening_hours);
        this.services = await fetchServices(this.token, this.locationId);
        this.services = this.services.filter(function(item){         
            return item.isAddOn != 1;
        });

        //console.log(this.services);

        
        let _this = this;
        let categoriesDisplay = [];
        let tempCategory = [];
        this.services.forEach(function(item){
            let category = tempCategory.filter(function(itemCategory){
                if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                    return itemCategory == item.category_customname;
                }else{
                    return itemCategory == item.category_name;
                }
                
            });
            if(!category.length){
                let cat_name = item.category_name;
                if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                    
                    cat_name = item.category_customname;
                }/*else{
                    _this.categories.push(item.category_name);
                }*/
                tempCategory.push(cat_name);  
                let categoriesDisplayData = {};
                categoriesDisplayData.name = cat_name;
                categoriesDisplayData.ordering = item.category_ordering;
                categoriesDisplayData.id = item.category_id;
                categoriesDisplayData.originname = item.category_name;
                categoriesDisplayData.category_backgroundcolor = item.category_backgroundcolor
                categoriesDisplay.push(categoriesDisplayData);
            }
        });

        categoriesDisplay = categoriesDisplay.sort(function (a, b) {
            if (a.ordering < b.ordering) return -1;
            else if (a.ordering > b.ordering) return 1;
            return 0;
          });
          
          categoriesDisplay.forEach(function(item){
            _this.categories.push(item.name);
          })

        this.listCategories = categoriesDisplay;  

        if(this.userData.isDisableCheckInAppBooking){
            let categoryCheckIn = [];
            this.listCategories.forEach((itemCategory) => {
                let serviceInCategories = this.services.filter(function(item){
                    if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                        return item.category_customname == itemCategory.name && item.status == 'YES';
                    }else{
                        return item.category_name == itemCategory.name && item.status == 'YES';
                    }
                    
                });  
                if(serviceInCategories.length){
                    categoryCheckIn.push(itemCategory);
                }           
            })

            this.listCategories = categoryCheckIn;
        }else{
            if(typeof(this.userData.showLessServicesWhenDisableCheckInBooking) != 'undefined'){
                if(this.userData.showLessServicesWhenDisableCheckInBooking){
                    let categoryCheckIn = [];
                    this.listCategories.forEach((itemCategory) => {
                        let serviceInCategories = this.services.filter(function(item){
                            if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                                return item.category_customname == itemCategory.name && item.status == 'YES';
                            }else{
                                return item.category_name == itemCategory.name && item.status == 'YES';
                            }
                            
                        });  
                        if(serviceInCategories.length){
                            categoryCheckIn.push(itemCategory);
                        }           
                    })

                    this.listCategories = categoryCheckIn;
                }
            }
        }
          /*
        _this.categories = _this.categories.sort(function (a, b) {
            if (a < b) return -1;
            else if (a > b) return 1;
            return 0;
          });*/
        
        this.listcombo = await fetchListCombo(this.token);
        this.isLoaded = true;
        //console.log(this.token);
        if(!isReady){
            this.setState({ appIsReady: true });
        }
    }

    async checkin (){
        if(!this.isLoaded){
            this.refs.SpinnerLoader.setState({ visible: true });
            await this.loadData(false);
        }
        this.props.navigator.push(Router.getRoute('checkin',{
            clients: this.clients,
            technicians: this.technicians,
            availablehours: this.availablehours.available_hours,
            services: this.services,
            token:this.token,
            blockedTime: this.blockedTime,
            TechniciansWorkingHour: this.TechniciansWorkingHour,
            userData: this.userData,
            blockedTimeYM: this.loadedYM,
            listcombo: this.listcombo,
            categories: this.categories,
            isShowStaffCheckIn: this.isShowStaffCheckIn,
            logo_app: this.logo_app
        }));
    }

    checkincustomer = async () =>{
        if(!this.isLoaded){
            this.refs.SpinnerLoader.setState({ visible: true });
            await this.loadData(false);
        }
        this.props.navigator.push(Router.getRoute('CustomerCheckIn',{
            clients: this.clients,
            token: this.token,
            businessname: this.businessname,
            logo_app: this.logo_app
        }));
    }

    cancelSetting = () => {
        this.keepawake();
        this.isOnScreenSaver = false;
    }

    openSetting = () => {
        this.isOnScreenSaver = true;
        this.refs.logout.show();
        /*
        AlertIOS.prompt(this.props.route.params.businessname,'Enter your merchant account password to access setting section',[
            {text: 'Cancel', style: 'cancel', onPress: this.cancelSetting},
            {text: 'OK', onPress: async password => await this.checkAuthorized(password)},
          ],'secure-text');*/    
    }
   async openSalon (){
        var checkcache = await AsyncStorage.getItem('salonlogin');
        if(checkcache ==  null){
            this.isOnScreenSaver = true;
            this.refs.salonpage.show(); 
        }else{
            this.props.navigator.push(Router.getRoute('rootNavigation',{language: "en-US"}));
        }
  
    }

    onClosePrompt = () => {
        this.keepawake();
        this.isOnScreenSaver = false;
    }

    cancelStaffCheckIn = () => {
        this.keepawake();
        this.isOnScreenSaver = false;
    }

    openStaffCheckIn = () => {
        
        this.isOnScreenSaver = true;
        this.refs.prompt.show();
        /*
        AlertIOS.prompt('Staff Check In','Enter your passcode to check in',[
            {text: 'Cancel', style: 'cancel', onPress: this.cancelStaffCheckIn},
            {text: 'OK', onPress: async password => await this.checkInWithPassCode(password)},
          ],'secure-text');  */
    }

    checkInWithPassCode = async (password, type) => {
        //this.refs.prompt.setState({animationType:'none'});
        this.keepawake();
        this.isOnScreenSaver = false;
        if(String.prototype.trim.call(password) == '')
        {
            Alert.alert('Error', 'Please enter passcode');
        }else{
            this.refs.authenticateLoader.setState({ visible: true });
            
            this.refs.prompt.close();
            let _this = this;
            let isSuccess = await fetch(setting.apiUrl + 'technician-checkin',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.token
                },
                body: JSON.stringify({
                    passcode: password,
                    type: type
                })
            }).then((response) => response.json()).then((responseJson) => {
                //console.log(responseJson);
                //this.refs.authenticateLoader.setState({ visible: false });
                if(!responseJson.success)
                {
                    setTimeout(function(){
                        _this.refs.authenticateLoader.setState({ visible: false });
                        setTimeout(function(){
                            Alert.alert('Error', responseJson.message);        
                        },200);
                    },1000)
                   
                    return false;
                }else
                {
                    setTimeout(function(){
                        _this.refs.authenticateLoader.setState({ visible: false });
                        
                        _this.refs.ProfileTechnicianCheckIn.setState({ 
                            name: responseJson.name,
                            phone: responseJson.phone,
                            email: responseJson.email,
                            message: responseJson.message,
                            datecheckin: responseJson.datecheckin,
                            datecheckout: responseJson.datecheckout,
                        });
                        _this.refs.ProfileTechnicianCheckIn.show();
                        setTimeout(function() {
                            _this.refs.CheckInLoader.setState({
                                visible: false
                            });
    
                            
                        }, 2000);
                    },1000)
                    
                    return true;    
                    //this.props.navigator.push(Router.getRoute('home'));
                }
                
            }).catch((error) => {
                console.error(error);
            });
            
        }
    }

    async checkAuthorized(password, type) {        
        this.keepawake();
        this.isOnScreenSaver = false;
        if(String.prototype.trim.call(password) == '')
        {
            Alert.alert('Error', 'Please enter password');
        }else{

            this.refs.authenticateLoader.setState({ visible: true });
            if(typeof(type) != "undefined" && type == "salonpage"){
                this.refs.salonpage.close();
            }else{
                this.refs.logout.close();
            }
            

            let _this = this;
            /*
            let _this = this;
            setTimeout(function(){
                _this.refs.authenticateLoader.setState({ visible: false });
            },1000)*/
            
            
            let isSuccess = await fetch(setting.apiUrl + 'authorize_check',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.token
                },
                body: JSON.stringify({
                    password: password
                })
            }).then((response) => response.json()).then((responseJson) => {
                //this.refs.authenticateLoader.setState({ visible: false });
                if(!responseJson.success)
                {
                    setTimeout(function(){
                        _this.refs.authenticateLoader.setState({ visible: false });
                        setTimeout(function(){
                            Alert.alert('Error', responseJson.message);        
                        },200);
                    },1000)
                    
                    return false;
                }else
                {
                    //console.log('valid');
                    return true;    
                    //this.props.navigator.push(Router.getRoute('home'));
                }
                
            }).catch((error) => {
                console.error(error);
            });
            if(isSuccess){

                if(typeof(type) != "undefined" && type == "salonpage"){
                    AsyncStorage.setItem('salonlogin', JSON.stringify({login: true}));
                    clearInterval(this.intervalCount);
                    setTimeout(function(){
                        _this.refs.authenticateLoader.setState({ visible: false });
                        _this.props.navigator.push(Router.getRoute('rootNavigation',{language: "en-US"}));
                    },1000)
                }else{
                    clearInterval(this.intervalCount);
                    setTimeout(function(){
                        _this.refs.authenticateLoader.setState({ visible: false });
                        _this.props.navigator.push(Router.getRoute('setting',{businessname:_this.props.route.params.businessname, token: _this.token }));
                    },1000)
                }
                
            }
        }
        
    }

    closeScreenSaver = () => {
        this.keepawake();
        this.isOnScreenSaver = false;
    }

    keepawake = () => {
        if(!this.userData.isDisableHowItWork){
            clearInterval(this.intervalCount);
            let _this = this;
            this.intervalCount = setInterval(function(){
                const { currentNavigatorUID, navigators } = _this.props.navigation.navigationState;
                const { index, routes } = navigators[currentNavigatorUID];
                const { routeName } = routes[index];
                if(routeName == 'home' && !_this.isOnScreenSaver){
                    _this.isOnScreenSaver = true;
                    _this.refs.ScreenSaver.show();
                    //_this.refs.ModalHowitwork.show();
                }
            //},0);      
            },60 * 3000);        
        }
        
        
    }

    onFocus = (isScroll) => {
        this.keepawake();
        this.isOnScreenSaver = true;
        this.isAvoidKeyBoard = true;
        if(isScroll){
            if(Platform.OS === 'ios'){
                
                this.refs.scrollview.scrollTo({x: 0, y: this.keyboardheight, animated: true});
            }else{
                
                //scrollviewpadding = Dimensions.get('window').height;
                //console.log(scrollviewpadding);
                //this.setState({rerender:true});
                //console.log(this.keyboardheight);
                //this.refs.scrollview.scrollTo({x: 0, y: this.keyboardheight, animated: true});
                this.setState({rerender: true});
            }    
            
        }
            
    }

    onBlur = () => {
 
        this.keepawake();
        this.isOnScreenSaver = false;
        this.isAvoidKeyBoard = false;
        if(Platform.OS === 'ios'){
            this.refs.scrollview.scrollTo({x: 0, y: 0, animated: true});
        }
        
        this.setState({rerender:true});
    }

    _keyboardDidShow(e) {
        
        //let keyboardHeight = Dimensions.get('window').height - e.endCoordinates.height;
        this.keyboardheight = e.endCoordinates.height;
        if(this.isAvoidKeyBoard){
           
            this.onFocus(true);
            this.setState({rerender:true});
        }

    }

    changeSearch = (value) => {
        var _this = this;
        value = String.prototype.trim.call(value);
        value = value.replace('(','');
        value = value.replace(')','');
        value = value.replace(' ','');
        value = value.replace('-','');
        if(value.length >= 3 && !isNaN(value)){
            let formatValue = formatPhone(value);
            _this.setState({search: formatValue, maxLength: 14});
        }
        else{
            _this.setState({search: value, maxLength:10000});
        }
    }

    searchdata = async () => {
        let inputData = this.state.search;
        inputData = inputData.replace('(','');
        inputData = inputData.replace(')','');
        inputData = inputData.replace(' ','');
        inputData = inputData.replace('-','');
        
        if(String.prototype.trim.call(this.state.search) == ''){
            Alert.alert('Error','Please input Phone or Email');       
        }else if(!isNaN(inputData) && this.state.search.length != 14){
            Alert.alert('Error','Please input an valid Phone');       
        }else if(isNaN(inputData) && !emailvalidator.validate(String.prototype.trim.call(this.state.search))){
            Alert.alert('Error','Please input an valid Email');      
        }else{
            let isPhone = !isNaN(inputData) && this.state.search.length == 14;
            let _this = this;    
            this.refs.authenticateLoader.setState({ visible: true });
            let clientData = {email: '', phone: ''};
            if(isPhone) {
                clientData.phone = this.state.search;
            }else clientData.email = this.state.search;
            let isClientExists = false;
            let appointments = [];
            let isNewCheckIn = true;
            await fetch(setting.apiUrl + "checkin/client/search?search="+inputData, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    Authorization: "Bearer " + this.token
                }
            })
            .then(response => response.json())
            .then(responseJson => {
                //console.log(responseJson);
                if (responseJson.success) {
                    isClientExists = true;
                    clientData = responseJson.data;
                }
                //console.log(clientData);

                
            })
            .catch(error => {
                console.error(error);
                //return [];
            });

            if(!this.isLoaded){
                await this.loadData(false);
                if(!isClientExists){
                    this.refs.authenticateLoader.setState({ visible: false });
                }
            }else if(!isClientExists){
                this.refs.authenticateLoader.setState({ visible: false });
            }


            if(isClientExists){
                var isSuccess = await fetch(setting.apiUrl + "checkin/get?id=" + clientData.id, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        Authorization: "Bearer " + this.token
                    }
                })
                .then(response => response.json())
                .then(responseJson => {
                    if (!responseJson.success) {
                        return true;
                    } else {
                        appointments = responseJson.data;
                        return true;
                    }
                })
                .catch(error => {
                    console.error(error);
                    return false;
                });
                this.refs.authenticateLoader.setState({ visible: false });
                if (isSuccess) {
                    if(appointments.length){
                        let listAppointmentAvailable = [];  
                        appointments.forEach(function(item){
                            let diff = moment.tz(item.startdatetime,_this.timezone).diff(moment().tz(_this.timezone),'minutes');
                            
                            //console.log(moment().tz(_this.timezone).diff(moment.tz(item.startdatetime,_this.timezone),'minutes'));
                            //let isValidTime = moment().tz(_this.timezone).isBefore(moment.tz(item.startdatetime,_this.timezone));
                            let isValidTime = false;
                            if(diff >= -20){
                                isValidTime = true;
                            }
                            if(item.isCheckedIn != 1 && isValidTime){
                                listAppointmentAvailable.push(item);
                            }        
                        });
                        //console.log(listAppointmentAvailable);
                        if(listAppointmentAvailable.length){
                            isNewCheckIn = false;
                            clearInterval(this.intervalCount);
                            this.props.navigator.push(Router.getRoute('Appointments',{
                                appointments: appointments,
                                client: clientData,
                                clients: this.clients,
                                token: this.token,
                                businessname: this.businessname,
                                isShowStaffCheckIn: this.isShowStaffCheckIn,
                                opening_hours:this.opening_hours,  

                                technicians: this.technicians,
                                availablehours: this.availablehours.available_hours,
                                services: this.services,
      
                                blockedTime: this.blockedTime,
                                TechniciansWorkingHour: this.TechniciansWorkingHour,
                                userData: this.userData,
                                blockedTimeYM: this.loadedYM,
                                listcombo: this.listcombo,
                                categories: this.categories,
                                isClientExists: isClientExists,
                                clientData: clientData,
                                logo_app: this.logo_app,
                                listCategories: this.listCategories

                            }));
                        }
                    }
                    
                }
                   
               
            }

            if(!this.userData.isDisableCheckInAppBooking){
                if(isNewCheckIn){
                    clearInterval(this.intervalCount);
                    this.props.navigator.push(Router.getRoute('checkin',{
                        clients: this.clients,
                        technicians: this.technicians,
                        availablehours: this.availablehours.available_hours,
                        services: this.services,
                        token:this.token,
                        blockedTime: this.blockedTime,
                        TechniciansWorkingHour: this.TechniciansWorkingHour,
                        userData: this.userData,
                        blockedTimeYM: this.loadedYM,
                        listcombo: this.listcombo,
                        categories: this.categories,
                        isShowStaffCheckIn: this.isShowStaffCheckIn,
                        isClientExists: isClientExists,
                        clientData: clientData,
                        opening_hours:this.opening_hours,
                        logo_app: this.logo_app,
                        listCategories: this.listCategories
                    }));
                } 
            }else{
                /*if(isClientExists){
                    this.refs.authenticateLoader.setState({ visible: false });
                }*/
                clearInterval(this.intervalCount);
                this.props.navigator.push(Router.getRoute('checkinwithoutbooking',{
                    token:this.token,
                    userData: this.userData,
                    listcombo: this.listcombo,
                    categories: this.listCategories,
                    isClientExists: isClientExists,
                    clientData: clientData,
                    logo_app: this.logo_app
                }));
            }
            
                
            

             
        }
    }

    scanQrcode = async () => {

        
        if(this.qrCodePermissionStatus){
            clearInterval(this.intervalCount);
            let orien = width > height ? 'lanscape' : 'portrait';
            ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
            this.refs.ModalBarCode.show(orien);
        }

        //Expo.Util.reload();
    }

    refresh = () => {
        Expo.Util.reload();
    }

    scannedBarCode = async (appointmentId) => {
        this.refs.ModalBarCode.close();
        setTimeout(async () => {
            let appointment = {};
            let clientData = {};
            let errMessage = '';
            this.refs.authenticateLoader.setState({ visible: true });
            var isSuccess = await fetch(setting.apiUrl + "checkin/get/qrcode?id=" + appointmentId, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    Authorization: "Bearer " + this.token
                }
            })
            .then(response => response.json())
            .then(responseJson => {
                if (!responseJson.success) {
                    errMessage = responseJson.msg;
                    return true;
                } else {
                    appointment = responseJson.data;
                    clientData = responseJson.client;
                    return true;
                }
            })
            .catch(error => {
                console.error(error);
                return false;
            });
            this.refs.authenticateLoader.setState({ visible: false });
            if (isSuccess) {
                if(typeof(appointment.id) != 'undefined'){
                    let listAppointmentAvailable = [];  
                    let diff = moment.tz(appointment.startdatetime,this.timezone).diff(moment().tz(this.timezone),'minutes');          
                    //console.log(moment().tz(_this.timezone).diff(moment.tz(item.startdatetime,_this.timezone),'minutes'));
                    //let isValidTime = moment().tz(_this.timezone).isBefore(moment.tz(item.startdatetime,_this.timezone));
                    let isValidTime = false;
                    if(diff >= -20){
                        isValidTime = true;
                    }
                    if(appointment.isCheckedIn != 1 && isValidTime){
                        listAppointmentAvailable.push(appointment);
                    }   
                    //console.log(listAppointmentAvailable);
                    if(listAppointmentAvailable.length){
                        isNewCheckIn = false;
                        clearInterval(this.intervalCount);
                        this.props.navigator.push(Router.getRoute('Appointments',{
                            appointments: [appointment],
                            client: clientData,
                            clients: this.clients,
                            token: this.token,
                            businessname: this.businessname,
                            isShowStaffCheckIn: this.isShowStaffCheckIn,
                            opening_hours:this.opening_hours,  

                            technicians: this.technicians,
                            availablehours: this.availablehours.available_hours,
                            services: this.services,
  
                            blockedTime: this.blockedTime,
                            TechniciansWorkingHour: this.TechniciansWorkingHour,
                            userData: this.userData,
                            blockedTimeYM: this.loadedYM,
                            listcombo: this.listcombo,
                            categories: this.categories,
                            isClientExists: true,
                            clientData: clientData,
                            logo_app: this.logo_app,
                            listCategories: this.listCategories
                        }));
                    }else if(!isValidTime){
                        setTimeout(() => {
                            Alert.alert('Error','Sorry, You are check in late! Please book new appointment');
                        },100)
                    }
                }else{
                    setTimeout(() => {
                        Alert.alert('Error',errMessage);
                    },100)
                }
                
            }
        },100);
        
    }

    closeBarCode = () => {
        this.keepawake();
    }

    render() {
        let logostyle = styles.logofont;
        if(this.businessname.indexOf('&') >= 0){
            logostyle = styles.logofontAngel;
        }

        let styleAvoid = styles.noavoid;
        if(this.isAvoidKeyBoard && Platform.OS === 'ios' ){
            styleAvoid = styles.avoid;
        }

        let styleByKeyBoard = styles.contanerIos;
        let plmargintop = styles.plmargintop;
        if(this.isAvoidKeyBoard && Platform.OS != 'ios'){
            styleByKeyBoard = styles.contanerAndroid;
            plmargintop = styles.plmargintopandroid;
        }

        if (this.state.appIsReady){
            return(
                <View style={{flex:1}}>
                    <TouchableOpacity activeOpacity={1} onPress={this.keepawake} style={{flex:1}}>
                        
                        <View style={{flex:1}}>
                           
                            {this.background_app != ''
                                &&
                                <Image 
                                    source={{uri:this.background_app}}
                        
                                    style={[styles.backgroundFullscreen,{width: width, height: height}]}
                                /> 
                            }

                            {this.background_app == ''
                            &&
                            <View >
                                <Image 
                                    source={require('../../assets/images/nailsbgv2.png')}
                            
                                    style={[styles.backgroundFullscreen,{width: width, height: height}]}
                                /> 
{/*                                 <LinearGradient start={[0, 0]} end={[1, 1.0]} colors={["rgba(241,82,149,0.8)", "rgba(241,82,149,0.05)"]} style={[styles.containerGradient,{width: width, height: height}]}>
                                </LinearGradient> */}
                            </View>
                            }

                            <ScrollView style={{flex:1,position:'relative',zIndex:10}} 
                                contentContainerStyle={styleByKeyBoard}  keyboardShouldPersistTaps='always' ref='scrollview' >
                                <Text style={styles.welcome}>Welcome to</Text> 
                                <View style={{ position:'absolute',top:50,left:50,zIndex:2}}>
                                    {this.logo_app != '' && !this.isAvoidKeyBoard
                                        &&
                                        <ImageResponsive 
                                            source={{uri:this.logo_app}}
                                            height={59}
                                            style={[styles.logo,{width:100,  }]}
                                        />
                                    }

                                    {this.logo_app == '' && !this.isAvoidKeyBoard
                                        &&        
                                        <Text style={[styles.logofontdefault,logostyle]}>{this.props.route.params.businessname}</Text>
                                    }
                                </View>

                                
                                {/*
                                <TouchableOpacity  activeOpacity={1} style={styles.btnHome} onPress={async () =>{await this.checkin();}}>
                                <MaterialCommunityIcons
                                        name={'calendar-clock'}
                                        size={40}
                                        color={'#EF75A4'} style={styles.iconmain}
                                    />
                                    <Text style={styles.btnHomeText}>For Walk-Ins Customer</Text>
                                </TouchableOpacity>
                                <TouchableOpacity  activeOpacity={1} style={styles.btnHome} onPress={async () =>{await this.checkincustomer();}}>
                                    <MaterialCommunityIcons
                                        name={'checkbox-multiple-marked-outline'}
                                        size={40}
                                        color={'#EF75A4'} style={styles.iconmain}
                                    />
                                    <Text style={styles.btnHomeText}>For Appointment Customer</Text>
                                </TouchableOpacity>
                                */}

                                <View style={[styleAvoid,{alignItems:'center'}]}>
                                    <Text style={[styles.lblcheckin, plmargintop,{fontSize:checkinfontsize}]}>Please CHECK-IN here</Text>    
                                    <LinearGradient start={[0, 0]} end={[1, 0]} colors={["rgba(236,111,160, 0.6)", "rgba(249,193,152, 0.6)"]}  style={[styles.txtLoginFormborder]}>
                                        <TextInput
                                            style={[styles.txtSearch]}
                                            placeholder='Enter your Phone or Email' 
                                            placeholderTextColor='#595c68'
                                            onChangeText={(search) => this.changeSearch(search)}
                                            value={this.state.search} 
                                            //value='(229) 516-4525'
                                            //value='(441) 231-2312'
                                            underlineColorAndroid={'transparent'}
                                            onFocus={() => this.onFocus(false)}
                                            onBlur={this.onBlur}
                                            autoCapitalize={'none'}
                                            maxLength={this.state.maxLength}
                                        /> 
                                    </LinearGradient>
 

                                    <TouchableOpacity activeOpacity={1} underlayColor='rgba(255,255,255,0.5)'
                                        style={styles.btnSearch}
                                        onPress={async () => {await this.searchdata()}}>

                                        <LinearGradient                             
                                            start={[0, 0]}
                                            end={[1, 0]}
                                            colors={["#F069A2", "#EEAEA2"]}
                                            style={styles.btnLinear}
                                        >
                                                <Text style={styles.txtsearchbtn}>{this.checkintext}</Text>
                                        </LinearGradient>
                                        
                                    </TouchableOpacity>

                                    {
                                        this.qrCodePermissionStatus && 
                                        <TouchableOpacity activeOpacity={1} underlayColor='rgba(255,255,255,0.5)'
                                            style={[styles.btnSearch,{flexDirection:'row',alignItems:'center',marginTop:40}]}
                                            onPress={async () => {await this.scanQrcode()}}>
                                            <MaterialCommunityIcons
                                                name={'qrcode-scan'}
                                                size={30}
                                                color={'#F069A2'} style={styles.iconmain}
                                            />
                                            <Text style={{fontFamily:'Futura',fontSize:30,backgroundColor:'transparent',color:'#F069A2'}}>Have a QR code?</Text>
                                        </TouchableOpacity> 
                                    }
                                       
                                </View>  

                                
                                
                            </ScrollView>
                           
                        </View>
                        <View style={{alignItems:'center',position:'absolute',zIndex:20,bottom:0,width:width,height:70}}>
                            <Text style={styles.copyright}>Powered by Thepronails.com</Text>
                            <Text style={styles.copyrightPhone}>(302) 543-2014</Text>

                            <TouchableOpacity  activeOpacity={1} style={styles.btnSetting} onPress={this.openSetting}>
                                <Text style={styles.btnSettingText}>Setting</Text>
                            </TouchableOpacity>
                            {this.isShowStaffCheckIn == true && 
                            <TouchableOpacity  activeOpacity={1} style={[styles.btnSettingStaffCheckIn]} onPress={this.openStaffCheckIn}>
                               
                                <Text style={styles.btnSettingText}>Staff Check In</Text>
                            </TouchableOpacity>
                            }    
                            <TouchableOpacity  activeOpacity={1} style={styles.btnRefresh} onPress={this.refresh}>
                                <Text style={styles.btnSettingText}>Refresh</Text>
                            </TouchableOpacity>
                            <TouchableOpacity  activeOpacity={1} style={styles.btnBack} onPress={async () => {await this.openSalon()}}>
                                <Text style={styles.btnSettingText}>Salon</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>        
                    <SpinnerLoader
                        visible={false}
                        textStyle={layout.textLoaderScreen}
                        overlayColor={"rgba(255,255,255,0.9)"}
                        textContent={"Loading..."}
                        color={Colors.spinnerLoaderColor}
                        ref='SpinnerLoader'
                    />

                    <SubmitLoader
                        ref="authenticateLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={"Processing..."}
                        color={Colors.spinnerLoaderColorSubmit}
                    />

                    <AlertLoader
                        ref="CheckInLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={"Welcome James Nguyen"}
                        color={Colors.spinnerLoaderColorSubmit}
                    />

                    <ScreenSaver ref='ScreenSaver' logo_app={this.logo_app} businessname={this.props.route.params.businessname} close={this.closeScreenSaver}/>    
                    <Prompt title='Enter your passcode to check in' type='password' ref='prompt' checkin={true}
                        header='Staff Check In' submittext='Check In' errorMessage='Please enter passcode' 
                        onSubmit={async (value, type) => {await this.checkInWithPassCode(value, type);}} onClose={this.onClosePrompt}/>

                    <Prompt title='Enter your merchant account password to access setting section' type='password' ref='logout' checkin={false}
                        header={this.props.route.params.businessname} submittext='Authorize' errorMessage='Please enter password' 
                        onSubmit={async (value) => {await this.checkAuthorized(value);}} onClose={this.onClosePrompt}/>

                    <Prompt title='Enter your merchant account password to access salon' type='password' ref='salonpage' checkin={false} 
                        header={this.props.route.params.businessname} submittext='Authorize' errorMessage='Please enter password' 
                        onSubmit={async (value) => {await this.checkAuthorized(value, "salonpage");}} onClose={this.onClosePrompt}/>

                    <ModalBarCode ref='ModalBarCode' closeBarCode={this.closeBarCode} scanned={this.scannedBarCode} />

                    <ProfileTechnicianCheckIn title='' type='password' ref='ProfileTechnicianCheckIn' checkin={true}
                        header='Profile Staff' submittext='Check In' errorMessage='Please enter passcode' 
                        onSubmit={async (value, type) => {await this.checkInWithPassCode(value, type);}} onClose={this.onClosePrompt}/>
                    
                </View>
            )  
        }else{
            return(
                <View style={{flex:1}}>
                    <Image 
                            source={require('../../assets/images/nailsbgv2.png')}
                            style={{width:width,height:height}}
                        /> 
                    <LinearGradient start={[0, 0]} end={[1, 1.0]} colors={["rgba(241,82,149,0.8)", "rgba(241,82,149,0.05)"]} style={[styles.containerGradient,{width:width,height:height}]}>
                        <View style={styles.maincontainer}>
                            <View style={styles.contentscontainer}>
                                <Text style={styles.loadingText1}>Thanks for signing in</Text>
                                <View style={styles.underline}></View>
                                <Text style={styles.loadingText}>Now loading data for</Text>
                                <Text style={styles.loadingText}>{this.props.route.params.businessname}</Text>
                                <ActivityIndicator
                                    color={'#fff'}
                                    size={'small'}
                                    style={{marginTop:10}}
                                />
                            </View>
                        </View>
                      {/* <TouchableOpacity  activeOpacity={1} style={styles.btnRefresh} onPress={this.openSetting}>
                                <Text style={styles.btnSettingText}>Setting</Text>
                            </TouchableOpacity>  */}
                            <Prompt title='Enter your merchant account password to access setting section' type='password' ref='logout'
                        header={this.props.route.params.businessname} submittext='Confirm' errorMessage='Please enter password' 
                        onSubmit={async (value) => {await this.checkAuthorized(value);}} onClose={this.onClosePrompt}/>
                                            <SubmitLoader
                        ref="authenticateLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={"Processing..."}
                        color={Colors.spinnerLoaderColorSubmit}
                    />
                    </LinearGradient>   
                    
                </View>
            )    
        }
        
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    welcome:{
        fontSize:18,
        backgroundColor:'transparent',
        color:'#595c68',
        fontFamily: 'futuralight',
        marginBottom:0,
        position:'absolute',top:30,left:50
    },
    copyright:{
        fontSize:18,
        backgroundColor:'transparent',
        color:'#595c68',
        fontFamily: 'Futura',
        position:'absolute',
        bottom:40,
        zIndex:2   
    },
    copyrightPhone:{
        fontSize:18,
        backgroundColor:'transparent',
        color:'#595c68',
        fontFamily: 'Futura',
        position:'absolute',
        bottom:10,
        zIndex:2  
    },
    maincontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentscontainer:{
        backgroundColor:'rgba(255,255,255,0.2)',
        paddingTop:20,
        paddingBottom:20,
        paddingLeft:20,
        paddingRight:20,
        borderRadius:10,
        alignItems: 'center',
    },
    containerGradient: {
        flex: 1,
		opacity:0.8,
		position:'absolute',
		top:0,
		bottom:0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:1
    },
    syncData:{
        backgroundColor:'rgba(0,0,0,0.3)',
        width:220,
        height:200
    },
    loadingText:{
        color:'#fff',
        fontSize:20,
    },
    loadingText1:{
        color:'#fff',
        fontSize:22,
        marginTop:-5
    },
    underline:{
        width:100,
        height:1,
        backgroundColor:'#fff',
        marginTop:10,
        marginBottom:10
    },
    backgroundFullscreen:{
        position:'absolute',
        zIndex:1
    },
    logofontdefault:{
        backgroundColor:'transparent',
        color:'#fff',
        fontSize:70,
        marginBottom:30,
        textAlign:'center',
        paddingLeft:20,
        paddingRight:20
    },
    logofont:{
        fontFamily: 'NeotericcRegular'
    },
    logofontAngel:{
        fontFamily: 'angel'
    },
    btnHome:{
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:20,
        paddingRight:10,
        borderRadius:40,
        height:70,
        alignItems: 'center',
        width:380,
        marginBottom:40,
        flexDirection:'row'
    },
    iconmain:{
        marginRight:10,
        //marginTop:4,
        backgroundColor:'transparent'
    },
    btnHomeText:{
        color:'#EF75A4',
        fontSize:24,
        fontFamily:'Futura'
    },
    btnSetting:{
        position:'absolute',
        bottom:20,
        right:30,
        zIndex:2 
    },
    btnSettingText:{
        fontSize:18,
        backgroundColor:'transparent',
        color:'#595c68',
        fontFamily: 'Futura',
        zIndex:2
    },

    btnSettingStaffCheckIn:{
        position:'absolute',
        bottom:20,
        left:140,
        zIndex:2,
        
        borderWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        borderRightWidth: 0,
        borderColor: '#595c68',
        paddingLeft:30
    },
    btnRefresh:{
        position:'absolute',
        bottom:20,
        left:90,
        zIndex:2
    },
    btnBack:{
        position:'absolute',
        bottom:20,
        left:30,
        zIndex:2
    },
    logo:{
        marginBottom:30
    },
    lblcheckin:{
        color:'#595c68',
        backgroundColor:'transparent',
        marginBottom:40,
        fontFamily:'Futura',

    },
    plmargintop:{
        marginTop:140
    },
    plmargintopandroid:{
        marginTop:60
    },
    txtSearchBlur:{
        borderRadius: 50,
    },
    txtSearch: {
        height: 60,
        backgroundColor: 'rgba(255,255,255,0.5)',
        borderRadius: 50,
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 10,
        paddingLeft: 20,
        color: '#595c68',
        width:400,
        fontSize:20, 
        fontFamily:'Futura'
    },
    btnSearch: {
        //backgroundColor:'rgba(255,255,255,0.5)',
//height:70,
        //borderRadius:50,
        alignItems: 'center',
        justifyContent: 'center',
        //width:400,
        marginTop:20
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
   
        borderRadius:50,
        padding:5,
        height:64,
        width:400,
        
    },
    txtsearchbtn: {
        fontSize:34,
        fontFamily:'Futura',
        color:'#fff',
        backgroundColor:'transparent',
    },
    avoid:{
        position:'absolute',
        bottom:50
    },
    contanerIos:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        position:'relative',
        zIndex:10
    },
    contanerAndroid:{
        flex:1,
        alignItems:'center',
        position:'relative',
        zIndex:10,
        paddingTop:10
    },
    txtLoginFormborder: {
        borderRadius: 50,
        height: 64,
        padding:2,
    },
});
