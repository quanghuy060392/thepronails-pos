import React from "react";
import {
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Alert,
    Platform
} from "react-native";
import layout from "../assets/styles/layout";
import Colors from "../constants/Colors";
import SpinnerLoader from "../helpers/spinner";
import {
    fetchTechniciansData,
    fetchAppointments,
    fetchClientsData,
    fetchBusinesshours,
    fetchServices,
    fetchListCombo,
    fetchTurns
} from "../helpers/fetchdata";
import {
    isLogged,
    jwtToken,
    getUserData,
    getDeviceId
} from "../helpers/authenticate";
import moment from "moment";
import { StackNavigation } from "@expo/ex-navigation";
import NavigationBarBackground from "../components/navigationBarBG";
import CarlendarTitleNav from "../components/CalendarTitleNav";
import collect from "collect.js";
import ScrollableTabView from "react-native-scrollable-tab-view";
import DefaultTabBar from "../components/DefaultTabBar";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import AppointmentListByDay from "../components/AppointmentList";
import AppointmentListByWeek from "../components/AppointmentList";
//import CalendarStrip from "../components/CalendarStrip";
import CalendarStrip from "react-native-calendar-strip";
import AddAppointment from "../components/AddAppointment";
import ViewAppointment from "../components/ViewAppointment";
import CalendarStripHeader from "../components/CalendarStripHeader";
import ModalCalendar from "../components/ModalCalendar";
import ModalNotification from "../components/ModalNotification";
import { getListAppointmentStatus, getListAppointmentStatusForCreate } from "../helpers/Utils";
import AppointmentSelectDay from "../components/AppointmentSelectDay";
import Alerts from "../constants/Alerts";
import { Notifications } from "expo";
import { getAppointmentDetails } from "../api/getAppointmentDetails";
import { formatPhone, get_time_zone, getUSState2Digit } from "../helpers/Utils";
import { getLanguageName, getLanguage, getTextByKey } from "../helpers/language";

export default class CalendarScreen extends React.Component {
    static route = {
        navigationBar: {
            visible: false,
            elevation: 0,
            renderBackground: () => {
                return <NavigationBarBackground />;
            },
            renderTitle: route => {
                return (
                    <CarlendarTitleNav
                        onSelectTechnician={route.params.onSelectTechnician}
                        onRefreshAppointment={async () => { await route.params.onRefreshAppointment() }}
                        onPressNotificationAppointment={async () => { await route.params.onPressNotificationAppointment() }}
                        technicianList={route.params.technicianList}
                        badge={route.params.badge}
                        language={route.params.language}
                    />
                );
            },
            renderLeft: () => {
                return false;
            }
        }
    };
    /*
    datesWhitelist = [
        {
            start: moment().startOf("week").add(0, "days"),
            end: moment().startOf("week").add(6, "days") // total 4 days enabled
        }
    ];*/
    languageKey = typeof(this.props.route.params.language) != 'undefined' ? this.props.route.params.language : 'en-US';
    token = "";
    deviceid = 0;
    isLoggedIn = false;
    currentYM = moment.utc().format("YMM");
    loadedMonths = {};
    today = moment();
    byDayDate = moment();
    technicians = [];
    techniciansWithSearch = [];
    clients = [];
    availablehours = [];
    appointments = [];
    appointmentsByDay = [];
    appointmentsByWeek = [];
    listAppointmentStatus = getListAppointmentStatus();
    listAppointmentStatusForCreate = getListAppointmentStatusForCreate();
    services = [];
    combos = [];
    selectServices = {};
    filter = { technician: 0 };
    startDateWeek = moment().startOf("week").add(0, "days");
    endDateWeek = moment().startOf("week").add(6, "days");
    isTabDay = true;
    newAppointmentTitle = getTextByKey(this.languageKey,'newappointmenttitle');
    addAppointmentTitle = this.newAppointmentTitle;
    isToday = true;
    byDayText = moment().format("ll");
    state = {
        appIsReady: false
    };
    focusedDay = moment();
    userData = {};
    isLoadedAppointment = false;
    appointmentPushDataId = 0;
    categories = [];
    turns = [];

    liststatus = ['pending','confirmed','completed','canceled'];
    stateData = '';
    timezone = '';

    
    componentWillUnmount() {
        this._notificationSubscription &&
            this._notificationSubscription.remove();
    }
    /*
    componentDidMount() {
        this._notificationSubscription = Notifications.addListener(
            this._handleNotification
        );
    }*/

    async componentWillMount() {
        //Alert.alert('error',JSON.stringify(this.props.route.params));
        //let currentNavigatorUID = this.props.navigation.navigationState.currentNavigatorUID;
        //let navigatior = this.props.navigation.navigationState.navigators[currentNavigatorUID];
        //let rootNavigator = this.props.navigation.getNavigator("root");
        //console.log( navigatior.routes[0].routeName);
        //Alert.alert("error", "ok");
        //console.log(this.props.route.params);

        this.isLoggedIn = await isLogged();
        if (this.isLoggedIn) {
            this.token = await jwtToken();
            this.userData = await getUserData();
            this.deviceid = await getDeviceId();

            this.languageKey = await getLanguage();
            /*
            this.stateData = getUSState2Digit(this.userData.state);
            this.timezone = get_time_zone('US',this.stateData);
            this.startDateWeek = moment().tz(this.timezone).startOf('week');
            this.focusedDay = moment().tz(this.timezone);*/

            //console.log(this.userData);
            //fetch technicians
            let TechniciansData = [];
            let TechniciansDataNav = [];

            //TechniciansData = await fetchTechniciansData(this.token);

            if (this.userData.role == 4) {
                TechniciansData = await fetchTechniciansData(this.token);
                TechniciansDataNav = [...TechniciansData];
                this.technicians = collect(TechniciansData);
                let allTechnician = {
                    id: 0,
                    fullname: getTextByKey(this.languageKey,'anytech'),
                    picture: ''
                };
                TechniciansData.splice(0, 0, allTechnician);

                this.techniciansWithSearch = TechniciansData;

                let allTechnicianNav = {
                    id: 0,
                    fullname: getTextByKey(this.languageKey,'anytechnav'),
                    picture: ''
                };
                TechniciansDataNav.splice(0, 0, allTechnicianNav);
            } else {
                this.filter.technician = this.userData.id;
                let techDefault = {};
                techDefault.id = this.userData.id;
                techDefault.firstname = this.userData.firstname;
                techDefault.lastname = this.userData.lastname;
                techDefault.fullname = this.userData.fullname;
                TechniciansData.push(techDefault);
                this.technicians = collect(TechniciansData);
                this.techniciansWithSearch = TechniciansData;

                TechniciansDataNav = [...TechniciansData];
            }

            this.props.route.params.onSelectTechnician = this.onSelectTechnician;
            this.props.route.params.onRefreshAppointment = this.onRefreshAppointment;
            this.props.route.params.onPressNotificationAppointment = this.onPressNotificationAppointment;
            this.props.route.params.badge = 0;
            this.props.route.config.navigationBar.visible = true;
            this.props.navigator.updateCurrentRouteParams({
                technicianList: TechniciansDataNav
            });

            
            /*
            //load turn
            if(this.userData.isManageTurn){
                this.turns = await fetchTurns(this.token);
                console.log(this.turns);
            }*/
            
            //fetch appointment
            var appointmentsData = await fetchAppointments(
                this.token,
                this.currentYM
            );
            var appointments = appointmentsData.appointments;
            
            let _this = this;
            appointments.forEach(function(appointment){
                appointment.status = _this.liststatus[appointment.status];
                var clientdata = appointmentsData.clients.filter(function (item) {
                    return item.schedulerid == appointment.id;
                });
                if(clientdata.length){
                    let x = clientdata[0];
                    appointment.client = String.prototype.trim.call(x.client_full_name);

                    if(typeof x.phone != 'undefined' && x.phone != '' && x.phone != null){
                        if(typeof x.client_full_name != 'undefined' && String.prototype.trim.call(x.client_full_name) != '' 
                            && x.client_full_name != null){
                            appointment.client += ' - ';
                        }
                        let displayPhone = formatPhone(x.phone);
                        if(_this.userData.role == 9){
                            let displayphoneSplit = displayPhone.split('-');
                            if(displayphoneSplit.length > 1){
                                displayPhone = '(xxx) xxx-' + displayphoneSplit[1];
                            }       
                        }
                        appointment.client += displayPhone;
                    }else if(typeof x.email != 'undefined' && x.email != '' && x.email != null){
                        if(typeof x.client_full_name != 'undefined' && String.prototype.trim.call(x.client_full_name) != '' 
                            && x.client_full_name != null){
                            appointment.client += ' - ';
                        }
                        appointment.client += x.email;
                    }
                }
    
                var technician_datalist = appointmentsData.resources.filter(function (item) {
                    return item.schedulerid == appointment.id;
                });

                if(technician_datalist.length){
                    let technician_datagroups = collect(technician_datalist).groupBy('technicianId');
                    appointment.resourceIds = technician_datagroups.keys().toArray();
                }

                appointment.services = appointmentsData.services.filter(function (item) {
                    return item.schedulerid == appointment.id;
                });
                
                
            });
           
         

            var collectAppointment = collect(appointments);
            this.loadedMonths[this.currentYM] = collectAppointment;
            
            if(Platform.OS === 'ios') {
                this.appointments = collect([...this.loadedMonths[this.currentYM]]);
             } else {
                this.customercheckins = this.loadedMonths[this.currentYM];
            } 
            this.isLoadedAppointment = true;

            //fetch clients
            var clients = await fetchClientsData(this.token);
            this.clients = clients;

            //fetch businesshours
            var businesshours = await fetchBusinesshours(this.token);
            this.availablehours = businesshours.available_hours;
          /*  Object.keys(
                this.availablehours
            ).map((x, i) => {
                this.availablehours[x];
                if(typeof this.availablehours[x] != undefined && this.availablehours[x].length > 0){

                        let lasttimestr =  this.availablehours[x].pop(); 
                        let lasttime = parseInt(lasttimestr.split(":")[0]);
                        switch (lasttimestr.split(":")[1]){
                            case "00":
                            this.availablehours[x].push(parseInt(lasttime) + ":15");
                            this.availablehours[x].push(parseInt(lasttime) + ":30");
                            this.availablehours[x].push(parseInt(lasttime) + ":45");
                            break;
                            case "15":
                            this.availablehours[x].push(parseInt(lasttime) + ":30");
                            this.availablehours[x].push(parseInt(lasttime) + ":45");
                            break;
                            case "30":
                            this.availablehours[x].push(parseInt(lasttime) + ":45");
                            break;
                        }
                        for(let i=0;i<2;i++){
                          lasttime += 1;
                          if(lasttime < 22){
                            this.availablehours[x].push(parseInt(lasttime) + ":00");
                            this.availablehours[x].push(parseInt(lasttime) + ":15");
                            this.availablehours[x].push(parseInt(lasttime) + ":30");
                            this.availablehours[x].push(parseInt(lasttime) + ":45");
                          }
                        }
                      
                }

            })*/
            //fetch combos
            this.combos = await fetchListCombo(this.token);

            //fetch services
            this.services = await fetchServices(this.token);
            this.services.forEach(function(item){
                item.isCombo = false;
                item.id = 'service_' + item.id;      
                
                let category = _this.categories.filter(function(itemCategory){
                    if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                        return itemCategory == item.category_customname;
                    }else{
                        return itemCategory == item.category_name;
                    }
                    
                });
                if(!category.length){
                    if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                        _this.categories.push(item.category_customname);    
                    }else{
                        _this.categories.push(item.category_name);
                    }
                    
                }
            });

            this.categories = this.categories.sort(function (a, b) {
                if (a < b) return -1;
                else if (a > b) return 1;
                return 0;
            });
            
            this.categories.push('Combo');

            /*
            this.categories.forEach(function(item){
                console.log(item);        
            })*/

            /*
                let sectionAppointment = {};
                sectionAppointment.key = item.id;
                sectionAppointment.data = this.getAppointmentDataByTechnicianId(item.id,appointmentsCurrentSelectedDay);
                this.appointmentsByDay.push(sectionAppointment);
            */
            
            
            this.combos.forEach(function(item){
                let serviceData = {
                    duration: 0,
                    happyhour: false,
                    id: 'combo_' + item.id,
                    isCombo: true,
                    price: item.price,
                    rewardpoint: item.rewardpoint,
                    service_name: item.comboname
                }   
                _this.services.push(serviceData);    
            });
          
            let firstService = {
                id: 0,
                service_name: getTextByKey(this.languageKey,'selectserviceappointment'),
                price: 0,
                technicianId: 0,
                technicianName: getTextByKey(this.languageKey,'selecttechnicianappointment'),
                isCombo: false,
                rewardpoint: 0
            };

            this.selectServices["service_0"] = firstService;
            
            this.setState({ appIsReady: true });
            this.displayAppointmentByDay(true, false);
            if (typeof this.props.route.params.showTabWeek != "undefined") {
                await _this.showNotificationAppointment(
                    this.props.route.params.notificationData.data,
                    this
                );
            }
            
        } else {
            this.props.navigator.push("login");
        }
    }

    onRefreshAppointment = async () => {
        this.fetchMonthData();
        
    }

    onPressNotificationAppointment = async () => {
        this.refs.modalNotification.show();
        this.props.navigator.updateCurrentRouteParams({
            badge: 0
        });
    }

    onPressNotificationItem = (id) => {
        this._onPressAppointment(id);
    }

    async showNotificationAppointment(data, _this) {
        let isLoaded = false;
        for (let loadedApptByYMkey in _this.loadedMonths) {
            let loadedList = _this.loadedMonths[loadedApptByYMkey];
            if (loadedList.where("id", data.id).count() > 0) {
                isLoaded = true;
                break;
            }
        }

        if (!isLoaded) {
            let AppointmentDetails = await getAppointmentDetails(
                _this.token,
                data.id
            );
            let appointmentDataFormat = {};
            appointmentDataFormat.id = AppointmentDetails.id;
            appointmentDataFormat.status = AppointmentDetails.status;
            appointmentDataFormat.booking_code =
                AppointmentDetails.booking_code;
            appointmentDataFormat.total = AppointmentDetails.total;
            appointmentDataFormat.start_time = AppointmentDetails.start_time;
            appointmentDataFormat.end_time = AppointmentDetails.end_time;
            appointmentDataFormat.technician_id =
                AppointmentDetails.technician_id;
            appointmentDataFormat.technician_fullname =
                AppointmentDetails.technician_fullname;
            appointmentDataFormat.client = AppointmentDetails.client.fullname;
            let YM = moment(AppointmentDetails.start_time).format("YMM");
            if (typeof _this.loadedMonths[YM] != "undefined") {
                _this.loadedMonths[YM].push(appointmentDataFormat);
            }
        }
        _this.byDayDate = moment(data.starttime);
        _this.focusedDay = moment(data.starttime);
        _this.startDateWeek = this.focusedDay.clone().startOf("week");
        _this.endDateWeek = this.focusedDay.clone().endOf("week");

        setTimeout(function() {
            _this.refs.tabAppointment.goToPage(1);
        }, 0);

        //_this.refs.tabAppointment.goToPage(1);
        //_this.setState({ appIsReady: true });
        /*
        setTimeout(function() {
            _this.refs.tabAppointment.goToPage(1);
        }, 1000);*/
    }

    //async _handleNotification({ origin, data }) {
        /*
    async _handleNotification({ origin, data }, _this) {
        
        switch (data.case) {
            case "new_appointment1":
                //Alert.alert("Error", 'yes');
                //Alert.alert('error',this.token);
                //this.userData = await getUserData();
                //this.token = await jwtToken();
                //Alert.alert("Error", this.token);
                if (
                    (_this.userData.role == 4 &&
                        _this.userData.id == data.serviceprovider_id) ||
                    (_this.userData.role == 9 &&
                        _this.userData.serviceprovider_id ==
                            data.serviceprovider_id &&
                        _this.userData.id == data.technician_id)
                ) {
                    if (origin == "selected") {
                        
                        if (this.isLoadedAppointment) {
                            this.showNotificationAppointment(data.id);
                        } else {
                            this.appointmentPushDataId = data.id;
                        }
                        //Alert.alert('error',JSON.stringify(data));
                        //this.setState({ appIsReady: true });
                        //await _this.showNotificationAppointment(data, _this);
                    } else {
                        
                        let newAppointment = await getAppointmentDetails(
                            this.token,
                            data.id
                        );
                        
                        let appointmentDataFormat = {};
                        appointmentDataFormat.id = newAppointment.id + 1;
                        appointmentDataFormat.status = newAppointment.status;
                        appointmentDataFormat.booking_code =
                            newAppointment.booking_code;
                        appointmentDataFormat.total = newAppointment.total;
                        appointmentDataFormat.start_time =
                            '2017-07-25 11:00:00';
                        appointmentDataFormat.end_time =
                            newAppointment.end_time;
                        appointmentDataFormat.technician_id =
                            newAppointment.technician_id;
                        appointmentDataFormat.technician_fullname =
                            newAppointment.technician_fullname;
                        appointmentDataFormat.client =
                            newAppointment.client.fullname;
                        this.appointments.push(appointmentDataFormat);
                        
                        if (
                            this.isTabDay &&
                            this.byDayDate.isSame(
                                moment(appointmentDataFormat.start_time),
                                "day"
                            )
                        ) {
                            this.displayAppointmentByDay(true);
                        }else{
                            
                            this.focusedDay = moment(
                                '2017-07-25 11:00:00'
                            );
                            
                            let _this = this;
                            setTimeout(function() {
                                _this.refs.tabAppointment.goToPage(1);
                                _this.displayAppointmentByWeek(true);
                                _this.refs.CalendarStripByWeek.setSelectedDate(_this.focusedDay);
                                
                            }, 100);
                            
                        }
                    }
                } else if (origin == "selected") {
                    this.setState({ appIsReady: true });
                    this.displayAppointmentByDay(true);
                }

                break;
        }
}*/
    getAppointmentDataByTechnicianId = (techId,appointmentsCurrentSelectedDay) => {
        let sectionAppointment = {};
        sectionAppointment.key = techId;

        let apptsByTech = appointmentsCurrentSelectedDay.filter(
            function(itemAppt) {
                let isExists = itemAppt.resourceIds.filter(function(itemTech){
                    return itemTech == techId;           
                })
                return isExists.length;
            }
        );
        if(apptsByTech.count()){
            return apptsByTech.toArray();        
        }else{
            return [{ id: 0 }];
        }
    }

    displayAppointmentByDay = (isRefresh = true, isRefreshDay = true) => {
        this.appointmentsByDay = [];
        var startTimeCurrentSelectedDate =
            this.byDayDate.format("Y-MM-DD") + " 00:00:00";
        var endTimeCurrentSelectedDate =
            this.byDayDate.format("Y-MM-DD") + " 23:59:59";

        let YM = this.byDayDate.format("YMM");
        if(Platform.OS === 'ios') {
            this.appointments = collect([...this.loadedMonths[YM]]);
         } else {
            this.appointments = this.loadedMonths[YM];
        } 
        var appointmentsCurrentSelectedDay = this.appointments
            .where("start_time", ">", startTimeCurrentSelectedDate)
            .where("start_time", "<", endTimeCurrentSelectedDate);


        appointmentsCurrentSelectedDay = appointmentsCurrentSelectedDay.sortBy(
            "start_time"
        );
        //console.log(appointmentsCurrentSelectedDay);
        let apptTechExists = [];   
        let _this = this;
        //console.log(this.filter.technician);
        if (this.filter.technician > 0){
            let sectionAppointment = {};
            sectionAppointment.key = this.filter.technician;
            sectionAppointment.data =this.getAppointmentDataByTechnicianId(this.filter.technician,appointmentsCurrentSelectedDay);
            this.appointmentsByDay.push(sectionAppointment);
        }else{
            this.technicians.map((item,i) => {
                //ignore any technician
                /*
                if(item.id > 0){
                    
                }*/
                let sectionAppointment = {};
                sectionAppointment.key = item.id;
                sectionAppointment.data = this.getAppointmentDataByTechnicianId(item.id,appointmentsCurrentSelectedDay);
                this.appointmentsByDay.push(sectionAppointment);
                
            })
        }
 
        if (isRefresh) {
            if (typeof this.refs.AppointmentListByDay != "undefined") {
                
                this.refs.AppointmentListByDay.setState({
                    visible: true,
                    byday: true,
                    data: this.appointmentsByDay
                });
            }

            //this.setState({ apptByDay: true, apptByWeek: false });
        }
        if (isRefreshDay) {
            if (this.byDayDate.isSame(this.today, "day")) {
                this.refs.AppointmentSelectDay.setState({
                    byDayText: this.byDayDate.format("ll"),
                    isToday: true
                });
            } else {
                this.refs.AppointmentSelectDay.setState({
                    byDayText: this.byDayDate.format("ll"),
                    isToday: false
                });
            }
        }

        //this.props.appointmentsByDay = this.appointmentsByDay;
        //console.log(this.props.appointmentsByDay);
    };

    async displayAppointmentByWeek(isRefresh = true, isRefreshStrip = true) {
        let _this = this;
        this.appointmentsByWeek = [];
        var startTimeCurrentSelectedDate =
            this.startDateWeek.format("Y-MM-DD") + " 00:00:00";
        var endTimeCurrentSelectedDate =
            this.endDateWeek.format("Y-MM-DD") + " 23:59:59";
        let YMStart = this.startDateWeek.format("YMM");
        let YMEnd = this.endDateWeek.format("YMM");
        if (YMStart != YMEnd) {
            this.refs.tabappointmentloader.setState({ visible: true });
            if (typeof this.loadedMonths[YMStart] == "undefined") {
                var appointmentsData = await fetchAppointments(this.token, YMStart);
                var appointments = this.fillData(appointmentsData);
                this.loadedMonths[YMStart] = collect(appointments);
            }

            if (typeof this.loadedMonths[YMEnd] == "undefined") {
                var appointmentsData = await fetchAppointments(this.token, YMEnd);
                var appointments = this.fillData(appointmentsData);
                this.loadedMonths[YMEnd] = collect(appointments);
            }
            //this.appointments = collect([...this.loadedMonths[YMStart]]);
            if(Platform.OS === 'ios') {
                this.appointments = collect([...this.loadedMonths[YMStart]]);
             } else {
                this.appointments = this.loadedMonths[YMStart];
            } 
            this.loadedMonths[YMEnd].each(function(item) {
                _this.appointments.push(item);
            });
            this.refs.tabappointmentloader.setState({ visible: false });
            //this.appointments = this.loadedMonths[YMStart].combine(this.loadedMonths[YMEnd]).all();
            //console.log(this.appointments);
            //console.log('render 1');
        } else {
            //this.appointments = collect([...this.loadedMonths[YMStart]])
            if(Platform.OS === 'ios') {
                this.appointments = collect([...this.loadedMonths[YMStart]]);
             } else {
                this.appointments = this.loadedMonths[YMStart];
            } 
        }
        //console.log(this.appointments);
        var appointmentsCurrentSelectedWeek = this.appointments
            .where("start_time", ">", startTimeCurrentSelectedDate)
            .where("start_time", "<", endTimeCurrentSelectedDate);
        //console.log(this.startDateWeek);
        if (this.filter.technician > 0) {
            var currentSelectedTechnician = this.filter.technician;
            appointmentsCurrentSelectedWeek = appointmentsCurrentSelectedWeek.filter(
                function(item) {
                    let isExists = item.resourceIds.filter(function(itemTech){
                        return itemTech == currentSelectedTechnician;           
                    })
                    return isExists.length;
                    //return item.technician_id == currentSelectedTechnician;
                }
            );
            //console.log(appointmentsCurrentSelectedDay);
        }
        appointmentsCurrentSelectedWeek = appointmentsCurrentSelectedWeek.sortBy(
            "start_time"
        );
        appointmentsCurrentSelectedWeek = appointmentsCurrentSelectedWeek.groupBy(
            function(item, key) {
                return item.start_time.substring(0, 10);
            }
        );

        var appointmentsByWeekData = appointmentsCurrentSelectedWeek.all();
        for (let i = 0; i < 7; i++) {
            let cloneStart = this.startDateWeek.clone();
            let startKey = cloneStart.add(i, "days").format("Y-MM-DD");

            let sectionAppointment = {};
            sectionAppointment.key = startKey;
            if (typeof appointmentsByWeekData[startKey] != "undefined") {
                sectionAppointment.data = appointmentsByWeekData[startKey];
                //console.log(startKey);
                //console.log(appointmentsByWeekData[startKey]);
            } else {
                //sectionAppointment.data = [];
                sectionAppointment.data = [{ id: 0 }];
            }
            this.appointmentsByWeek.push(sectionAppointment);
        }
        /*
        for (var key in appointmentsByWeekData) {
            
            sectionAppointment.data = appointmentsByWeekData[key];
            this.appointmentsByWeek.push(sectionAppointment);
        }*/
        //console.log(this.appointmentsByWeek);
        //console.log(this.appointmentsByWeek);
        if (isRefreshStrip) {
            setTimeout(function() {
                //_this.refs.CalendarStripByWeek.setSelectedDate(_this.focusedDay);
                _this.refs.CalendarStripByWeek.setState({
                    selectedDate: _this.focusedDay.clone(),
                    startingDate: _this.focusedDay.clone().startOf("week")
                });
            }, 0);
        }
        if (isRefresh) {
            if (typeof this.refs.AppointmentListByWeek != "undefined") {
                let sectionIndex = this.focusedDay.day();
                //console.log(sectionIndex);
                this.refs.AppointmentListByWeek.setState({
                    visible: true,
                    byday: false,
                    data: this.appointmentsByWeek,
                    sectionIndex: sectionIndex,
                    itemIndex: 0
                });

                //this.refs.AppointmentListByWeek.scrollTo();
            }
            //this.setState({ apptByDay: false, apptByWeek: true });
        }
    }

    fillData = (appointmentsData) => {
        let _this = this;
        let appointments = appointmentsData.appointments;
        appointments.forEach(function(appointment){
            appointment.status = _this.liststatus[appointment.status];
            var clientdata = appointmentsData.clients.filter(function (item) {
                return item.schedulerid == appointment.id;
            });
            if(clientdata.length){
                let x = clientdata[0];
                appointment.client = String.prototype.trim.call(x.client_full_name);

                if(typeof x.phone != 'undefined' && x.phone != '' && x.phone != null){
                    if(typeof x.client_full_name != 'undefined' && String.prototype.trim.call(x.client_full_name) != '' 
                        && x.client_full_name != null){
                        appointment.client += ' - ';
                    }
                    let displayPhone = formatPhone(x.phone);
                    if(_this.userData.role == 9){
                        let displayphoneSplit = displayPhone.split('-');
                        if(displayphoneSplit.length > 1){
                            displayPhone = '(xxx) xxx-' + displayphoneSplit[1];
                        }       
                    }
                    appointment.client += displayPhone;
                }else if(typeof x.email != 'undefined' && x.email != '' && x.email != null){
                    if(typeof x.client_full_name != 'undefined' && String.prototype.trim.call(x.client_full_name) != '' 
                        && x.client_full_name != null){
                        appointment.client += ' - ';
                    }
                    appointment.client += x.email;
                }
            }

            var technician_datalist = appointmentsData.resources.filter(function (item) {
                return item.schedulerid == appointment.id;
            });

            if(technician_datalist.length){
                let technician_datagroups = collect(technician_datalist).groupBy('technicianId');
                appointment.resourceIds = technician_datagroups.keys().toArray();
            }

            appointment.services = appointmentsData.services.filter(function (item) {
                return item.schedulerid == appointment.id;
            });
            
            
        });
        return appointments;
    }

    onSelectTechnician = id => {
        this.filter.technician = id;
        if (this.isTabDay) {
            this.displayAppointmentByDay();
        }else{
            this.displayAppointmentByWeek();
        }
    };

    async fetchMonthData() {
        this.refs.tabappointmentloader.setState({ visible: true });
        if (this.isTabDay) {
            if (this.byDayDate.isSame(this.today, "day")) {
                this.refs.AppointmentSelectDay.setState({
                    byDayText: this.byDayDate.format("ll"),
                    isToday: true
                });
            } else {
                this.refs.AppointmentSelectDay.setState({
                    byDayText: this.byDayDate.format("ll"),
                    isToday: false
                });
            }
        }

        var appointmentsData = await fetchAppointments(this.token, this.currentYM);
        var appointments = this.fillData(appointmentsData);

        var collectAppointment = collect(appointments);
        this.loadedMonths[this.currentYM] = collectAppointment;
        
        if(Platform.OS === 'ios') {
            this.appointments = collect([...this.loadedMonths[this.currentYM]]);
         } else {
            this.customercheckins = this.loadedMonths[this.currentYM];
        } 
        if (this.isTabDay) {
            this.displayAppointmentByDay(true, false);
        } else this.displayAppointmentByWeek(true);
        this.refs.tabappointmentloader.setState({ visible: false });
    }

    changeDate = option => {
        if (option == "prev") {
            this.byDayDate.subtract(1, "day");
            this.focusedDay.subtract(1, "day");
        } else {
            this.byDayDate.add(1, "day");
            this.focusedDay.add(1, "day");
        }
        let YM = this.byDayDate.format("YMM");
        if (typeof this.loadedMonths[YM] == "undefined") {
            this.currentYM = YM;
            this.fetchMonthData();
        } else {
            this.currentYM = YM;
            
            if(Platform.OS === 'ios') {
                this.appointments = collect([...this.loadedMonths[this.currentYM]]);
             } else {
                this.customercheckins = this.loadedMonths[this.currentYM];
            } 
            this.displayAppointmentByDay(true);
        }

        /*
        if (typeof this.refs.CalendarStripByWeek != "undefined") {
            //this.refs.CalendarStripByWeek.setState({selectedDate:this.focusedDay});
            //this.refs.CalendarStripByWeek.setSelectedDate(this.focusedDay);
        }*/
        //console.log(this.focusedDay);
        //console.log(this.focusedDay.clone().startOf("week"));
        this.startDateWeek = this.focusedDay.clone().startOf("week");
        this.endDateWeek = this.focusedDay.clone().endOf("week");
    };

    changeWeek = week => {
        this.startDateWeek = week.clone();
        this.endDateWeek = week.clone().add(6, "days");
        this.focusedDay = this.startDateWeek
            .clone()
            .add(this.focusedDay.day(), "days");
        this.byDayDate = this.focusedDay.clone();
        //this.refs.CalendarStripByWeek.setSelectedDate(this.focusedDay);
        this.displayAppointmentByWeek(true);
        //startDateWeek
        //this.refs.CalendarStripByWeek.setSelectedDate(date);
        //this.focusedDay = date;
    };

    _today = () => {
        if (!this.byDayDate.isSame(this.today, "day")) {
            this.byDayDate = moment();
            this.focusedDay = moment();
            this.startDateWeek = this.focusedDay.clone().startOf("week");
            this.endDateWeek = this.focusedDay.clone().endOf("week");
            /*
            if (typeof this.refs.CalendarStripByWeek != "undefined") {
                this.refs.CalendarStripByWeek.setSelectedDate(moment());
            }*/
            this.displayAppointmentByDay(true);
        }
    };

    toDayWeek = () => {
        if (!this.focusedDay.isSame(this.today, "day")) {
            this.byDayDate = moment();
            this.focusedDay = moment();
            this.startDateWeek = this.focusedDay.clone().startOf("week");
            this.endDateWeek = this.focusedDay.clone().endOf("week");
            /*
            if (typeof this.refs.CalendarStripByWeek != "undefined") {
                this.refs.CalendarStripByWeek.setSelectedDate(moment());
            }*/
            this.displayAppointmentByWeek(true);
        }
    };

    changeTab = index => {
        if (index.i == 0) {
            if (!this.isTabDay) {
                this.isTabDay = true;
                this.displayAppointmentByDay(true);
            }
        } else {
            if (this.isTabDay) {
                this.isTabDay = false;
            }

            this.displayAppointmentByWeek(true);
        }
    };

    addAppointment = async key => {
        this.addAppointmentTitle = this.newAppointmentTitle;
        this.refs["addAppointment"].isShowLoaderAppointmentDetails = false;
        this.refs["addAppointment"].clearForm();
        this.refs["addAppointment"].title = this.addAppointmentTitle;
        this.refs["addAppointment"].selectedStatus = 'confirmed';
        this.refs["addAppointment"].statusName = 'confirmed';
        if (this.isTabDay) {
            this.refs["addAppointment"].selectedTechnician = key;
            let selectedTech = this.technicians.first(function(item) {
                return item.id == key;
            });
            this.refs["addAppointment"].technicianName = selectedTech.fullname;
        } else {
            this.refs["addAppointment"].selectedDate = moment(key);
            let dayname = moment(key).format("dddd").toLowerCase();
            let availablehours = this.availablehours[dayname];
            if (availablehours.length) {
                this.refs["addAppointment"].selectedHour = availablehours[0];
                this.refs["addAppointment"].appointmentDate = moment(
                    key
                ).format("DD-MM-Y");
                this.refs[
                    "addAppointment"
                ].appointmentHour = this.convertTo24Hour(availablehours[0]);
                this.refs["addAppointment"].selectedTime =
                    moment(key).format("MM-DD-Y") + " " + availablehours[0];
            }
        }
        this.refs["addAppointment"].clients = await fetchClientsData(this.token);

        //load turn
        this.refs["addAppointment"].turns = [];
        if(this.userData.isManageTurn){
            this.refs["addAppointment"].showLoaderAppointmentDetail();
            this.refs["addAppointment"].turns = await fetchTurns(this.token);
            this.refs["addAppointment"].isShowLoaderAppointmentDetails = false;
        }
        
        this.refs["addAppointment"].setState({
            modalVisible: true,
            appointmentId: 0
        });
    };

    async _onPressAppointment(id) {
        this.refs["addAppointment"].showLoaderAppointmentDetail();
        this.token = await jwtToken();
        let AppointmentDetails = await getAppointmentDetails(this.token, id);
        //set technician
        this.refs["addAppointment"].selectedTechnician =
            AppointmentDetails.technician_id;
        this.refs["addAppointment"].technicianName =
            AppointmentDetails.technician_fullname;
        //set time
        let starttime = moment(AppointmentDetails.start_time);
        this.refs["addAppointment"].selectedDate = starttime;
        this.refs["addAppointment"].selectedHour = starttime.format("hh:mm A");
        this.refs["addAppointment"].appointmentDate = starttime.format(
            "DD-MM-Y"
        );
        this.refs["addAppointment"].appointmentHour = starttime.format("H:mm");
        this.refs["addAppointment"].selectedTime =
            starttime.format("MM-DD-Y") + " " + starttime.format("hh:mm A");
        //set client
        this.refs["addAppointment"].clientName = String.prototype.trim.call(AppointmentDetails.client.fullname);
        let x = AppointmentDetails.client;
        if(typeof x.phone != 'undefined' && x.phone != '' && x.phone != null){
            let displayPhone = formatPhone(x.phone)
            if(this.userData.role == 9){
                let displayphoneSplit = displayPhone.split('-');
                if(displayphoneSplit.length > 1){
                    displayPhone = '(xxx) xxx-' + displayphoneSplit[1];
                }       
            }
            this.refs["addAppointment"].clientName = displayPhone;
        }else if(typeof x.email != 'undefined' && x.email != '' && x.email != null){
            this.refs["addAppointment"].clientName = x.email;
        }    
        let infoclient = this.clients.filter(function(itemclient){
            return itemclient.id == AppointmentDetails.client.id;
        })
        if(typeof(infoclient) != "undefined" && infoclient.length > 0){
            this.refs["addAppointment"].InfoClient = infoclient[0];
        }
        this.refs["addAppointment"].selectedClient =
            AppointmentDetails.client.id;
        //set Status
        this.refs["addAppointment"].statusName = collect(
            this.listAppointmentStatus
        ).first(function(item) {
            return item.id == AppointmentDetails.status;
        }).title;
        this.refs["addAppointment"].selectedStatus = AppointmentDetails.status;
        //set services
        this.refs["addAppointment"].selectServices = {};
        let serviceList = collect(this.services);
        let _this = this;
        let countService = 0;
        AppointmentDetails.services.map((dataservice, i) => {
            let servicedata = serviceList.first(function(item) {
                return item.id == 'service_' + dataservice.service_id;
            });
            let techName = getTextByKey(this.languageKey,'selecttechnicianappointment');
            let technicianData = this.techniciansWithSearch.filter(function(itemTech){
                return itemTech.id == dataservice.technicianId;
            });
            if(technicianData.length){
                techName = technicianData[0].fullname;
            }

            let service = {
                id: 'service_' + dataservice.service_id,
                service_name: servicedata.service_name,
                price: dataservice.price,
                technicianId: dataservice.technicianId,
                technicianName: techName,
                duration: servicedata.duration,
                rewardpoint: servicedata.rewardpoint,
                isCombo: false,
                appointment_service_id: dataservice.appointment_service_id
            };
            _this.refs["addAppointment"].selectServices[
                "service_" + countService
            ] = service;
            countService++;
        });

        AppointmentDetails.combos.map((dataservice, i) => {
            let comboname = '';
            let rewardpoint = 0;
            let combodata = this.combos.filter(function(item) {
                return item.id == dataservice.comboid;
            });

            if(combodata.length){
                comboname = combodata[0].comboname;
                rewardpoint = combodata[0].rewardpoint;
            }

            let service = {
                id: 'combo_' + dataservice.comboid,
                service_name: comboname,
                price: dataservice.price,
                technicianId: 0,
                technicianName: '',
                duration: 0,
                rewardpoint: rewardpoint,
                isCombo: true,
                appointment_combo_id: dataservice.appointment_combo_id
            };
      
            service.servicesIncombo = dataservice.servicesInCombo;
            service.servicesIncombo.forEach(function(itemService){
                itemService.sp_combo_id = dataservice.comboid;
                let serviceCombo = serviceList.first(function(serviceData){ 
                    return serviceData.id == 'service_' + itemService.serviceid;        
                });
                
                let techName = getTextByKey(this.languageKey,'selecttechnicianappointment');
                let technicianData = _this.techniciansWithSearch.filter(function(itemTech){
                    return itemTech.id == itemService.technicianId;
                });
                if(technicianData.length){
                    techName = technicianData[0].fullname;
                }

                if(serviceCombo){
                    itemService.servicename = serviceCombo.service_name;
                    itemService.duration = serviceCombo.duration;
                    itemService.price = itemService.price;
                    itemService.technicianId = itemService.technicianId;
                    itemService.technicianName = techName;
                    itemService.appointment_service_id = itemService.appointment_service_id
                }
            })

            //console.log(service);
            _this.refs["addAppointment"].selectServices[
                "service_" + countService
            ] = service;
            countService++;
            
        });

        _this.refs["addAppointment"].refs.btnAddService.setState({
            isShowPlusService: true,
            count: countService
        });
        //console.log(this.refs["addAppointment"].selectServices);
        this.refs["addAppointment"].refs.AppointmentSelectedService.setState({
            selectServices: this.refs["addAppointment"].selectServices
        });
        this.refs["addAppointment"].refs.coupon.setState({
            DataserviceSelect: this.refs["addAppointment"].selectServices
        });
        //set payments
        this.refs["addAppointment"].payments = AppointmentDetails.payments;
        this.refs["addAppointment"].newPaymentAnimation = "slide";
        this.refs["addAppointment"].total = AppointmentDetails.total;
        this.refs["addAppointment"].remaining =
            AppointmentDetails.remaining_payment;
        this.refs["addAppointment"].paid_total = AppointmentDetails.paid_total;
        this.refs["addAppointment"].isShowLoaderAppointmentDetails = false;

        this.refs["addAppointment"].clients = await fetchClientsData(this.token);
        //console.log(this.refs["addAppointment"].clients);  
        //await this.refs["addAppointment"].open(id);
        this.refs["addAppointment"]._onGetCoupon();
        this.refs["addAppointment"].setState({
            modalVisible: true,
            appointmentId: id
        });
    }

    async _onViewAppointment(id) {
        this.refs["viewAppointment"].showLoaderAppointmentDetail();
        this.token = await jwtToken();
        let AppointmentDetails = await getAppointmentDetails(this.token, id);
        
        //set time
        let starttime = moment(AppointmentDetails.start_time);
       
        this.refs["viewAppointment"].selectedTime =
            starttime.format("MM-DD-Y") + " " + starttime.format("hh:mm A");
        //set client
        this.refs["viewAppointment"].clientName = String.prototype.trim.call(AppointmentDetails.client.fullname);
        let x = AppointmentDetails.client;
        if(typeof x.phone != 'undefined' && x.phone != '' && x.phone != null){
            let displayPhone = formatPhone(x.phone)
            if(this.userData.role == 9){
                let displayphoneSplit = displayPhone.split('-');
                if(displayphoneSplit.length > 1){
                    displayPhone = '(xxx) xxx-' + displayphoneSplit[1];
                }       
            }
            this.refs["viewAppointment"].clientName = displayPhone;
        }else if(typeof x.email != 'undefined' && x.email != '' && x.email != null){
            this.refs["viewAppointment"].clientName = x.email;
        }    


        //set Status
        this.refs["viewAppointment"].statusName = collect(
            this.listAppointmentStatus
        ).first(function(item) {
            return item.id == AppointmentDetails.status;
        }).title;
       
        //set services
        this.refs["viewAppointment"].selectServices = {};
        let serviceList = collect(this.services);
        let _this = this;
        let countService = 0;
        AppointmentDetails.services.map((dataservice, i) => {
            let servicedata = serviceList.first(function(item) {
                return item.id == 'service_' + dataservice.service_id;
            });
            let techName = getTextByKey(this.languageKey,'selecttechnicianappointment');
            let technicianData = this.techniciansWithSearch.filter(function(itemTech){
                return itemTech.id == dataservice.technicianId;
            });
            if(technicianData.length){
                techName = technicianData[0].fullname;
            }

            let service = {
                id: 'service_' + dataservice.service_id,
                service_name: servicedata.service_name,
                price: dataservice.price,
                technicianId: dataservice.technicianId,
                technicianName: techName,
                duration: servicedata.duration,
                rewardpoint: servicedata.rewardpoint,
                isCombo: false,
                appointment_service_id: dataservice.appointment_service_id
            };
            _this.refs["viewAppointment"].selectServices[
                "service_" + countService
            ] = service;
            countService++;
        });

        AppointmentDetails.combos.map((dataservice, i) => {
            let comboname = '';
            let rewardpoint = 0;
            let combodata = this.combos.filter(function(item) {
                return item.id == dataservice.comboid;
            });

            if(combodata.length){
                comboname = combodata[0].comboname;
                rewardpoint = combodata[0].rewardpoint;
            }

            let service = {
                id: 'combo_' + dataservice.comboid,
                service_name: comboname,
                price: dataservice.price,
                technicianId: 0,
                technicianName: '',
                duration: 0,
                rewardpoint: rewardpoint,
                isCombo: true,
                appointment_combo_id: dataservice.appointment_combo_id
            };
      
            service.servicesIncombo = dataservice.servicesInCombo;
            service.servicesIncombo.forEach(function(itemService){
                itemService.sp_combo_id = dataservice.comboid;
                let serviceCombo = serviceList.first(function(serviceData){ 
                    return serviceData.id == 'service_' + itemService.serviceid;        
                });
                
                let techName = getTextByKey(this.languageKey,'selecttechnicianappointment');
                let technicianData = _this.techniciansWithSearch.filter(function(itemTech){
                    return itemTech.id == itemService.technicianId;
                });
                if(technicianData.length){
                    techName = technicianData[0].fullname;
                }

                if(serviceCombo){
                    itemService.servicename = serviceCombo.service_name;
                    itemService.duration = serviceCombo.duration;
                    itemService.price = itemService.price;
                    itemService.technicianId = itemService.technicianId;
                    itemService.technicianName = techName;
                    itemService.appointment_service_id = itemService.appointment_service_id
                }
            })

            //console.log(service);
            _this.refs["viewAppointment"].selectServices[
                "service_" + countService
            ] = service;
            countService++;
            
        });

        this.refs["viewAppointment"].refs.AppointmentSelectedService.setState({
            selectServices: this.refs["viewAppointment"].selectServices
        });
        //set payments
        
        this.refs["viewAppointment"].isShowLoaderAppointmentDetails = false;

        

        this.refs["viewAppointment"].setState({
            modalVisible: true,
            appointmentId: id
        });
    }

    convertTo24Hour(time) {
        time = time.toLowerCase();
        var hours = time.substr(0, 2);
        if (time.indexOf("am") != -1 && hours == 12) {
            time = time.replace("12", "0");
        }
        if (time.indexOf("pm") != -1 && parseInt(hours) < 12) {
            time = time.replace(hours, parseInt(hours) + 12);
        }
        return time.replace(/(am|pm)/, "");
    }

    calendarStripHeaderPress = () => {
        //console.log(this.refs.CalendarStripByWeek.getSelectedDate());
        //console.log(this.refs.CalendarStripByWeek.getSelectedDate());
        //this.refs.modalCalendarByWeek.props.date = this.refs.CalendarStripByWeek.getSelectedDate();
        this.refs.modalCalendarByWeek.show(
            this.refs.CalendarStripByWeek.getSelectedDate()
        );
    };

    calendarHeader = title => {
        return (
            <CalendarStripHeader
                onPress={this.calendarStripHeaderPress}
                headerTitle={title}
                toDayOnPress={this.toDayWeek}
                language={this.languageKey}
            />
        );
    };

    onSelectDateModalByWeek = date => {
        //this.refs.CalendarStripByWeek.setSelectedDate(date);
        this.focusedDay = moment(date).clone();
        this.byDayDate = moment(date).clone();
        this.startDateWeek = this.focusedDay.clone().startOf("week");
        this.endDateWeek = this.focusedDay.clone().endOf("week");

        this.displayAppointmentByWeek(true);
        //console.log(moment(date));
        //console.log(date);
    };

    calendarStripSelectedDate = date => {
        //this.setState({ dateselect: date })
        this.focusedDay = date.clone();
        this.byDayDate = date.clone();
        this.displayAppointmentByWeek(true, false);

        //if (!this.isTabDay) {
        //this.displayAppointmentByWeek(true);
        // console.log(date.day());
        /*    
            this.refs.AppointmentListByWeek.setState({
                byday: false,
                sectionIndex: date.day(),
                itemIndex: 0,
                delay: 50
            });*/

        //console.log('yes');
        //}
    };

    SaveAppointmentSuccess = (appointmentId, data) => {
        this.refs.addAppointment.close();
        //console.log(data);
        /*
        let appointmentDataFormat = {};
        appointmentDataFormat.id = data.id;
        appointmentDataFormat.status = data.status;
        appointmentDataFormat.booking_code = data.booking_code;
        appointmentDataFormat.total = data.total;
        appointmentDataFormat.start_time = data.start_time;
        appointmentDataFormat.end_time = data.end_time;
        //appointmentDataFormat.technician_id = '';
        //appointmentDataFormat.technician_fullname = data.technician_fullname;
        appointmentDataFormat.client = data.client;*/

        /*
        if (appointmentId == 0) {
            this.appointments.push(appointmentDataFormat);
        } else {
            this.appointments = this.appointments.reject(function(item) {
                return item.id == data.id;
            });
            this.appointments.push(appointmentDataFormat);
        }
        this.loadedMonths[this.currentYM] = this.appointments;*/
        this.refreshData(
            moment(data.start_time),
            appointmentId,
            data
        );
        if (
            this.isTabDay &&
            this.byDayDate.isSame(moment(data.start_time), "day")
        ) {
            this.displayAppointmentByDay(true);
        } else {
           
            this.focusedDay = moment(data.start_time);
            this.byDayDate = moment(data.start_time).clone();
            this.startDateWeek = this.focusedDay.clone().startOf("week");
            this.endDateWeek = this.focusedDay.clone().endOf("week");
            if (this.isTabDay) {
                this.refs.tabAppointment.goToPage(1);
            }
            this.displayAppointmentByWeek(true);
            //this.refs.CalendarStripByWeek.setSelectedDate(this.focusedDay);
        }
        //console.log(isNew);
        //console.log(data);
    };

    refreshData = (starttime, appointmentId, appointmentDataFormat) => {
        let YM = starttime.format("YMM");
        if (typeof this.loadedMonths[YM] != "undefined") {
            if(Platform.OS === 'ios') {
                if (appointmentId == 0) {
                    this.loadedMonths[YM] = collect([
                        ...this.loadedMonths[YM]
                    ]).push(appointmentDataFormat);
                } else {
                    this.loadedMonths[YM] = collect([
                        ...this.loadedMonths[YM]
                    ]).reject(function(item) {
                        return item.id == appointmentId;
                    });
                    this.loadedMonths[YM] = collect([
                        ...this.loadedMonths[YM]
                    ]).push(appointmentDataFormat);
                }
             } else {
                if (appointmentId == 0) {
                    this.loadedMonths[YM] = this.loadedMonths[YM].push(appointmentDataFormat);
                } else {
                    this.loadedMonths[YM] = this.loadedMonths[YM].reject(function(item) {
                        return item.id == appointmentId;
                    });
                    this.loadedMonths[YM] = this.loadedMonths[YM].push(appointmentDataFormat);
                }
            } 
        }
        //console.log(this.loadedMonths[YM]);
    };

    render() {
        if (this.state.appIsReady) {
            return (
                <View style={styles.container}>
                    <ScrollableTabView
                        renderTabBar={() => <DefaultTabBar />}
                        onChangeTab={this.changeTab}
                        locked={true}
                        ref="tabAppointment"
                    >
                        <View tabLabel={getTextByKey(this.languageKey,'byday')} style={{ flex: 1 }}>
                            <AppointmentSelectDay
                                changeDate={this.changeDate}
                                today={this._today}
                                isToday={this.isToday}
                                byDayText={this.byDayText}
                                ref="AppointmentSelectDay"
                                language={this.languageKey}
                            />
                            <View style={styles.tabContents}>
                                <AppointmentListByDay
                                    ref="AppointmentListByDay"
                                    visible={false}
                                    addAppointment={async (key) => {await this.addAppointment(key)}}
                                    byday={true}
                                    sectiondata={this.technicians}
                                    data={this.appointmentsByDay}
                                    userData={this.userData}
                                    token={this.token}
                                    onPressItem={async id => {
                                        await this._onPressAppointment(id);
                                    }}
                                    onViewItem={async id => {
                                        await this._onViewAppointment(id);
                                    }}
                                    refresh={this.refreshData}
                                    language={this.languageKey}
                                />
                            </View>
                        </View>
                        <View tabLabel={getTextByKey(this.languageKey,'byweek')} style={{ flex: 1 }}>
                            <View style={styles.calendarcontainer}>
                                <CalendarStrip
                                    //datesWhitelist={this.datesWhitelist}
                                    startingDate={this.startDateWeek.clone()}
                                    useIsoWeekday={false}
                                    calendarHeaderFormat="MMM Y"
                                    style={{ flex: 1 }}
                                    dateNameStyle={styles.dateNameStyle}
                                    dateNumberStyle={styles.dateNumberStyle}
                                    dateNumberStyleText={
                                        styles.dateNumberStyleText
                                    }
                                    calendarHeaderStyle={
                                        styles.calendarHeaderStyle
                                    }
                                    selectedDate={this.focusedDay.clone()}
                                    highlightDateNameStyle={styles.selectedDate}
                                    highlightDateNumberStyle={
                                        styles.highlightDateNumberStyle
                                    }
                                    highlightDateNumberStyleText={
                                        styles.highlightDateNumberStyleText
                                    }
                                    calendarHeader={this.calendarHeader}
                                    ref="CalendarStripByWeek"
                                    onDateSelected={
                                        this.calendarStripSelectedDate
                                    }
                                    onWeekChanged={this.changeWeek}
                                />
                            </View>
                            <View style={styles.tabContents}>
                                <AppointmentListByWeek
                                    ref="AppointmentListByWeek"
                                    addAppointment={async (key) => {await this.addAppointment(key)}}
                                    visible={false}
                                    byday={false}
                                    sectiondata={this.technicians}
                                    data={this.appointmentsByWeek}
                                    userData={this.userData}
                                    token={this.token}
                                    onPressItem={async id => {
                                        await this._onPressAppointment(id);
                                    }}
                                    onViewItem={async id => {
                                        await this._onViewAppointment(id);
                                    }}
                                    refresh={this.refreshData}
                                    language={this.languageKey}
                                />
                            </View>
                        </View>
                    </ScrollableTabView>

                    <ModalNotification ref="modalNotification" onPress={this.onPressNotificationItem} language={this.languageKey} token={this.token}/>
                    <AddAppointment
                        clients={this.clients}
                        technicians={this.techniciansWithSearch}
                        title={this.addAppointmentTitle}
                        ref={"addAppointment"}
                        availablehours={this.availablehours}
                        selectServices={this.selectServices}
                        token={this.token}
                        services={this.services}
                        listStatus={this.listAppointmentStatusForCreate}
                        SaveAppointmentSuccess={this.SaveAppointmentSuccess}
                        deviceid={this.deviceid}
                        userData={this.userData}
                        combos={this.combos}
                        categories={this.categories}
                        language={this.languageKey}
                    />

                    <ViewAppointment
                        clients={this.clients}
                        technicians={this.techniciansWithSearch}
                        title={this.addAppointmentTitle}
                        ref={"viewAppointment"}
                        availablehours={this.availablehours}
                        selectServices={this.selectServices}
                        token={this.token}
                        services={this.services}
                        listStatus={this.listAppointmentStatus}
                        SaveAppointmentSuccess={this.SaveAppointmentSuccess}
                        deviceid={this.deviceid}
                        userData={this.userData}
                        combos={this.combos}
                        language={this.languageKey}
                    />

                    <ModalCalendar
                        ref="modalCalendarByWeek"
                        onPress={this.onSelectDateModalByWeek}
                    />

                    <SpinnerLoader
                        visible={false}
                        textStyle={layout.textLoaderScreen}
                        overlayColor={"#fff"}
                        textContent={getTextByKey(this.languageKey,'loadingappointment')}
                        color={Colors.spinnerLoaderColor}
                        ref="tabappointmentloader"
                    />

                    
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    <SpinnerLoader
                        visible={true}
                        textStyle={layout.textLoaderScreen}
                        overlayColor={"transparent"}
                        textContent={getTextByKey(this.languageKey,'loadingappointment')}
                        color={Colors.spinnerLoaderColor}
                    />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    mainTab: {
        backgroundColor: "red"
    },
    tabContents: {
        flex: 1
    },
    bydaytext: {
        fontSize: 18
    },
    calendarcontainer: {
        height: 93,
        paddingTop: 10,
        paddingBottom: 0,
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: "#ddd"
    },
    dateNameStyle: {
        color: "#333",
        fontWeight: "normal",
        fontSize: 14
    },
    dateNumberStyle: {
        width: 24,
        height: 24,
        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden"
    },
    dateNumberStyleText: {
        color: "#333",
        fontWeight: "normal",
        fontSize: 14
    },
    calendarHeaderStyle: {
        color: "#333",
        fontWeight: "normal",
        fontSize: 16
    },
    selectedDate: {
        color: "#F069A2",
        fontWeight: "normal",
        fontSize: 14
    },
    highlightDateNumberStyle: {
        width: 24,
        height: 24,
        borderRadius: 24,
        backgroundColor: "#F069A2",

        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden"
    },
    highlightDateNumberStyleText: {
        color: "#fff",
        fontSize: 14,
        fontWeight: "normal"
    }
});
