import React from "react";
import {
    StyleSheet,
    Text,
    View
} from "react-native";
import {Expo, LinearGradient} from "expo";
import {MaterialCommunityIcons} from "@expo/vector-icons";

import Router from "../navigation/Router";
import { StackNavigation } from "@expo/ex-navigation";
import SpinnerLoader from "../helpers/spinner";

export default class ApplyLanguageScreen extends React.Component {
    static route = {
        navigationBar: {
            visible: false
        },
    };
    
    async componentWillMount(){
        if(typeof(this.props.route.params.changeLanguage) != 'undefined'){
            let _this = this;
            setTimeout(function(){
                _this.props.navigator.push(Router.getRoute('rootNavigation',{language:_this.props.route.params.language, isSetLanguage: true}));
            },2000)
        }
    }

    
    render() {
        
        return (
            <LinearGradient
                start={[0, 0]}
                end={[1, 1]}
                colors={["#F069A2", "#EECBA3"]}
                style={styles.loader}
            >
                <SpinnerLoader
                    visible={true}
                    textStyle={styles.textLoader}
                    overlayColor={"transparent"}
                    textContent={"Applying Language..."}
                />
            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    statusBarUnderlay: {
        height: 24,
        backgroundColor: "rgba(0,0,0,0.2)"
    },
    textLoader: {
        color: "#fff",
        fontWeight: "normal",
        fontSize: 16
    },
    loader: {
        flex: 1
    },
    Maintainloader: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    maintaintext:{
        color:'#fff',
        fontSize:22
    }
});
