import React from "react";
import { StyleSheet, View, Text, Alert, AppState } from "react-native";
import { Notifications } from "expo"; 
import {
    StackNavigation,
    TabNavigation,
    TabNavigationItem
} from "@expo/ex-navigation";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Alerts from "../constants/Alerts";
import Colors from "../constants/Colors";
//import registerForPushNotificationsAsync from "../api/registerForPushNotificationsAsync";
import Router from "./Router";
import { getUserData } from "../helpers/authenticate";
import moment from "moment";
import { getLanguageName, getLanguage, getTextByKey } from "../helpers/language";
import { jwtToken, recheckNotification } from "../helpers/authenticate";

export default class RootNavigation extends React.Component {
    currentTab = 'calendar';
    badge = 0;
    listTabs = [];
    languageKey = this.props.route.params.language; 

    async componentDidMount() {
        this.token = await jwtToken();
        recheckNotification(this.token,this._handleLocalNotification);
        this.userData = await getUserData();
        this._notificationSubscription = this._registerForPushNotifications(
            this
        );

        let _this = this;
        if(typeof(this.props.route.params.isSetLanguage) != 'undefined'){
            this.props.navigation.performAction(
                ({ tabs, stacks }) => {
                    tabs("maintab").jumpToTab('more');

                    setTimeout(function(){
                        //console.log(_this.languageKey);
                        let rootNavigator = _this.props.navigation.getNavigator('more');   
                        rootNavigator.replace(Router.getRoute('more', {language: _this.languageKey, isSetLanguage: true}));         
                    },100)
                }
            );
        }

        AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            recheckNotification(this.token,this._handleLocalNotification);
        }
        this.setState({appState: nextAppState});
    }

    _handleLocalNotification = (data) => {
       
        if(typeof(data) != 'undefined'){
            let localbadge = data.appointmentBadgeCount;
            let _this = this;
            if(this.currentTab != 'calendar'){
                _this.badge = localbadge;
            }
            
            _this.props.navigation.performAction(
                ({ tabs, stacks }) => {
                    switch(_this.currentTab){
                        case 'more':
                            tabs("maintab").jumpToTab('clients');
                            break;
                        case 'clients':
                            tabs("maintab").jumpToTab('more');
                            break;  
                        case 'dashboard':
                            tabs("maintab").jumpToTab('more');
                            break;      
                    }

                    tabs("maintab").jumpToTab(_this.currentTab);
                    
                    let rootNavigator = _this.props.navigation.getNavigator(
                        "calendar"
                    );
                    rootNavigator.updateCurrentRouteParams({
                        badge: localbadge
                    });
                }
            );
        }
      
    }

    state = {
        initialTab: "calendar",
        appState: AppState.currentState
    };

    
    async componentWillMount(){
        

        this.languageKey = await getLanguage();
 
        var backtab = {
            route: 'back',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['back'] = backtab;

        var dashboardtab = {
            route: 'dashboard',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['dashboard'] = dashboardtab;

        var calendartab = {
            route: 'calendar',
            language: this.languageKey,
            isOpen: true
        };
        this.listTabs['calendar'] = calendartab;

        var checkintab = {
            route: 'checkin',
            language: this.languageKey,
            isOpen: true
        };
        this.listTabs['checkin'] = checkintab;

        var manageEgifttab = {
            route: 'manageEgift',
            language: this.languageKey,
            isOpen: true
        };
        this.listTabs['manageEgift'] = manageEgifttab;

        var clienttab = {
            route: 'clients',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['clients'] = clienttab;

        var stafftab = {
            route: 'staff',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['staff'] = stafftab;

        var blockedtimetab = {
            route: 'blockedtime',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['blockedtime'] = blockedtimetab;

        var servicetab = {
            route: 'service',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['service'] = servicetab;

        var smstab = {
            route: 'sms',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['sms'] = smstab;

        var moretab = {
            route: 'more',
            language: this.languageKey,
            isOpen: false
        };
        this.listTabs['more'] = moretab;
    }

    componentWillUnmount() {
        this._notificationSubscription &&
            this._notificationSubscription.remove();
    }

    onPressTab = async (tabItemOnPress, event, tabid) => {
        this.currentTab = tabid;
        if(tabid == "calendar"){
            this.badge = 0;
            /*
            this.props.navigation.performAction(
                ({ tabs, stacks }) => {
                    
                    let rootNavigator = this.props.navigation.getNavigator(
                        "calendartab"
                    );
                    rootNavigator.updateCurrentRouteParams({
                        badge: 3
                    });
                }
            );*/

            /*
            this.props.navigation.performAction(
                ({ tabs, stacks }) => {
    
                    let rootNavigator = this.props.navigation.getNavigator(
                        "calendartab"
                    );
                    rootNavigator.replace('calendar');
                }
            );*/
        }
        
        
        tabItemOnPress();
        
        
        this.languageKey = await getLanguage();
        var currentTab = this.listTabs[tabid];
       
        if(this.languageKey != currentTab.language){
            this.listTabs[tabid].language = this.languageKey;
            let _this = this;
            this.props.navigation.performAction(
                ({ tabs, stacks }) => {
                    if(currentTab.isOpen){
                        let rootNavigator = _this.props.navigation.getNavigator(tabid);   
                        rootNavigator.replace(Router.getRoute(currentTab.route, {language: _this.languageKey}));
                    }else{
                        setTimeout(function(){
                            let rootNavigator = _this.props.navigation.getNavigator(tabid);   
                            rootNavigator.replace(Router.getRoute(currentTab.route, {language: _this.languageKey}));         
                        },100)
                    }
                    
                   
                }
            );
        }
        this.listTabs[tabid].isOpen = true;
        
    }
    async onback(){
        let rootNavigator = this.props.navigation.getNavigator("root");
        rootNavigator.replace(Router.getRoute("home",{businessname: this.userData.businessname}));
    }
    render() {
        return (
            <TabNavigation
                navigatorUID="maintab"
                id="maintab"
                tabBarHeight={56}
                initialTab={this.state.initialTab}
            >
                <TabNavigationItem
                    id="back"
                    ref='back'
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "backburger",
                            isSelected,
                            "backtab"
                        )}
                        onPress={async () => {await this.onback()}}  
                >
                    <StackNavigation
                        navigatorUID="calendar"
                        id="calendar"
                        initialRoute={Router.getRoute("calendar",{language: this.languageKey})}
                    />
                </TabNavigationItem>
                <TabNavigationItem
                    id="dashboard"
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "home-outline",
                            isSelected,
                            "dashboardtab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'dashboard');}}        
                >
                    <StackNavigation initialRoute="dashboard" id="dashboard" navigatorUID="dashboard"/>
                </TabNavigationItem>

                <TabNavigationItem
                    id="calendar"
                    ref='calendar'
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "calendar-text",
                            isSelected,
                            "calendartab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'calendar');}}  
                >
                    <StackNavigation
                        navigatorUID="calendar"
                        id="calendar"
                        initialRoute={Router.getRoute("calendar",{language: this.languageKey})}
                    />
                </TabNavigationItem>

                <TabNavigationItem
                    id="checkin"
                    ref='checkin'
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "calendar-clock",
                            isSelected,
                            "checkintab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'checkin');}}  
                >
                    <StackNavigation
                        navigatorUID="checkin"
                        id="checkin"
                        initialRoute={Router.getRoute("checkinmerchant",{language: this.languageKey})}
                    />
                </TabNavigationItem>

                <TabNavigationItem
                    id="manageEgift"
                    ref='manageEgift'
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "wallet-giftcard",
                            isSelected,
                            "manageEgifttab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'manageEgift');}}  
                >
                    <StackNavigation
                        navigatorUID="manageEgift"
                        id="manageEgift"
                        initialRoute={Router.getRoute("ManageEgift",{language: this.languageKey})}
                    />
                </TabNavigationItem>

                <TabNavigationItem
                    id="clients"
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "account-outline",
                            isSelected,
                            "clientstab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'clients');}} 
                >
                    <StackNavigation initialRoute={Router.getRoute("clients",{language: this.languageKey})} navigatorUID="clients" id="clients"/>
                </TabNavigationItem>

                <TabNavigationItem
                    id="blockedtime"
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "timer-off",
                            isSelected,
                            "blockedtimetab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'blockedtime');}} 
                >
                    <StackNavigation initialRoute={Router.getRoute("blockedtime",{language: this.languageKey})} navigatorUID="blockedtime" id="blockedtime"/>
                </TabNavigationItem>

                <TabNavigationItem
                    id="staff"
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "account-settings-variant",
                            isSelected,
                            "stafftab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'staff');}} 
                >
                    <StackNavigation initialRoute={Router.getRoute("staff",{language: this.languageKey})} navigatorUID="staff" id="staff"/>
                </TabNavigationItem>

                <TabNavigationItem
                    id="service"
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "pencil-box-outline",
                            isSelected,
                            "servicetab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'service');}} 
                >
                    <StackNavigation initialRoute={Router.getRoute("service",{language: this.languageKey})} navigatorUID="service" id="service"/>
                </TabNavigationItem>

                <TabNavigationItem
                    id="sms"
                    renderIcon={isSelected =>
                        this._renderIcon(
                            "message-reply-text",
                            isSelected,
                            "smstab"
                        )}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'sms');}} 
                >
                    <StackNavigation initialRoute={Router.getRoute("sms",{language: this.languageKey})} navigatorUID="sms" id="sms"/>
                </TabNavigationItem>

                <TabNavigationItem
                    id="more"
                    renderIcon={isSelected =>
                        this._renderIcon("dots-horizontal", isSelected, "moretab")}
                        onPress={async (tabItemOnPress,event) => {await this.onPressTab(tabItemOnPress,event,'more');}} 
                >
                    <StackNavigation initialRoute={Router.getRoute("more",{language: this.languageKey})} id="more" navigatorUID="more"/>
                </TabNavigationItem>
            </TabNavigation>
        );
    }

    _renderIcon(name, isSelected, tabName) {
        
        return (
            <View>
                <MaterialCommunityIcons
                    name={name}
                    size={32}
                    color={
                        isSelected
                            ? Colors.tabIconSelected
                            : Colors.tabIconDefault
                    }
                    style={styles.tabCenter}
                />
                <Text
                    style={[
                        styles.tabCenter,
                        styles.tabTitle,
                        isSelected
                            ? styles.tabTextSelected
                            : styles.tabTextDefault
                    ]}
                >
                    {getTextByKey(this.languageKey,tabName)}
                </Text>
                {
                    name == 'calendar-text' && this.badge > 0 &&
                    <View style={styles.badgecontainer}>
                        <Text style={styles.badgetext}>{this.badge}</Text>
                    </View>            
                }
                
            </View>
        );
    }

    _registerForPushNotifications(_this) {
        // Send our push token over to our backend so we can receive notifications
        // You can comment the following line out if you want to stop receiving
        // a notification every time you open the app. Check out the source
        // for this function in api/registerForPushNotificationsAsync.js
        //registerForPushNotificationsAsync();

        // Watch for incoming notifications
        this._notificationSubscription = Notifications.addListener(data => {
            this._handleNotification(data, _this);
        });
    }

    _handleNotification({ origin, data }, _this) {
        
        switch (data.case) {
            case "new_appointment":
                if (
                    (_this.userData.role == 4 &&
                        _this.userData.id == data.serviceprovider_id) ||
                    (_this.userData.role == 9 &&
                        _this.userData.serviceprovider_id ==
                            data.serviceprovider_id &&
                        _this.userData.id == data.technician_id)
                ) {
                    if (origin == "selected") {
                        let pushData = {};
                        pushData.data = data;
                        pushData.origin = origin;
                        
                        
                        setTimeout(function() {
                            _this.props.navigation.performAction(
                                ({ tabs, stacks }) => {
                                    tabs("maintab").jumpToTab("calendar");
                                    let rootNavigator = _this.props.navigation.getNavigator(
                                        "calendar"
                                    );
                                    rootNavigator.replace(Router.getRoute("calendar", {
                                        showTabWeek: true,
                                        notificationData: pushData
                                    }));
                                }
                            );
                        }, 0);

                    }else{
                        if(typeof(data.localcase) != 'undefined'){
                            let time = moment(data.starttime).format('ll');
                            _this.props.navigator.showLocalAlert('You have new appointment on ' + time, {
                                text: {color: '#fff'},
                                container: {backgroundColor: '#ED536E'},
                                duration: 5000
                            }); 
                            
                            if(_this.currentTab != 'calendar'){
                               
                                let localbadge = 0;
                                if(typeof(data.badge) != 'undefined'){
                                    localbadge = data.badge;
                                }
                                /*
                                if(localbadge){
                                    _this.badge = 3;
                                    _this.props.navigation.performAction(
                                        ({ tabs, stacks }) => {
                                            
                                            let rootNavigator = _this.props.navigation.getNavigator(
                                                "calendartab"
                                            );
                                            rootNavigator.updateCurrentRouteParams({
                                                badge: 3
                                            });
                                        }
                                    );
                                }*/

                                _this.badge = localbadge;
                                _this.props.navigation.performAction(
                                    ({ tabs, stacks }) => {
                                        switch(_this.currentTab){
                                            case 'more':
                                                tabs("maintab").jumpToTab('clients');
                                                break;
                                            case 'clients':
                                                tabs("maintab").jumpToTab('more');
                                                break;  
                                            case 'dashboard':
                                                tabs("maintab").jumpToTab('more');
                                                break;      
                                        }
               
                                        tabs("maintab").jumpToTab(_this.currentTab);
                                      
                                        let rootNavigator = _this.props.navigation.getNavigator(
                                            "calendar"
                                        );
                                        rootNavigator.updateCurrentRouteParams({
                                            badge: localbadge
                                        });
                                    }
                                );
                            }
                            //Alert.alert(this.currentTab);
                        }
                        
                    }
                }
                break;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    selectedTab: {
        color: Colors.tabIconSelected
    },
    tabCenter: {
        textAlign: "center"
    },
    tabTitle: {
        fontSize: 10
    },
    tabTextSelected: {
        color: Colors.tabIconSelected
    },
    tabTextDefault: {
        color: Colors.tabIconDefault
    },
    badgecontainer: {
        position:'absolute',
        right:0,
        top:-3,
        width:20,
        height:20,
        backgroundColor:'red',
        borderRadius:20,
        justifyContent: "center",
        alignItems: "center",
        zIndex:1
    },
    badgetext:{
        color:'#fff',
        fontSize:14,
        fontWeight:'bold'
    }
});
