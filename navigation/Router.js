import { createRouter } from '@expo/ex-navigation';

import CalendarScreen from '../screens/CalendarScreen';
import ClientsScreen from '../screens/ClientsScreen';
import DashboardScreen from '../screens/DashboardScreen';
import MoreScreen from '../screens/MoreScreen';
import LoginScreen from '../screens/LoginScreen';
import RootNavigation from './RootNavigation';
import PayoutScreen from '../screens/PayoutScreen';
import ApplyLanguageScreen from '../screens/ApplyLanguage';
import StaffScreen from '../screens/StaffScreen';
import BlockedTimeScreen from '../screens/BlockedTimeScreen';
import ServiceScreen from '../screens/ServiceScreen';
import CheckinSectionScreen from '../screens/CheckinSectionScreen';
import SelectAppScreen from '../screens/SelectAppScreen';
import ManageEgiftScreen from '../screens/ManageEgiftScreen';
import SmsTrackingScreen from '../screens/SmsTrackingScreen';

//checkin app screen
import HomeScreen from '../screens/checkinapp/HomeScreen';
import CheckInScreen from '../screens/checkinapp/CheckInScreen';
import BookingSuccessScreen from '../screens/checkinapp/BookingSuccessScreen';
import SettingScreen from '../screens/checkinapp/SettingScreen';
import CustomerCheckInScreen from '../screens/checkinapp/CustomerCheckIn';
import AppointmentsScreen from '../screens/checkinapp/AppointmentsScreen';
import CheckInSucccessScreen from '../screens/checkinapp/CheckInSuccessScreen'; 
import BlockedScreen from '../screens/checkinapp/Blocked'; 
import CheckInWithoutBookingScreen from '../screens/checkinapp/CheckInWithoutBooking';
import WithoutBookingSuccessScreen from '../screens/checkinapp/WithoutBookingSuccessScreen';
import BlockedMultipleLocationScreen from '../screens/checkinapp/BlockedMultipleLocation';

export default createRouter(() => ({
  calendar: () => CalendarScreen,
  clients: () => ClientsScreen,
  dashboard: () => DashboardScreen,
  more: () => MoreScreen,
  login: () => LoginScreen,
  selectapp: () => SelectAppScreen,
  rootNavigation: () => RootNavigation,
  payout: () => PayoutScreen,
  staff: () => StaffScreen,
  blockedtime: () => BlockedTimeScreen,
  service:()=> ServiceScreen,
  checkinmerchant:()=>CheckinSectionScreen,
  applyLanguage: () => ApplyLanguageScreen,
  ManageEgift: () => ManageEgiftScreen,
  sms: () => SmsTrackingScreen,

  home: () => HomeScreen,
  checkin: () => CheckInScreen,
  bookingSuccess: () => BookingSuccessScreen,
  setting: () => SettingScreen,
  CustomerCheckIn: () => CustomerCheckInScreen,
  Appointments: () => AppointmentsScreen,
  CheckInSuccess: () => CheckInSucccessScreen,
  blocked: () => BlockedScreen,
  checkinwithoutbooking: () => CheckInWithoutBookingScreen,
  WithoutBookingSuccessScreen: () => WithoutBookingSuccessScreen,
  BlockedMultipleLocation: () => BlockedMultipleLocationScreen,
  
}));

